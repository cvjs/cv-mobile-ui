const fs = require('fs-extra');
const path = require('path');
const glob = require('glob');
const exec = require('child_process').exec;
const argv = process.argv.splice(2)[0];

const projectPath = process.cwd();

console.log(projectPath);
return;
const componentsPath = path.join(__dirname, '../src/components');
const uiPath = path.join(__dirname, '../uni_modules/');
const lib = path.join(uiPath, 'lib');
const root = path.join(__dirname, '../');

var filenames = [];
var filenamesUpper = [];

const uiPathPackagePath = path.join(uiPath, 'package.json');
var uiPathData = fs.readFileSync(path.join(root, 'package.json'), 'utf-8');
uiPathData = JSON.parse(uiPathData);

var manifest = fs.readFileSync(path.join(root, 'src', 'manifest.json'), 'utf-8');
manifest = JSON.parse(filter_comments(manifest));

var uiPathPackageData = fs.readFileSync(uiPathPackagePath, 'utf-8');
uiPathPackageData = JSON.parse(uiPathPackageData);

var manifest = fs.readFileSync(path.join(root, 'src', 'manifest.json'), 'utf-8');
manifest = JSON.parse(filter_comments(manifest));

let currentVersion = '';
let currentVersionPackage = '';
let manifestVersionName = manifest.versionName;
let manifestVersionCode = parseInt(manifest.versionCode);

uiPathData = versionUpdate(uiPathData, uiPathPackageData);
uiPathPackageData.version = uiPathData.version;
fs.outputFileSync(uiPathPackagePath, JSON.stringify(uiPathPackageData, '', 2));
fs.copySync(path.join(root, 'README.md'), path.join(uiPath, 'README.md'));
fs.removeSync(lib);

fs.copy(componentsPath, lib).then(() => {
  console.log('---- 同步完成 ----');
  const delFileLists = glob.sync(lib + '/**/*.{json,md,bak}');
  // 删除json  md 等不必要文件
  delFileLists
    .reduce((promise, fileName) => {
      return promise.then(() => {
        return fs.remove(fileName);
      });
    }, Promise.resolve([]))
    .then(() => {
      // 删除所有文件成功之后，开始去同步 npm
      console.log(argv);
      if (argv === 'npm') {
        start();
      }
    });
});

/* 更新版本号 */
function versionUpdate(uiPathData, uiPathPackageData) {
  currentVersion = uiPathData.version;
  currentVersionPackage = uiPathPackageData.version;
  console.log(currentVersion, currentVersionPackage);
  if (uiPathData.version == uiPathPackageData.version) {
    console.log('--- 版本号自动加1');
    const versionArr = uiPathData.version.split('.');
    versionArr[versionArr.length - 1]++;
    uiPathData.version = versionArr.join('.');
  } else {
    console.log('--- 版本号已手动修改跳过自动自增');
  }
  console.log(uiPathData.version);

  fs.outputFileSync('package.json', JSON.stringify(uiPathData, '', 2));

  manifest.versionName = uiPathData.version;
  manifest.versionCode = parseInt(manifest.versionCode) + 1;
  fs.outputFileSync(path.join(root, 'src', 'manifest.json'), JSON.stringify(manifest, '', 2));

  return uiPathData;
}
/* 版本号回退 */
function versionBack() {
  console.log('发布失败，版本号回退至自增前');
  console.log(currentVersion, currentVersionPackage);
  uiPathData.version = currentVersion;
  fs.outputFileSync('package.json', JSON.stringify(uiPathData, '', 2));
  uiPathPackageData.version = currentVersionPackage;
  fs.outputFileSync(uiPathPackagePath, JSON.stringify(uiPathPackageData, '', 2));

  manifest.versionName = manifestVersionName;
  manifest.versionCode = manifestVersionCode;
  fs.outputFileSync(path.join(root, 'src', 'manifest.json'), JSON.stringify(manifest, '', 2));
}
function start() {
  // 任何你期望执行的cmd命令，ls都可以
  let cmdStr1 = 'npm publish --access=public';
  //let cmdPath = path.join(__dirname, '..')

  //只需要传lib
  let cmdPath = path.join(uiPath);
  let workerProcess = null;
  // 子进程名称
  runExec(cmdStr1, cmdPath, workerProcess);
}

function runExec(cmdStr, cmdPath, workerProcess) {
  workerProcess = exec(cmdStr, {
    cwd: cmdPath
  });
  // 打印正常的后台可执行程序输出
  workerProcess.stdout.on('data', function (data) {
    console.log(data);
  });
  // 打印错误的后台可执行程序输出
  workerProcess.stderr.on('data', function (data) {
    console.log(data);
  });
  // 退出之后的输出
  workerProcess.on('close', function (code) {
    console.log(!code ? '发布成功' : '发布失败：code' + code);
    if (code !== 0) {
      //版本号回退
      versionBack();
    }
  });
}

/* 过滤注释 */
function filter_comments(string) {
  string = string.replace(/\/\/[\s\S]*?(\r\n|\n)|\/\*[\s\S]*?\*\//g, ''); //过滤多行注释
  return string;
}
