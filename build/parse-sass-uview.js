const fs = require('fs');

// 读取变量

fs.readFile('/path/to/file.txt', 'utf8', (err, data) => {
  if (err) throw err;
  console.log(data);
});

// 解析文件
