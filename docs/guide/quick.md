# 快速开始

>本ui库尽量使用 cli 脚手架模式  
>后续还有自动化部署打包  

**下载**

```sh
npm install @10yun/cv-mobile-ui -D
```

**更新**

```sh
npm install @10yun/cv-mobile-ui -f
```

**配置easycom组件模式**


- [原文地址](https://uniapp.dcloud.io/collocation/pages?id=easycom)


1. uni-app为了调试性能的原因，修改easycom规则不会实时生效，配置完后，您需要重启HX或者重新编译项目才能正常使用 cv-mobile-ui 的功能。
2. 请确保您 `pages.json` 中只有一个 `easycom` 字段，否则请自行合并多个引入规则。
 

此配置需要在项目的 `pages.json` 中配置。

> 在 ``pages.json`` 文件中加入以下匹配规则
> 若使用uni-app的 `easycom` 引入方式，则无需在`script`中重复引用

```json
// pages.json
{
  "easycom": {
    "autoscan": true,
    "custom": {
      "^cv-(.*)": "@10yun/cv-mobile-ui/ui-cv/components//cv-$1/cv-$1.vue"
    }
  },
	// 此为本身已有的内容
	"pages": [
		// ......
	]
}
```




### 使用方法

配置easycom规则后，自动按需引入，无需import组件，直接引用即可。

```vue
<template>
	<cv-input-text label="文本" v-model=""/>
</template>
```

详细参考

- 表单
- 富交互
- 更多