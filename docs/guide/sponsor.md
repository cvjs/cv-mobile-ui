# 赞助我们

捐赠[十云] ctocode 开发组的研发

- 移动端ui组件
- pc端ui组件
- cvjs
- 客户端
- js工具库
- php包
- java包
  
做一个大型架构是一项庞大的工作，尤其是要考虑

- 前后分离
- 交互
- 部署
- 自动化
- 官方的更新
- 技术栈
- 微服务
- 开放开发

研发组经常为此工作到深夜……


<img src="/weixin.png" style="width:200px;margin: 0 30px;display:inline-block;"/>
<img src="/zhifubao.png" style="width:200px;margin: 0 30px;display:inline-block;"/>



### 赞助商

<span style="color:red; font-weight: bold;">[十云平台]</span> 拥有众多用户，且文档详尽，经得起推敲，受得住考验，平台每天有大量访问者，如果您认为这些有助于您公司的业务推广，可以成为我们的赞助商， 我们会在适当的位置展示您的推广内容。

- 官方网站
- h5产品
- h5应用
- pc产品
- pc应用  
- 微信小游戏