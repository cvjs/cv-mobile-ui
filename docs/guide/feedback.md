# 交流反馈

欢迎加QQ群交流反馈，一起学习，共同进步。

### 官方QQ交流群

点击群号即可快速加群

- 群：468217742 [点此加入](https://qm.qq.com/cgi-bin/qm/qr?k=334vc9ChCoP7zWDZsAF5YyRPzRNoPR3K&jump_from=webapi) 



### 贡献代码

在使用 `cv-mobile-ui` 中，如遇到无法解决的问题，请提 [Issues](https://gitee.com/cvjs/cv-mobile-ui/issues) 给我们；  
假如您有更好的点子或更好的实现方式，也欢迎给我们提交 [PR](https://gitee.com/cvjs/cv-mobile-ui/pulls)

