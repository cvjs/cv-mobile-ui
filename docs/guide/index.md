<style>
.disanfang {
  text-align: center;
}
.disanfang p {
  display: inline-block;
  margin-right: 10px;
}
</style>

<p align="center">
  <img alt="logo" src="/logo.png" style="margin-bottom: 10px;width:250px">
</p>
<h1 align="center" style="font-weight: bold;">cv-mobile-ui 组件 v-0.3.x</h1>
<h4 align="center">移动端UI</h4>
<h4 align="center">多平台快速开发的UI框架</h4>
<h4 align="center">一款高性能，简洁、快速开发的ui组件，功能全面，上手简单</h4>
<div class="disanfang">

[![star](https://gitee.com/cvjs/cv-mobile-ui/badge/star.svg?theme=dark)](https://gitee.com/cvjs/cv-mobile-ui/stargazers)

[![fork](https://gitee.com/cvjs/cv-mobile-ui/badge/fork.svg?theme=dark)](https://gitee.com/cvjs/cv-mobile-ui/members)

[![Website](https://img.shields.io/badge/cv-mobile-ui?style=flat-square)](https://cvjs.cn/cv-mobile-ui)
<!-- 
[![star](https://gitee.com/cvjs/cv-mobile-ui/badge/star.svg?theme=gvp)](https://gitee.com/cvjs/cv-mobile-ui/stargazers)
[![fork](https://gitee.com/cvjs/cv-mobile-ui/badge/fork.svg?theme=gvp)](https://gitee.com/cvjs/cv-mobile-ui/members)
-->
[![npm](https://img.shields.io/npm/v/@10yun/cv-mobile-ui.svg)](https://www.npmjs.com/package/@10yun/cv-mobile-ui)

[![download](https://img.shields.io/npm/dw/@10yun/cv-mobile-ui.svg)](https://npmcharts.com/compare/@10yun/cv-mobile-ui?minimal=true)
<!-- [![stars](https://img.shields.io/github/stars/10yun/cv-mobile-ui?style=flat-square&logo=GitHub)](https://github.com/10yun/cv-mobile-ui)
[![forks](https://img.shields.io/github/forks/10yun/cv-mobile-ui?style=flat-square&logo=GitHub)](https://github.com/10yun/cv-mobile-ui)
[![issues](https://img.shields.io/github/issues/10yun/cv-mobile-ui?style=flat-square&logo=GitHub)](https://github.com/10yun/cv-mobile-ui/issues)
[![release](https://img.shields.io/github/v/release/10yun/cv-mobile-ui?style=flat-square)](https://gitee.com/cvjs/cv-mobile-ui/releases) 
-->
[![license](https://img.shields.io/github/license/10yun/cv-mobile-ui?style=flat-square)](https://en.wikipedia.org/wiki/MIT_License)

</div> 

---

<a href="https://cvjs.cn/cv-mobile-ui">官方文档：https://cvjs.cn/cv-mobile-ui</a>

## 特点优势

- cv-mobile-ui 是一款 高性能，简洁、快速开发的ui组件;
- 基于 uniapp开发
- 全端兼容的基于flex布局的、无dom的ui库
- 目前为止，在小程序和混合app领域，高性能的框架。
- 自动差量更新数据
- 可按需引入必要的扩展组件。
- 暂不支持使用 Vue.use() 的方式安装
- 基础 `devDependencies` 已内置，可按需进行修改
- 不依赖SCSS，您无需额外安装此插件。
- 去除sass写法，可提升安装sass扩展所需要的较长时间，
- 未来将会全面支持在nvue页面中使用

### 研发组

 cv-mobile-ui 的理念是“复制黏贴，极简快速使用”。  
 cv-mobile-ui 免费开源，无需授权，欢迎商用。  
 cv-mobile-ui 的发展也得到各个小伙伴的支持，我们一起为构建一个更加优秀的UI框架而努力。  


### 展望未来

cv-mobile-ui的目标是做成uniapp生态的标杆，自由且免费开源。

对此，cv-mobile-ui有清晰且明确的计划安排，cv-mobile-ui将会全面兼容nvue，适配暗黑模式，整合unicloud，加入更多组件和模板等。

但是，一个人的力量是不够的：
  1. **为了开源，理想和自由**，您可以加入cv-mobile-ui的研发工作组，我们一起并肩奋战。
  2. 如果您是做UI开发的同学，cv-mobile-ui同样欢迎，因为cv-mobile-ui需要内外兼修。
  3. 如果您是个积极活跃的人，那么也欢迎您加入cv-mobile-ui的QQ群成为管理员。


### 版权信息

cv-mobile-ui 遵循[MIT](https://en.wikipedia.org/wiki/MIT_License)开源协议，意味着您无需支付任何费用，也无需授权，即可将 cv-mobile-ui 应用到您的产品中