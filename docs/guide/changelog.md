# 更新日志

## 2023-07-30

> 版本：v0.5.19
* 文档优化
* cv-mobile-ui 兼容 vue3
* 内置 `uni-ui`，升级到 `1.4.28`，改造成纯css模式
* 内置 `uview-ui` ，升级到 `2.0.36`，改造成纯css模式
* 内置 `uview-plus`，升级到 `3.1.34`，改造成纯css模式


## 2022-12-16
> 版本：v0.3.26
* 文档升级 vitepress

## 2022-12-12
> 版本：v0.3.26
* 优化form相关组件，向pc使用方式靠齐
* 删除 `cv-nav-group`

## 2021-09-28
> 版本： v-0.3.22
* 优化 ` request.js ` 组件，使 uniapp 支持 `PATCH` 请求方式 ，以及 post情况下的`PUT、PATCH、DELETE`

## 2021-09-06
> 版本： v-0.3.19
* 优化 ` picker ` 组件，`watch`监听value变化 
  
## 2021-08-26
> 版本： v-0.3.18
* 优化 ` cv-upload-image ` 上传图片组件 `apiFunc` 优化上传api方法外置
* 增加 ` cv-dialog-share ` 分享弹窗组件

## 2021-05-14
> 版本： v-0.3.13
* 增加 ` cv-geo-region ` 城市组件 `onChoose` 选中获取到的数据

## 2021-05-12
> 版本： v-0.3.12
* 增加 ` LibClass `类 `getConfig`、`setKey`、`setType` 方法

## 2021-04-25
* 默认单选bug

## 2021-03-19
* 文档优化

## 2021-03-11
> 版本： v-0.3.2
* 【导航】组件名称调整
* 更新 `uni-ui ` 库

## 2021-03-09
> 版本： v-0.3.1
* 文档更新
* 品牌全新升级
* 组件名称调整

## 2020-12-22
> 版本： v-0.2.15 [将会是cv-mobile-ui的0.2.x最后一个版本]
* `cv-editor-quill` 组件移除在线图标库，改用本地图标库


## 2020-12-22
> 版本： v-0.1.115
* 优化 `cv-draw-qrcode` 组件部分Android机型不兼容问题
  
## 2020-12-14
> 版本： v-0.1.110
* 优化 `cv-date-base` 组件初始化时间不生效问题
* 优化 `cv-datetime-base` 组件初始化时间不生效问题

## 2020-12-14
> 版本： v-0.1.109
* 优化 `cv-geo-local` 组件：新增 `location` 属性，用于初始化地图中心点位置

## 2020-12-06
> 版本： v-0.1.106
* 优化 `cv-tab-lists` 组件：接口返回数据为空时不触发 `dataLists` 事件问题

## 2020-12-09
> 版本： v-0.1.104
* 优化 `cv-upload-image` 组件：`z-index` 改为 997

## 2020-12-08
> 版本： v-0.1.101
* 优化 `cv-tab-lists` 组件：新增 `dataLists` 事件，用于接收当前列最新数据

## 2020-12-01
> 版本： v-0.1.100
* 优化 `cv-picker2` 组件：如果传默认值，不会再自动选择第一个
* 优化 `cv-picker3` 组件：如果传默认值，不会再自动选择第一个
* 优化 `cv-block` 组件：新增 `margin` 、 `marginLeft` 、 `marginRight` 、 `marginTop` 、 `marginBottom` 字段，可以控制组件的外边距

## 2020-11-10
> 版本： v-0.1.88
* 优化 `cv-dialog-bottom` 默认对齐方式为左对齐

## 2020-10-20
> 版本： v-0.1.85
* 【新增】
  +  `cv-banner`、`cv-banner-card` 组件，新增事件 `change` ,current 改变时会触发 change 事件
  +  `cv-banner-card` 组件，新增属性 `margin` ,前后边距，可用于露出前后一项的一小部分，接受 px 和 rpx 值
  +  `cv-banner-card` 组件，新增属性 `imageMode` ,图片裁剪、缩放的模式

## 2020-10-20
> 版本： v-0.1.82
* 【优化】
  +  优化 `cv-tab-lists` 在APP中的兼容性


## 2020-10-19
> 版本： v-0.1.80
* 【优化】
  +  优化 `cv-geo-region` 、 `cv-picker3` 在小程序中的兼容性

## 2020-10-17
> 版本： v-0.1.78
* 【新增】
  +  `cv-banner`、`cv-banner-card` 组件，新增字段 `height` ,用于自定义轮播的高度，默认值：160，单位：px
  +  新增组件 `cv-draw-posters`，可以把多张图片或者文本资源组合到一起，可用于海报自动化生成


## 2020-10-16
> 版本： v-0.1.72
* 【优化】
  + 优化 `cv-cell-row` 组件绑定click事件时的样式
* 【新增】
  +  `cv-block` 组件，新增点击触发 `click` 事件

## 2020-10-15
> 版本： v-0.1.69
* 【优化】
  + 优化部分场景下ios收起键盘没有返回输入框内容问题

## 2020-10-15
> 版本： v-0.1.67

* 【优化】
  + 优化 `cv-specs` sku匹配规则算法
  + 优化 `cv-tab-lists` 兼容问题


## 2020-10-13
> 版本： v-0.1.65

* 【新增】
  + 新增 `cv-specs` 组件，一般用于商品规格选择


## 2020-09-28
> 版本： v-0.1.60

* 【BUG】
  + 修复 `cv-tab-lists` 在android设备中兼容性问题


## 2020-09-22
> 版本： v-0.1.58

* 【BUG】
  + 修复 `cv-draw-qrcode` 参数 `loadMake` 设置无效问题


## 2020-09-21
> 版本： v-0.1.55

* 【新增】
  + 新增 `cv-input-btn` 带按钮的文本输入框
* 【BUG】
  + 修复 `cv-picker1` 初始化默认值为空时显示不正确的bug


## 2020-09-16
> 版本： v-0.1.52

* 【优化】
  + `cv-cell-row` 新增具名插槽（before），主要用于配合图标使用

## 2020-09-11
> 版本： v-0.1.51

* 【新增】
  + 新增 `cv-rate` 评分组件，多用于商品评价打分、服务态度评价、用户满意度等场景

## 2020-09-10
> 版本： v-0.1.50

* 【优化】
  + 优化表单中 `labelWidth` ,当值为 `0` 时，不会再渲染 `label` 区域，右边输入区域宽度会默认100%

## 2020-09-09
> 版本： v-0.1.48

* 【优化】
  + `cv-input-address` 更行为 `cv-geo-local` ,并优化小程序支持能力
  + `cv-code-sms` 新增 `apiFunc` 字段，用于设置验证码请求函数
  + 优化 `cv-tab-lists` 适配不同大小屏幕的能力
  + 优化 `cv-draw-qrcode` 初始化时也会生成二维码
  + 优化 `cv-draw-progress` 字体单位，现统一为：px
  + `cv-treaty` 新增 `@click` ,并把字段 `treatyUrl` 改为为： `url`


## 2020-09-08
> 版本： v-0.1.43

* 【优化】
  + `cv-input-address` 新增 `@location` 字段，用于监听组件选择地址的所有信息
  + `cv-input-address` 新增 `isShowMap` 字段，用于控制是否显示地图预览

## 2020-09-07
> 版本： v-0.1.41

* 【优化】
  + `cv-icons` 新增 `rotate` 字段，用于控制图标是否旋转

## 2020-09-04
> 版本： v-0.1.40

* 【优化】
  + 优化 `cv-grid-item` 支持微信小程序的能力
  + 优化 `cv-tab-lists` 支持微信小程序的能力
  + 优化 `cv-datetime-base` 小程序体验
  + 优化 `cv-geo-region` 小程序体验
  + `cv-tab-lists` 字段 `publicFunc` 默认改为：`null`
  + 组件名称统一改为首字母大写驼峰式命名
  + `cv-nac-col` 新增 `imgPadding` 字段 ；控制图片内边框

## 2020-09-03
> 版本： v-0.1.31

* 【优化】
  + 优化 `cv-icons` 图标库命名
  + `cv-icons` 图标库新增 媒体 分类
  + `cv-tab-lists` 新增 `publicFunc` 和 `tabsConfig[key].apiFlag` 字段，其中 `apiFlag` 是  `publicFunc` 的第一个参数（非必填）

## 2020-09-02
> 版本： v-0.1.28

* 【优化】
  + `cv-cell-row` 组件中 `url` 类型改为 `String` ；
  + `cv-cell-row` 组件中新增 `urlType` 字段和 `@click` 事件；[详见](/components/layout/cell-row)
  + `cv-grid-item` 组件中 `url` 类型改为 `String` ；
  + `cv-grid-item` 组件中新增 `urlType` 字段和 `@click` 事件；[详见](/components/basic/nav-col)
  + `cv-tab-lists` 组件中字段 `piblicParam` 更名为：`publicParam`
  + `cv-tab-lists` 组件中新增字段 `tabIndex` ：手动定位当前显示列
  + `cv-tab-lists` 组件中新增事件 `@change` ：监听tab变动
  + `cv-tab-lists` 组件中下拉加载提示新增点击加载更多
  + 优化 `cv-icons` 图标库，新增大量图标并分类为：基础、箭头、状态、钱包、订单、行为、人物等

## 2020-09-01
> 版本： v-0.1.25

* 【优化】
  + `cv-tab-lists` 组件中新增 `tabConfig[index].nodataMsg` 属性；用于当前列空数据占位内容
  + `cv-grid-item` 新增 `imgMaxSize` 字段，用于限制图片最大宽度，默认值40，单位（px）
  + `cv-grid-item` 插槽内容也可以水平垂直居中了
  + 优化 `cv-tab-lists` 组件tab区域布局问题
  + `cv-grid-item` 自动点击和间距
* 【新增】
  + 新增 `cv-draw-barcode` 组件，条形码生成。
  + 新增 `cv-draw-qrcode` 组件，二维码生成。
  + 新增 `cv-draw-post` 组件，海报生成。
  + 新增 `cv-draw-progress` 组件，圆形进度生成。
  + 新增 `cv-skeleton` 组件，骨架屏。

## 2020-08-31
> 版本： v-0.1.14

* 【新增】
  + 新增 `cv-editor-quill` 组件，富文本编辑器。
* 【优化】
  + 去除 `cv-dialog-full` 内容居中对齐样式


## 2020-08-28
> 版本： v-0.1.12

* 【新增】
  + 新增 `cv-tab-lists` 字段 `piblicParam` ,用于批量设置 `tabConfig` 的 `apiParam` 参数
  + 新增 `cv-search` 字段 `disableRealTime` ，用于禁止输入框内容实时传出
* 【优化】
  + 优化 `cv-search` 点击方法绑定，props `onSearch` 绑定方式改为 `@search` 接收方式,参数为输入框当前内容
  + 优化 `cv-from-item` 布局，新增 `box-sizing: unset`

## 2020-08-25
>  版本：v-0.1.3

* 【修复】
  + 修复 `cv-picker3` 默认值无效问题
  + 修复 `cv-cell-row` 右侧提示内容排版问题
* 【新增】
  + 新增 `cv-nav-group` 字段 `label`
  + 新增 `cv-banner-card` 卡片式轮播组件



## 2020-08-23
>  版本：v-0.0.40 [将会是cv-mobile-ui的0.0.x最后一个版本]

* 【优化】
  + `cv-info` 新增infoAlign字段
  + 减少 `cv-box` 组件内部结构层级
  + 减少 `cv-block` 组件内部结构层级
  + `cv-tab-list` 组件字段 `getUrl` 改为 `apiUrl` , `Param` 改为 `apiParam` 
  + `cv-search` 组件背景色改为白色，字体颜色改为黑色
  + 修改 `cv-grid-item` 垂直顶部对齐


## 2020-08-20
>  版本：v-0.0.37

* 【修复】
  + 修复滑块 `cv-switch` 的 `labelWidth` 参数不生效问题
  + 修复 `cv-box` 组件 cvBoxMainStyle 未定义问题
* 【新增】
  + icons图标预览可复制功能，点击图标即可复制 `type` 名称或 `unicode`


## 2020-08-19
>  版本：v-0.0.35

* 【修复】
  + `<cv-info>` 加入混入并修复命名错误

* 【优化】
  + 修改.vuepress左边菜单栏宽度
  + 分离.vuepress手册菜单
 


## 2020-08-18
>  版本：v-0.0.28

* 【新增】 
  + `<cv-box> <cv-block>` 布局组件
  + `<cv-dialog-loading>` 弹框加载中
  + `<cv-dialog-bottom>` 底部弹窗
  + `<cv-dialog-full>` 全屏弹窗
* 【修复】
  + 修复 `cv-picker1` 字段 `dataLists` 中对象重组为空的bug 

## 2020-08-16 
>  版本：v-0.0.24    

- 【新增】 `<cv-datetime-linkage>` 日期时间  五级联动 


## 2020-08-15 
>  版本：v-0.0.22    

- 【新增】 cv-mobile-ui 文档手册
- [bug]处理 `<cv-geo-region />` 组件命名问题

## 2020-08-14 
>  版本：v-0.0.21     

- 【新增】 `<cv-geo-region />` 三级联动城市选择