# 启动时同步加载配置


启动时候，同步加载配置


- 创建项目/src/plugins/bindGlobal/base-init.js

```js
import Vue from "vue";

Vue.prototype.$onLaunched = new Promise((resolve) => {
  Vue.prototype.$isResolve = resolve;
});

```

- 项目/src/App.vue

```vue
<script>
export default{
  onLaunch(){
    // 加载配置
    console.log('---加载 1---');
    this.initLoadConfig();
  },
  methods:{
    async initLoadConfig(){
      console.log('---加载 2---');
      await localRemoteConf1();
      await localRemoteConf2();
      await localRemoteConf3();
      this.$isResolve();
      console.log('---加载 3---');
    }
  }
}
</script>
```

- 项目/src/pages/index.vue

```vue
<template>

</template>
<script>
export default{
  async onLoad(){
    uni.showLoading({ title: '加载中' });
    await this.$onLaunched;
    console.log('---加载 4---');
    uni.hideLoading();
  },
}
</script>
```

刷新执行后，控制台会依次输出

```html
---加载 1---
---加载 2---
---加载 3---
---加载 4---
```


### 参考
