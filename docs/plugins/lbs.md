# LBS位置服务

### 简要


**方法说明**

在后台配置应用的参数

**参数说明**

| 实例参数 | 作用             | 必填 | 默认      |
| :------- | :--------------- | :--- | :-------- |
| key      | 开放平台身份验证 | 是   |
| type     | 开放平台类型     | 否   | qq 、amap |


### 获取定位 getLocation


- 请求方式： GET  
- 描述

根据用户ip地址获取当前市级位置

- 参数说明
    
| 参数 | 类型   | 是否必填 | 默认 | 说明                                                                           |
| :--- | :----- | :------- | :--- | :----------------------------------------------------------------------------- |
| ip   | String | 否       |      | 需要搜索的IP地址（仅支持国内）若用户不填写IP，则取客户http之中的请求来进行定位 |

  
```js

HttpObj.flagGet('SDK_MAP_COMMON_GET_LOCATION',{
  ip: '福建省',
}).then((res) => {
})

```


### 获取地区

- 请求方式： GET  
- 描述

获取全部行政区划数据  

- 参数说明
    
| 参数    | 类型   | 是否必填 | 说明       |
| :------ | :----- | :------- | :--------- |
| keyword | string | 是       | 查询关键字 |
  
- 示例

```js

HttpObj.flagGet('SDK_MAP_COMMON_GET_DISTRICT',{
  keyword: '福建省',
}).then((res) => {
})

```


### 获取路线规划方案

- 请求方式： GET  
- 参数说明  
  
| 参数           | 类型   | 是否必填 | 说明            |
| :------------- | :----- | :------- | :-------------- |
| form_latitude  | String | 是       | 起点坐标 - 维度 |
| form_longitude | String | 是       | 起点坐标 - 经度 |
| to_latitude    | String | 是       | 终点坐标 - 维度 |
| to_longitude   | String | 是       | 终点坐标 - 经度 |

> 其他参数请参考以及交通工具类型请参考 
> [腾讯地图](https://lbs.qq.com/service/webService/webServiceGuide/webServiceRoute),
> [高德地图](https://lbs.amap.com/api/webservice/guide/api/direction)

- 示例  

```js

HttpObj.flagGet('SDK_MAP_COMMON_GET_ROUTES',{
  type: 'bicycling',
  form_latitude:'119.318390',
  form_longitude:'26.074054',
  to_latitude: '119.318390',
  to_longitude: '26.074054,',
}).then((res) => {
  this.routes = res.result.routes;
})

```