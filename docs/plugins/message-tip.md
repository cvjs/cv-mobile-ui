# MessageTip消息提示

### 简要

>对其pc使用方式


### MessageTip 配置项

| 属性     | 类型   | 说明                                             | 默认 |
| :------- | :----- | :----------------------------------------------- | :--- |
| message  | string | 消息文字                                         | -    |
| type     | string | 消息类型：可选 【info、success、warning、error】 | info |
| duration | number | 显示时间，单位为毫秒。 设为 0 则不会自动关闭     | 2000 |


### MessageTip 方法

| 方法名  | 描述     | 说明   |
| :------ | :------- | :----- |
| info    | 默认提示 | 无图标 |
| success | 成功提示 | 有图标 |
| warning | 警告提示 | 有图标 |
| error   | 错误提示 | 有图标 |


### 使用说明

```js
import MessageTip from '@10yun/cv-mobile-ui/plugins/MessageTip.js';

// 可以传入 string 类型参数
MessageTip('我是一个提示');
// 可以传入 object 类型参数，object参数详见【配置项】
MessageTip({
  message:'我是一个提示'
});
// 可以直接使用方法
MessageTip.info('message');
MessageTip.success('message');
MessageTip.warning('message');
MessageTip.error('message');

```

### 默认提示

以下几种调用方式，展示相同

```js

// 方式1
MessageTip('默认提示');

// 方式2
MessageTip({
  message:'默认提示'
});
MessageTip({
  message:'默认提示'
  type:'info'
});
// 方式3（推荐） 
MessageTip.info('默认提示'); 

```

### 成功提示

以下几种调用方式，展示相同

```js

// 方式1 
MessageTip({
  message:'成功'
  type:'success'
});
// 方式2 
MessageTip.success('成功'); 

```

### 警告提示

以下几种调用方式，展示相同

```js

// 方式1 
MessageTip({
  message:'警告'
  type:'warning'
});
// 方式2 
MessageTip.warning('警告'); 

```

### 错误提示

以下几种调用方式，展示相同

```js

// 方式1 
MessageTip({
  message:'错误'
  type:'error'
});
// 方式2 
MessageTip.error('错误'); 

```


### 全局使用

如果需要绑定全局

添加全局 ` 项目/src/plugins/bindGlobal/MessageTip.js ` 

```js
import MessageTip from '@10yun/cv-mobile-ui/plugins/MessageTip.js';
Vue.prototype.MessageTip = MessageTip;
```


在用到的地方调用
```js

this.MessageTip.xxxx('/xxx/xxx');

```