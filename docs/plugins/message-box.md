# MessageBox消息弹窗

### 包含功能

| 方法    | 作用     | 说明 |
| :------ | :------- | :--- |
| alert   | 消息提示 |      |
| confirm | 消息确认 |      |
| prompt  | 提交内容 |      |


### alert 消息确认框

- 参数说明
    
| 参数        | 类型   | 是否必需 | 说明         | 默认 |
| :---------- | :----- | :------- | :----------- | :--- |
| content     | string | 是       | 确认内容     | -    |
| title       | string |          | 标题         | -    |
| confirmText | string |          | 确认按钮标题 |      |

- 原型
```js
alert(content, title, confirmText).then((alertRes)=>{
  console.log('点击确认了') 
})
```

- 示例

```js
import MessageBox from '@10yun/cv-mobile-ui/plugins/MessageBox.js';

MessageBox.alert('提示内容', '提示标题').then(() => {
  console.log('点击了确认');
});
```

### confirm 消息确认框

- 参数说明
    
| 参数    | 类型   | 是否必需 | 说明     | 默认 |
| :------ | :----- | :------- | :------- | :--- |
| content | string | 是       | 确认内容 | ？？ |
| title   | string |          | 标题     | 提示 |

- 原型
```js
confirm(content, title).then((confirmRes)=>{
  console.log('点击确认了') 
})
```

- 示例

```js
import MessageBox from '@10yun/cv-mobile-ui/plugins/MessageBox.js';

MessageBox.confirm('确认内容', '确认标题').then(() => {
  console.log('点击了确认');
}).catch(() => {
  console.log('点击了取消');
});
```

### prompt 消息输入

- 参数说明
    
| 参数    | 类型   | 是否必需 | 说明           | 默认   |
| :------ | :----- | :------- | :------------- | :----- |
| content | string | 否       | 默认输入的内容 |        |
| title   | string | 否       | 输入标题       | 请输入 |

- 原型
```js
prompt(content, title).then((promptRes)=>{
  console.log('点击确认了') 
})
```

- 示例

```js
import MessageBox from '@10yun/cv-mobile-ui/plugins/MessageBox.js';

MessageBox.prompt().then((promptRes) => {
  console.log('输入的内容', promptRes.content);
});
```



### 全局使用

如果需要绑定全局

添加全局 ` 项目/src/plugins/bindGlobal/MessageBox.js ` 

```js
import MessageBox from '@10yun/cv-mobile-ui/plugins/MessageBox.js';
Vue.prototype.MessageBox = MessageBox;
```


在用到的地方调用
```js

this.MessageBox.xxxx('/xxx/xxx');

```