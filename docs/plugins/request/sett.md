# 请求配置


### 接口flag配置


**1、自定义接口flag**  

创建接口配置文件 ` 项目/src/api/index.js ` 

```js
export default {
  API_FLAG111: '/v1/login/xxxx',
  API_FLAG222: '/v1/goods'
  ...
};
```

**2、[flag:ur]参考对应的业务接口文档**  


### 局部调用

添加扩展 ` 项目/src/plugins/http.js ` 

```js  
// 加载接口flag配置
import ApiFlagSet from '@/api/index.js';
// 加载插件
import RequestClass from '@10yun/cv-mobile-ui/plugins/request.js';

// 有些flag配置是请求后获取到的，需要重新获取 flag
function getFlagAll(){
  let ApiFlagAll = {};
  // 可以获取flag缓存
  ApiFlagAll = Object.assign(ApiFlagAll, ApiFlagSet || {});
  // console.log(ApiFlagAll);
  return ApiFlagAll;
}


// 实例对象
var HttpObj = new RequestClass({
  baseURL: 'https://api.xxx.com/',
  flagFunc: getFlagAll,
  flagMap: getFlagAll(),
  storeHandle: $store,//store句柄
  needMethods: ['PATCH'],
  headers: {
    'Token': uni.getStorageSync('syCacheAppToken'),
    'ClientPlatform': uni.getSystemInfoSync().platform || '',
    'Authorization': uni.getStorageSync('syCacheAppJwt'),
  },
  requests: {},
});

export default HttpObj;

```

在用到的地方调用

```js
import HttpObj from "@/plugins/http.js";

HttpObj.方法(参数);

```


### 全局调用

如果需要绑定全局

添加全局 ` 项目/src/plugins/bindGlobal/http.js ` 

```js

import Vue from 'vue';

import HttpObj from "@/plugins/http.js";

Vue.prototype.$request = HttpObj;

```

在用到的地方调用
```js

this.$request.方法(参数);

```