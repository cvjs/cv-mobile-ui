# url 请求方式

### 简要

- url调用，特殊未定义flag时候

### 参数
    
- 当参数2个时

| 参数  | 类型   | 是否必需 | 说明                  |
| :---- | :----- | :------- | :-------------------- |
| url   | string | 是       | api接口标识           |
| param | object | 是       | api发送参数，默认空{} |

方法如下：
```js
// 模式1
urlXxx(url,param).then((succRes)=>{   });
// 模式2，一般不用
urlXxx(url)(param).then((succRes)=>{   });
```


- 当参数3个时
  
| 参数  | 类型   | 是否必需 | 说明                         |
| :---- | :----- | :------- | :--------------------------- |
| url   | string | 是       | api接口标识                  |
| id    | int    | 否       | flag转url拼接成  /xxx/xxx/id |
| param | object | 是       | api发送参数，默认空{}        |

方法如下：
```js
// 模式1
urlXxx(url,id,param).then((succRes)=>{   });
// 模式2，一般不用
urlXxx(url,id)(param).then((succRes)=>{   });
```


- 1、`v1/order/xxx` 为不含基础路径的接口，RequestClass 会自动补齐
- 2、`https://api.xxx.com/v1/order/xxx` 如果写全接口，会跳过补齐
- 3、`param* ` 为请求参数

### urlGet

```js
import HttpObj from "@/plugins/http.js";

HttpObj.urlGet('/xxx/xxxx/xxx/xx?saxxx',{
    key1 : val1,
    key2 : val3,
    ...更多key : ...更多val
}).then((succRes)=>{

});
```



### urlPost
 
```js
import HttpObj from "@/plugins/http.js";

HttpObj.urlPost('/xxx/xxxx',{
    key1:val1,
    key2:val3,
    ...更多key : ...更多val
}).then((succRes)=>{

});
```


### urlPut

如 urlGet、urlPost调用


### urlPatch

如 urlGet、urlPost调用


### urlDel

如 urlGet、urlPost调用