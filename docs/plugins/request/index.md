# RequestClass

### 简要

- 数据请求
- 基于 uni.request 封装


### 构造参数

| 参数        | 类型     | 说明                                       | 示例 |
| :---------- | :------- | :----------------------------------------- | :--- |
| baseURL     | String   | 接口请求的基础url路径                      |      |
| flagFunc    | Function | 重新获取flag的方法                         |      |
| flagMap     | Object   | flag方式请求需配置                         |      |
| needMethods | Array    | 需要转换的methods，支持 PUT、PATCH、DELETE |      |
| headers     | Object   | 每次请求携带的 header 变量                 |      |
| requests    | Object   | 每次请求携带的 request 变量                |      |

### 方法

| 方式       | 说明                              | 示例                                     |
| :--------- | :-------------------------------- | :--------------------------------------- |
| setFlag    | 设置flag                          |                                          |
| setHeader  | 设置header                        |                                          |
| setRequest | 设置request                       |                                          |
| flagGet    | GET请求,获取多条、一条            | [查看](/plugins/request/flag#flagget)    |
| flagPost   | POST请求，提交、新增、更新 、删除 | [查看](/plugins/request/flag#flagpost)   |
| flagPut    | PUT请求，修改全部字段，更新数据   | [查看](/plugins/request/flag#flagput)    |
| flagPatch  | PATCH请求，修改单个字段，更新部分 | [查看](/plugins/request/flag#flagpatch)  |
| flagDel    | DELETE请求，删除数据              | [查看](/plugins/request/flag#flagdel)    |
| flagUpImg  | POST上传图片                      |                                          |
| urlGet     | GET请求数据                       | [查看](/plugins/request/url.html#urlget) |
| urlPost    | POST提交数据                      | [查看](/plugins/request/url)             |
| urlPut     | PUT更新数据                       |                                          |
| urlPatch   | PATCH更新部分                     |                                          |
| urlDel     | DELETE删除数据                    |                                          |
| urlUpImg   | POST上传图片                      |                                          |


### restfule 接口风格

| 请求method | 接口 url | 前端方法                  | 说明                                             |
| :--------- | :------- | :------------------------ | :----------------------------------------------- |
| GET        | /zoos    | flagGet(flag,param)       | 列出所有动物园                                   |
| GET        | /zoos/ID | flagGet (flag,id)         | 获取某个指定动物园的信息                         |
| POST       | /zoos    | flagPost (flag,param)     | 新建一个动物园                                   |
| POST       | /zoos/ID | flagPost (flag,id,param)  | 更新一个动物园                                   |
| PUT        | /zoos/ID | flagPut (flag,id,param)   | 更新某个指定动物园的信息(提供该动物园的全部信息) |
| PATCH      | /zoos/ID | flagPatch (flag,id,param) | 更新某个指定动物园的信息(提供该动物园的部分信息) |
| DELETE     | /zoos/ID | flagDel (flag,id)         | 删除某个动物园                                   |
 