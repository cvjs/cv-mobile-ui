# StorageClass 

### 简要

>数据存储
>统一前缀

### 构造参数

| 参数   | 类型   | 说明 | 示例 |
| :----- | :----- | :--- | :--- |
| prefix | String | 前缀 |      |
| suffix | String | 后缀 |      |

### 方法

| 方式        | 说明                                   | 示例 |
| :---------- | :------------------------------------- | :--- |
| setAsync    | 异步设置缓存，同 uni.setStorage        |      |
| setSync     | 同步设置缓存，同 uni.setStorageSync    |      |
| getAsync    | 异步获取缓存，同 uni.getStorage        |      |
| getSync     | 同步获取缓存，同 uni.getStorageSync    |      |
| removeAsync | 异步删除缓存，同 uni.removeStorage     |      |
| removeSync  | 同步删除缓存，同 uni.removeStorageSync |      |
| clearAsync  | 异步清空缓存，同 uni.clearStorage      |      |
| clearSync   | 同步清空缓存，同 uni.clearStorageSync  |      |



### 局部调用

添加扩展 ` 项目/src/plugins/storage.js ` 

```js
import StorageClass from '@10yun/cv-mobile-ui/plugins/storage.js';
// 缓存ID名称
var appID = process.env.VUE_APP_SY_PROID || '';
// 实例对象
var StorageObj = new StorageClass({
  // 自定义的，统一缓存key前缀
  prefix: `sy${appID}_`,
  suffix: ''
});

export default StorageObj;

```

在用到的地方调用

```js
import StorageObj from "@/plugins/storage.js";

StorageObj.方法(参数);

```

### 全局调用

如果需要绑定全局

添加全局 ` 项目/src/plugins/bindGlobal/storage.js ` 

```js

import Vue from 'vue';

import StorageObj from "@/plugins/storage.js";

Vue.prototype.$storage = StorageObj;

```

在用到的地方调用
```js

this.$storage.方法(参数);

```


### 设置缓存

```js
import StorageObj from '@/plugins/storage.js';

// 异步
StorageObj.setAsync('cacheKey',data);
// 同步
StorageObj.setSync('cacheKey',data);

```

### 读取缓存

```js
import StorageObj from '@/plugins/storage.js';

// 异步
StorageObj.getAsync({
  key:'cacheKey',
  success:(cacheRes)=>{
  }
});
// 同步
StorageObj.getSync('cacheKey');

```

### 删除缓存

```js
import StorageObj from '@/plugins/storage.js';

// 异步，方式1
StorageObj.removeAsync('cacheKey');
// 异步，方式2
StorageObj.removeAsync({
  key:'cacheKey',
  success:(cacheRes)=>{
  }
});
// 同步
StorageObj.removeSync('cacheKey');

```

### 清空缓存

```js
import StorageObj from '@/plugins/storage.js';

// 异步
StorageObj.clearAsync();
// 同步
StorageObj.clearSync();

```