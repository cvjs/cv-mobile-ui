export default [
  {
    text: '基础',
    collapsed: false,
    items: [
      { text: '跳转', link: '/plugins/jumps' },
      { text: '缓存', link: '/plugins/storage' },
      { text: '消息提示', link: '/plugins/message-tip' },
      { text: '消息弹窗', link: '/plugins/message-box' }
    ]
  },
  {
    text: '网络请求',
    collapsed: false,
    items: [
      { text: '介绍', link: '/plugins/request/' },
      { text: '配置', link: '/plugins/request/sett' },
      { text: 'flag 请求（推荐）', link: '/plugins/request/flag' },
      { text: 'url 请求', link: '/plugins/request/url' }
    ]
  },
  {
    text: '位置服务',
    link: '/plugins/lbs'
  }
];
