## uni支持的扩展工具

| 类名         | 作用     |
| :----------- | :------- |
| StorageClass | 缓存     |
| MessageTip   | 消息提示 |
| MessageBox   | 消息框   |
| RequestClass | 请求     |
| LbsClass     | 位置服务 |
