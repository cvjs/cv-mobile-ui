# 全局方法

### 简要

>

### 参数配置

``` /config.js ```

| 参数       | 作用             | 说明 |
| :--------- | :--------------- | :--- |
| page_home  | 设定首页         |      |
| page_my    | 设定个人中心页面 |      |
| page_login | 设定登录         |      |
| page_xieyi | 协议             |      |


### 方法

| 全局方法   | 作用       | 说明                                    |
| :--------- | :--------- | :-------------------------------------- |
| jumpsPage  | 页面       | 跳转到普通页面                          |
| jumpsAuth  | 鉴权页面   | 跳转前，先鉴定是否登录                  |
| jumpsHome  | 首页       | 跳转 首页，需要注册实例时，注册进去     |
| jumpsMy    | 个人中心   | 跳转 个人中心，需要注册实例时，注册进去 |
| jumpsLogin | 登录       | 跳转 登录                               |
| jumpsPre   | 上一个页面 |                                         |
| jumpsBack  | 上一个页面 |                                         |


### 调用

```vue

<template>
  <!-- 在 html 中使用 -->
  <view @click="jumpsPage('xxxx')">跳转</view>
  <view @click="jumpsAuth('xxxx')">跳转</view>
  <view @click="jumpsXxxx('xxxx')">跳转</view>
</template>
<script>
// 【⚠️注意】：这里无需 import 引入了
export default {
  data() {
    return {
      // 【⚠️注意】：这里也不用引入
    }
  },
  methods: {
    function1(){
      // 在js中使用
      uni.jumpsPage('xxxx');
      uni.jumpsAuth('xxxx');
      uni.jumpsXxxx('xxxx'); 
    }
  }
}
</script>

```
 
