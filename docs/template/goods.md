# 商品列表模板


一般用于商品列表排版，默认为实心白色背景。

### 示例1

<CvCodePreview src="/pages/template/tpl_goods1">

<<< @/../src/pages/template/tpl_goods1.vue

</CvCodePreview>

### 示例2

<CvCodePreview src="/pages/template/tpl_goods2">

<<< @/../src/pages/template/tpl_goods2.vue

</CvCodePreview>


### 示例3

<CvCodePreview src="/pages/template/tpl_goods3">

<<< @/../src/pages/template/tpl_goods3.vue

</CvCodePreview>

### 示例4

<CvCodePreview src="/pages/template/tpl_goods4">

<<< @/../src/pages/template/tpl_goods4.vue

</CvCodePreview>