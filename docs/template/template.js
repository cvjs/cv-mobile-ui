export default [
  {
    text: '常用模板',
    collapsed: false,
    items: [
      { text: '商品列表', link: '/template/goods' },
      { text: '优惠券', link: '/template/coupons' }
    ]
  }
];
