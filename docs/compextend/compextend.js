export default [
  {
    text: '扩展组件',
    collapsed: false,
    items: [
      { text: 'uni-ui', link: '/compextend/ui/uni-ui' },
      { text: 'uview-ui', link: '/compextend/ui/uview-ui' },
      { text: 'uview-plus', link: '/compextend/ui/uview-plus' }
    ]
  },
  {
    text: '扩展组件-sdk',
    collapsed: false,
    items: [
      { text: 'u-chart', link: '/compextend/sdk/u-chart' },
      { text: 'webview-main', link: '/compextend/sdk/webview-main' },
      { text: '使用授权', link: '/compextend/sdk/privacy-policy' }
    ]
  }
];
