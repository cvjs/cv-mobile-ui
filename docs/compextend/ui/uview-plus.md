# uView-plus

- 内置 `uview-plus` , 如有侵权请联系作者

### 配置 easycom

```json
// pages.json
{
  "easycom": {
    "autoscan": true,
    "custom": {
      "^u-(.*)": "@10yun/cv-mobile-ui/uview-plus/components/u-$1/u-$1.vue"//uni-ui相关组件 可不引用
    }
  },
	// 此为本身已有的内容
	"pages": [
		// ......
	]
}
```