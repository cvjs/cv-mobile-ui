# uView-ui


- 内置 `uView-ui` , 如有侵权请联系作者

uView-ui 已经改造成 纯css模式

>推荐vue3  
>推荐使用 uview-plus

### 主要改造方式


```js

import uView from 'uview-ui'
// 引入的包名改成
import uView from '@10yun/cv-mobile/uview-ui'

```

### 配置 easycom

```json
// pages.json
{
  "easycom": {
    "autoscan": true,
    "custom": {
      "^u-(.*)": "@10yun/cv-mobile-ui/uview-ui/components/u-$1/u-$1.vue"
    }
  },
	// 此为本身已有的内容
	"pages": [
		// ......
	]
}
```



### 使用方法


- 默认单位配置
```js

// main.js，注意要在use方法之后执行
import uView from '@10yun/cv-mobile/uview-ui'
Vue.use(uView)
// 如此配置即可
uni.$u.config.unit = 'rpx'


```

- 修改uView内置配置方案

```js

// main.js
import uView from '@10yun/cv-mobile/uview-ui'
Vue.use(uView)

// 调用setConfig方法，方法内部会进行对象属性深度合并，可以放心嵌套配置
// 需要在Vue.use(uView)之后执行
uni.$u.setConfig({
	// 修改$u.config对象的属性
	config: {
		// 修改默认单位为rpx，相当于执行 uni.$u.config.unit = 'rpx'
		unit: 'rpx'
	},
	// 修改$u.props对象的属性
	props: {
		// 修改radio组件的size参数的默认值，相当于执行 uni.$u.props.radio.size = 30
		radio: {
			size: 15
		}
		// 其他组件属性配置
		// ......
	}
})
```