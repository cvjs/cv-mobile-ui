# uni-ui

- 内置 `uni-ui` , 如有侵权请联系作者


### 配置 easycom

```json
// pages.json
{
  "easycom": {
    "autoscan": true,
    "custom": {
      // uni-ui 规则如下配置
      "^uni-(.*)": "@10yun/cv-mobile-ui/uni-ui/lib/uni-$1/uni-$1.vue"
    }
  },
	// 此为本身已有的内容
	"pages": [
		// ......
	]
}
```