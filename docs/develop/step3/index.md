# 进入开发



1、下载相关依赖

```sh
npm install 
```

2、启动

```sh
npm run dev:h5
```

3、配置页面

根据需求配置页面
```sh
npm run cv:pages
```
配置问页面后，执行`cv:pages`生成对应的聚合 `page.json` 和 权限 文件 `page-auth.js`,
如果是没有创建页面的话，会默认生成空页面模版


4、根据 UI 组件进行开发

## 内置
 
- `cv:pages` ：更新pages.js,若不使用模块化（已内置）请勿使用此指令,[pages模块化文档]
