# 应用分包  pages 模块化配置

* 用户pages模块化，分别为三类配置： common，main，sub
* 【分包】`*-sub`为当前分包所有路径配置，多个分包配置文件会自动合并，建议配置文件放到当前分包文件夹内；
* 【主包】`*-main`为主包所有路径配置，多个主包配置文件会自动合并，建议配置文件放到主包 [tabBar] 文件夹内；
* 【通用配置】`*-common`;支持配置：`globalStyle`、`tabBar`、`easycom`、 `preloadRule`、`condition`；多个通用配置文件会自动合并；建议配置文件放到主包 [tabBar] 文件夹内；
* 路径文件不存在时自动创建vue文件，模板文件：`build/themp.vue`
* 内置命令 `npm run cv:pages`

#### 安装

``` shell
npm install @10yun/cv-build -D
```
::: tip package：@10yun/cv-build
内含 `pages` 和 `publish` 两个脚本

`pages` ：pages模块化配置聚合脚本；最终聚合内容会覆盖掉 `/src/pages.json` 原有内容  
`publish` ：npm包发布脚本，自动自增版本号并发布到npm库，其中包内容需放置 `/packages` 内；支持多包管理 `/packages/<packages_name>`
:::

#### 配置

```json
"scripts": {
    "cv:pages": "node node_modules/@10yun/cv-build/lib/pages/build-pages.js",//pages配置聚合脚本
    "cv:publish": "node node_modules/@10yun/cv-build/lib/publish/build-publish.js <packages_name>",//npm包自动发布脚本,packages_name为可选参数
}
```

#### 使用

``` shell
npm run cv:pages #执行pages配置聚合脚本
npm run cv:publish #执行npm包自动发布脚本
#或
# node node_modules/@10yun/cv-build/lib/pages/build-pages.js
# node node_modules/@10yun/cv-build/lib/publish/build-publish.js <packages_name>
```

##### 目录结构
~~~
├── pages
    ├── subPackages1        //分包一
    |   ├── pages-sub.json // 分包路径配置
    |   ...                //你自己的项目文件
    ├── subPackages2       //分包二
    |   ├── pages-sub.json // 分包路径配置
    |   ...                //你自己的项目文件
    ├── taBar                 //主包
    |   ├── pages-main.json //主包所有路径配置
    |   ├── pages-bar.json //globalStyle、tabBar和easycom的配置
    |   ...                //你自己的项目文件

~~~

**pages-main.json**

```json
[
    {
        "path": "pages/tabBar/index/index",
        "style": {
            "navigationBarTitleText": "首页"
        }
    },
    {
        "path": "pages/tabBar/member/member",
        "style": {
            "navigationBarTitleText": "成员"
        }
    }
    
    
]
```

**pages-sub.json**

```json
{
    {
        "path": "pages/tabBar/index/index",
        "style": {
            "navigationBarTitleText": "首页"
        }
    },
    {
        "path": "pages/tabBar/member/member",
        "style": {
            "navigationBarTitleText": "成员"
        }
    }
}
```

**pages-common.json**

```json
{
    "globalStyle": {
        "navigationBarTextStyle": "black",
        "navigationBarTitleText": "uni-app",
        "navigationBarBackgroundColor": "#F8F8F8",
        "backgroundColor": "#F8F8F8"
    },
    "tabBar": {
        "color": "#7A7E83",
        "selectedColor": "#3cc51f",
        "borderStyle": "black",
        "backgroundColor": "#ffffff",
        "list": [
            {
                "pagePath": "pages/tabBar/index/index",
                "iconPath": "static/tabbar/index.png",
                "selectedIconPath": "static/tabbar/index_cur.png",
                "text": "首页"
            },
            {
                "pagePath": "pages/tabBar/member/member",
                "iconPath": "static/tabbar/member.png",
                "selectedIconPath": "static/tabbar/member_cur.png",
                "text": "我的"
            }
        ]
    },
    "condition":{},
    "preloadRule":{},
    "easycom": {
        "autoscan": true,
        "custom": {
        }
    }
}
```