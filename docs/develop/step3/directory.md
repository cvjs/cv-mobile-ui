# 目录结构


~~~

cv-mobile-base                    WEB部署目录（或者子目录）
|
├── build
|   ├── pages.js                    pages模块化脚本文件，新手勿改
|   └─  themp.vue                   pages模块化配置自动生成vue文件的模板文件
├── public
├── src                             uni项目根目录
|   ├── components                  自定义组件目录
|   ├── pages                       项目页面目录
|   |   ├── subPackages             分包目录，可以有多个分包
|   |   |   └─  page-sub.json       分包pages配置，空路径会自动生成vue模板文件，一般一个分包只需要一个
|   |   └─  tabBar                  主包目录，一般只放tabBar页面
|   |       ├── page-common.json    通用pages配置
|   |       └─  page-main.json      主包pages配置
|   ├── static                      项目静态资源目录
|   ├── template                    项目模板目录
|   ├── App.vue                     项目默认首页主要文件
|   ├── main.js                     项目的入口文件
|   ├── manifest.json               应用的配置文件，用于指定应用的名称、图标、权限等。
|   └─  pages.json                  页面文件的路径、窗口表现、设置多 tab 等【使用pages模块化不用手动修改pages.json】
├── package.json
└─  postcss.config.js


~~~