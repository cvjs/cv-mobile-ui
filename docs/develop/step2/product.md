# 创建产品
 

### npm下载模版

- 1、快捷创建项目

```sh

# 进入代码项目目录
cd www_uniapp
# 创建 移动端 项目
npm create cvjs my-cv-uniapp-app 

# 2、选择框架
> 移动端项目

# 3、选择模版
> uniapp主应用

# 4、进入项目
cd my-cv-uniapp-app 

# 5、下载安装依赖
pnpm install 

# 6、运行启动项目
npm run dev:h5

```

### 手动下载模版

模版代码查看：[模版代码](https://gitee.com/cvjs/cv-create-project/tree/master/template/uniapp-app)
