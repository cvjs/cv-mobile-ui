# 创建应用

>基于 uniapp 开发
>webview技术开发


### npm下载模版

- 1、快捷创建项目

```sh

# 进入代码项目目录
cd www_uniapp
# 创建 移动端 项目
npm create cvjs my-cv-uniapp-micro 

# 2、选择框架
> 移动端项目

# 3、选择模版
> uniapp子应用

# 4、进入项目
cd my-cv-uniapp-micro 

# 5、下载安装依赖
pnpm install 

# 6、运行启动项目
npm run dev:h5

```

### 手动下载模版

模版代码查看：[模版代码](https://gitee.com/cvjs/cv-create-project/tree/master/template/uniapp-micro)


### 配置

- 1、引入 `plugins/webview-sub.js` webview通讯class
- 2、修改 ` src/utils/mixins-init.js `

```js

import pagesDiyPlus from '@/utils/pages-diy-plus.js';
export default {
  data() {
    return {
      isJumpsAuth: this.$wvs ? true : false,
    }
  },
  onLoad() {

  },
  onShow() {
    if (!this.isJumpsAuth) {
      this._initAuthenticate();
    }
  },
  ......
}
```

- 3、修改 ` src/App.vue `

```js

export default {
  onShow: function () {
    /**
     * 子应用加载
     */
    if (this.$wvs) {
      this.$nextTick(() => {
        this.$wvs.subReceiveParent();
      });
    }
  },
  ......
}
```