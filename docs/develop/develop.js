export default [
  {
    text: '1、开发环境', // 必要的
    collapsed: false,
    items: [
      { text: '基础环境配置', link: '/develop/step1/' },
      { text: '安装vscode', link: '/develop/step1/vscode' },
      { text: '自动补齐ui标签', link: '/develop/step1/snippets' }
    ]
  },
  {
    text: '2、创建项目', // 必要的
    collapsed: false,
    items: [
      {
        text: '选择类型', // 必要的
        items: [
          { text: 'uni产品', link: '/develop/step2/product' },
          { text: '微应用', link: '/develop/step2/micro' },
          { text: '小程序', link: '/develop/step2/mini' }
        ]
      },
      { text: '配置', link: '/develop/step2/peizhi' },
      {
        text: '微应用',
        items: [
          { text: 'webview通讯', link: '/develop/micro/webview' }
          // { text: '配置', link: '/develop/step2/peizhi' },
          // { text: '配置', link: '/develop/step2/peizhi' }
        ]
      }
    ]
  },
  {
    text: '3、进行开发', // 必要的
    items: [
      { text: '介绍', link: '/develop/step3/' },
      { text: '目录结构', link: '/develop/step3/directory' },
      { text: '分包', link: '/develop/step3/subpackage' }
    ]
  },
  {
    text: '4、应用发布', // 必要的
    items: [
      { text: '发布h5', link: '/develop/step4/h5' },
      { text: '发布安卓', link: '/develop/step4/android' }
    ]
  }
  // {
  //   text: '快速使用',
  //    collapsed: false,
  //   items: [
  //     // {  text: '快速安装',link: '/develop/install/', },
  //     { text: 'npm安装',link: '/develop/install/npm',  },
  //     { text: 'uniapp插件市场安装',link: '/develop/install/plugin',  },
  //   ]
  // },
  // {
  //   text: '模块化开发',
  //    collapsed: false,
  //   items: [
  //     // { text: '介绍', link: '/develop/basepk/' },
  //     { text: '目录结构', link: '/develop/basepk/directory' },
  //     { text: '配置', link: '/develop/basepk/module' },
  //   ]
  // },
];
