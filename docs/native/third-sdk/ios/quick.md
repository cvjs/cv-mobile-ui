### iOS 模块配置
[iOS 模块配置](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/android)    


### Geolocation（定位）
[百度定位](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/geolocation?id=%e7%99%be%e5%ba%a6%e5%ae%9a%e4%bd%8d)    
[高德定位](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/geolocation?id=%e9%ab%98%e5%be%b7%e5%ae%9a%e4%bd%8d)    
[系统定位](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/geolocation?id=%e7%b3%bb%e7%bb%9f%e5%ae%9a%e4%bd%8d)    


### Push（消息推送）
[uniPush](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/push?id=unipush)    
[个推推送](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/push?id=%e4%b8%aa%e6%8e%a8%e6%8e%a8%e9%80%81)    


### Share（分享）
[微信分享](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/share?id=%e5%be%ae%e4%bf%a1%e5%88%86%e4%ba%ab)    
[QQ分享](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/share?id=qq%e5%88%86%e4%ba%ab)    
[新浪微博分享](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/share?id=%e6%96%b0%e6%b5%aa%e5%be%ae%e5%8d%9a%e5%88%86%e4%ba%ab)    


### Oauth（登录鉴权）
[一键登录](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/oauth?id=%e4%b8%80%e9%94%ae%e7%99%bb%e5%bd%95%ef%bc%88univerify%ef%bc%89)    
[微信登录](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/oauth?id=%e5%be%ae%e4%bf%a1%e7%99%bb%e5%bd%95)    
[QQ登录](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/oauth?id=qq%e7%99%bb%e5%bd%95)    
[新浪微博登录](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/oauth?id=%e6%96%b0%e6%b5%aa%e5%be%ae%e5%8d%9a%e7%99%bb%e5%bd%95)    
[苹果登录](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/oauth?id=%e8%8b%b9%e6%9e%9c%e7%99%bb%e5%bd%95)    


### Map（地图）
[百度地图](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/map?id=%e7%99%be%e5%ba%a6%e5%9c%b0%e5%9b%be)  
[高德地图](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/map?id=%e9%ab%98%e5%be%b7%e5%9c%b0%e5%9b%be)  


### Payment（支付）
[支付宝](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/pay?id=%e6%94%af%e4%bb%98%e5%ae%9d)  
[微信支付](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/pay?id=%e5%be%ae%e4%bf%a1%e6%94%af%e4%bb%98)  
[苹果应用内购支付](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/pay?id=%e8%8b%b9%e6%9e%9c%e5%ba%94%e7%94%a8%e5%86%85%e8%b4%ad%e6%94%af%e4%bb%98)  


### Speech（语音输入）
[百度语音](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/speech?id=%e7%99%be%e5%ba%a6%e8%af%ad%e9%9f%b3)  
[讯飞语音](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/iOSModuleConfig/speech?id=%e8%ae%af%e9%a3%9e%e8%af%ad%e9%9f%b3)  