## 低功耗蓝牙权限配置

### Androidmainfest.xml文件需要修改的项

**feature节点**
```json
<feature name="Bluetooth" value="io.dcloud.feature.bluetooth.BluetoothFeature"/>
```
#### 需要在application节点前添加权限

```json
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />,
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />,
<uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />,
<uses-permission android:name="android.permission.BLUETOOTH" />
```