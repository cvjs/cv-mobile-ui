### Android 模块配置
[Android 模块配置](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/android)

### Geolocation（定位）
[百度定位](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/geolocation?id=%e7%99%be%e5%ba%a6%e5%ae%9a%e4%bd%8d)  
[高德定位](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/geolocation?id=%e9%ab%98%e5%be%b7%e5%ae%9a%e4%bd%8d)  
[系统定位](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/geolocation?id=%e7%b3%bb%e7%bb%9f%e5%ae%9a%e4%bd%8d)  


### Push（消息推送）
[uniPush](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/push)  
[个推推送](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/push?id=%e4%b8%aa%e6%8e%a8%e6%8e%a8%e9%80%81)  
[谷歌推送](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/push?id=%e8%b0%b7%e6%ad%8c%e6%8e%a8%e9%80%81)  
[GooglePlay渠道配置](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/push?id=googleplay%e6%b8%a0%e9%81%93%e9%85%8d%e7%bd%ae)  


### Share（分享）
[微信分享](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/share)  
[QQ分享](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/share?id=qq%e5%88%86%e4%ba%ab)  
[新浪微博分享](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/share?id=%e6%96%b0%e6%b5%aa%e5%be%ae%e5%8d%9a%e5%88%86%e4%ba%ab)  


### Oauth（登录鉴权）
[一键登录](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/oauth)  
[微信登录](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/oauth?id=%e5%be%ae%e4%bf%a1%e7%99%bb%e5%bd%95)  
[QQ登录](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/oauth?id=qq%e7%99%bb%e5%bd%95)  
[新浪微博登录](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/oauth?id=%e6%96%b0%e6%b5%aa%e5%be%ae%e5%8d%9a%e7%99%bb%e5%bd%95)  
[小米登录](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/oauth?id=%e5%b0%8f%e7%b1%b3%e7%99%bb%e5%bd%95)  
 

### Map（地图）
[百度地图](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/map?id=%e7%99%be%e5%ba%a6%e5%9c%b0%e5%9b%be)  
[高德地图](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/map?id=%e9%ab%98%e5%be%b7%e5%9c%b0%e5%9b%be)  


### Payment（支付）
[支付宝](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/pay?id=%e6%94%af%e4%bb%98%e5%ae%9d)  
[微信支付](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/pay?id=%e5%be%ae%e4%bf%a1%e6%94%af%e4%bb%98)  


### Speech（语音输入）
[百度语音](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/speech?id=%e7%99%be%e5%ba%a6%e8%af%ad%e9%9f%b3)  
[讯飞语音](https://nativesupport.dcloud.net.cn/AppDocs/usemodule/androidModuleConfig/speech?id=%e8%ae%af%e9%a3%9e%e8%af%ad%e9%9f%b3)  