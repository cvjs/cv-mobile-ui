## 相机权限配置

### Androidmainfest.xml文件需要修改的项

#### 需要在application节点前添加权限

```json
<uses-feature android:name="android.hardware.camera"/>,
<uses-feature android:name="android.hardware.camera.autofocus"/>,
<uses-permission android:name="android.permission.CAMERA"/>,
```