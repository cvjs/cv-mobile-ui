export default [
  {
    text: 'uniapp三方SDK配置',
    items: [
      {
        text: 'Android 模块配置',
        items: [
          { text: '官方文档合集', link: '/native/third-sdk/android/quick' },
          { text: '照相机', link: '/native/third-sdk/android/camera' },
          { text: '低功耗蓝牙', link: '/native/third-sdk/android/bluetooth' }
        ]
      },
      {
        text: 'iOS 模块配置',
        items: [{ text: '官方文档合集', link: '/native/third-sdk/ios/quick' }]
      }
    ]
  }
];
