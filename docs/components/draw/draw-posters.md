# cv-draw-posters 绘制海报

### 简要

>可以把图片文字等元素合并到一起

### 示例

<CvCodePreview src="/pages/extend/cv-draw-posters">

<<< @/../src/pages/extend/cv-draw-posters.vue

</CvCodePreview>

### 属性

| 属性名     | 类型   | 必填 | 描述               |
| ---------- | ------ | ---- | ------------------ |
| width      | Number | 是   | 画布宽度 (单位:px) |
| height     | Number | 是   | 画布高度(单位:px)  |
| background | String | 否   | 画布背景颜色       |
| radius     | Number | 否   | 圆角               |
| views      | Array  | 否   | 海报的所有元素     |

#### `views.type='text'`

| 字段          | 类型            | 必填 | 描述                                              |
| ------------- | --------------- | ---- | ------------------------------------------------- |
| type          | String          | 是   | 类型，值：text                                    |
| width         | Number(单位:px) | 是   | 宽度                                              |
| height        | Number(单位:px) | 否   | 高度                                              |
| left          | Number(单位:px) | 否   | 距离海报左边距                                    |
| top           | Number(单位:px) | 否   | 距离海报上边距                                    |
| fontSize      | Number(单位:px) | 否   | 字体大小，默认：16                                |
| lineHeight    | Number(单位:px) | 否   | 行高，默认：20                                    |
| breakWord     | Boolean         | 否   | 是否自动换行，默认：false                         |
| bolder        | Boolean         | 否   | 是否加粗，默认：false                             |
| textAlign     | String          | 否   | 对齐方式，可选值：left、center、right，默认：left |
| color         | String          | 否   | 字体颜色                                          |
| content       | String          | 是   | 文本内容                                          |
| MaxLineNumber | Number          | 否   | 显示多少行，超出省略                              |

#### `views.type='rect'`
| 字段       | 类型            | 必填 | 描述                                                    |
| ---------- | --------------- | ---- | ------------------------------------------------------- |
| type       | String          | 是   | 类型，值：rect                                          |
| width      | Number(单位:px) | 是   | 宽度                                                    |
| height     | Number(单位:px) | 是   | 高度                                                    |
| left       | Number(单位:px) | 否   | 距离海报左边距                                          |
| top        | Number(单位:px) | 否   | 距离海报上边距                                          |
| radius     | Number(单位:px) | 否   | 圆角半径，如果radius === width / 2，则是个圆，和CSS一样 |
| background | String          | 否   | 填充背景色                                              |

#### `views.type='image'`
| 字段   | 类型            | 必填 | 描述                                                    |
| ------ | --------------- | ---- | ------------------------------------------------------- |
| type   | String          | 是   | 类型，值：image                                         |
| tailor | Number(单位:px) | 否   | 裁剪方式，可选值：center                                |
| radius | Number(单位:px) | 否   | 圆角半径，如果radius === width / 2，则是个圆，和CSS一样 |
| width  | Number(单位:px) | 是   | 宽度                                                    |
| height | Number(单位:px) | 是   | 高度                                                    |
| left   | Number(单位:px) | 否   | 距离海报左边距                                          |
| top    | Number(单位:px) | 否   | 距离海报上边距                                          |
| url    | String          | 是   | 图片路径                                                |


### 事件

| 事件名  | 返回值               | 说明               |
| :------ | :------------------- | ------------------ |
| success | successData          | 海报生成成功时触发 |
| error   | 海报生成失败错误信息 | 海报生成失败时触发 |

##### successData

```json
{
    height: 394,
    path: "",//H5时，路径为base64
    width: 700,
}
```

### 感谢

