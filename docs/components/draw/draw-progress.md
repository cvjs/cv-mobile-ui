# cv-draw-process 绘制环形进度

### 简要

>绘制环形进度

### 示例

<CvCodePreview src="/pages/extend/cv-draw-progress">

<<< @/../src/pages/extend/cv-draw-progress.vue

</CvCodePreview>


### 属性

| 属性名        | 类型    | 默认值                  | 说明                                                                                               |
| :------------ | :------ | :---------------------- | -------------------------------------------------------------------------------------------------- |
| canvasId      | String  | cv-draw-progress-canvas | canvasId，页面存在多个条形码组件时需设置不同的ID                                                   |
| cSize         | Number  | 80                      | 圆的大小 单位px 取偶数                                                                             |
| cBackground   | String  | '#f4f4f4'               | 进度条背景底色                                                                                     |
| cColor        | String  | '#fe9b25'               | 进度条颜色                                                                                         |
| lineWidth     | Number  | 8                       | 进度条线的宽度   注意：因为圆的半径设置为画布大小的一半-2 所以注意进度条线的粗细会不会超过圆的大小 |
| cBili         | Number  | 100                     | 进度条的占比                                                                                       |
| cPlaceContent | Boolean | true                    | 是否显示默认提示内容                                                                               |





### 感谢

