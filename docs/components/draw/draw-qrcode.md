# cv-draw-qrcode 绘制二维码

### 简要

>绘制二维码

### 示例

<CvCodePreview src="/pages/extend/cv-draw-posters">

<<< @/../src/pages/extend/cv-draw-posters.vue

</CvCodePreview>

### 属性

| 属性名          | 类型    | 默认值                | 可选值 | 说明                                             |
| :-------------- | :------ | :-------------------- | :----- | :----------------------------------------------- |
| cid             | String  | cv-draw-qrcode-canvas |        | canvasId，页面存在多个条形码组件时需设置不同的ID |
| unit            | String  | px                    | upx    | 单位                                             |
| show            | Boolean | true                  |        | 默认使用组件中的image标签显示条形码              |
| val             | String  |                       |        | 要生成的内容                                     |
| background      | String  | #ffffff               |        | 背景色                                           |
| onval           | Boolean | false                 |        | 监听val值变化自动重新生成条形码                  |
| loadMake        | Boolean | false                 |        | 组件初始化完成后自动生成条形码，val需要有值      |
| foreground      | String  | #000000               |        |                                                  |
| pdground        | String  | #000000               |        |                                                  |
| icon            | String  |                       |        |                                                  |
| iconSize        | Number  | 40                    |        |                                                  |
| size            | Number  | 200                   |        |                                                  |
| lv              | Number  | 3                     |        |                                                  |
| usingComponents | Boolean | true                  |        |                                                  |
| showLoading     | Boolean | true                  |        |                                                  |
| loadingText     | String  | '二维码生成中'        |        |                                                  |



### 事件

| 事件名 | 返回值                         | 说明                                    |
| :----- | :----------------------------- | --------------------------------------- |
| result | 生成的图片base64或图片临时地址 | 返回条形码路径 注：_clearCode()后返回空 |


### 感谢

