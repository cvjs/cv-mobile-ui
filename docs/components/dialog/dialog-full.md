# cv-dialog-full 模态弹窗 - 全屏

### 简要

>全屏过度弹入弹出  
>app中兼容性不是很好，无法遮挡导航栏、状态栏、tabBar菜单；
 
### 示例

<CvCodePreview src="/pages/dialog/cv-dialog-full">

<<< @/../src/pages/dialog/cv-dialog-full.vue

</CvCodePreview>
 
### 属性

| 属性名 | 类型    | 默认值 | 说明         |
| :----- | :------ | :----- | :----------- |
| show   | Boolean | false  | 是否显示弹窗 |


### 事件

| 事件名  | 说明             | 回值    |
| :------ | :--------------- | :------ |
| confirm | 监听点击确定按钮 | Bollean |
| cancel  | 监听点击取消按钮 | Bollean |
