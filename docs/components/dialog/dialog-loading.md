# cv-dialog-loading 弹框加载中

### 简要

>弹框加载中
 
### 示例

<CvCodePreview src="/pages/dialog/cv-dialog-loading">

<<< @/../src/pages/dialog/cv-dialog-loading.vue

</CvCodePreview>

### 属性

| 属性名  | 类型   | 默认值           | 说明     |
| :------ | :----- | :--------------- | :------- |
| img     | String | /static/logo.png | 图片路径 |
| content | String | 加载中...        | 描述问题 |

