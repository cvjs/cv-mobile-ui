# cv-dialog-bottom 模态弹窗-底部

### 简要

>底部过度弹入弹出
 
### 示例

<CvCodePreview src="/pages/dialog/cv-dialog-bottom">

<<< @/../src/pages/dialog/cv-dialog-bottom.vue

</CvCodePreview>

### 属性

| 属性名 | 类型    | 默认值 | 说明         |
| :----- | :------ | :----- | :----------- |
| show   | Boolean | false  | 是否显示弹窗 |


### 事件

|事件名			|说明				|返回值						|
|:---			|:---				|:---						|
|confirm	|监听点击确定或取消按钮	|Bollean|

