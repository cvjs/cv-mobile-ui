# cv-code-sms 短信验证码
  
### 简要

>验证码，一般和表单配合使用，默认为实心白色背景。

- ` HttpObj ` 插件查看[网络请求](/plugins/request/sett)

### 示例

<CvCodePreview src="/pages/form/cv-code-sms">

<<< @/../src/pages/form/cv-code-sms.vue

</CvCodePreview>

### 属性

| 属性名      | 类型     | 默认值 | 说明                           |
| :---------- | :------- | :----- | :----------------------------- |
| v-model     | String   | -      | 数据双向绑定                   |
| disabled    | String   | false  | 是否禁止                       |
| maxlength   | String   | -1     | 验证码长度                     |
| mobile      | String   | -      | 发送的手机号码                 |
| mobileParam | String   | mobile | 发送手机号字段给后台接受的字段 |
| codeSign    | Boolean  | false  | 短信识别码                     |
| apiFunc     | Function | -      | 验证码请求函数                 |
| waitTime    | String   | 60     | 验证码发送成功后等待时间（s）  |
