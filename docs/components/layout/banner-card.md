# cv-banner-card 卡片式幻灯片

### 简要

>卡片式轮播图，可自定义卡片大小和内容等，默认为透明背景。
 
### 示例

<CvCodePreview src="/pages/layout/cv-banner">

<<< @/../src/pages/layout/cv-banner.vue

</CvCodePreview>

### 属性

| 属性名        | 类型    | 默认值     | 说明                                                                       |
| :------------ | :------ | :--------- | :------------------------------------------------------------------------- |
| dataLists     | Array   | -          | 图片数组                                                                   |
| height        | Number  | 160        | 轮播的高度，单位：px                                                       |
| paramConfig   | Object  | -          | dataLists字段配置                                                          |
| indicatorDots | Boolean | true       | 是否显示面板指示点                                                         |
| margin        | string  | 35px       | 前后边距，可用于露出前后一项的一小部分，接受 px 和 rpx 值                  |
| imageMode     | string  | aspectFill | 图片裁剪、缩放的模式[详见(mode)](https://uniapp.dcloud.io/component/image) |

- [ `paramConfig` ]参数明细

| 字段名 | 类型   | 默认值 | 说明     |
| :----- | :----- | :----- | :------- |
| image  | String | image  | 图片地址 |
| text   | String | text   | 底部文字 |

### 事件

| 属性名  | 说明                                                                                |
| :------ | :---------------------------------------------------------------------------------- |
| @change | current 改变时会触发 change 事件，event.detail = {current: current, source: source} |
