# cv-cell-row 单行单元格

### 简要

>列表页，一般用户列表业务使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/layout/cv-cell-row">

<<< @/../src/pages/layout/cv-cell-row.vue

</CvCodePreview>
 

### 属性


| 属性名      | 类型   | 默认值 | 说明       |
| :---------- | :----- | :----- | :--------- |
| leftIcon    | String | -      | 左边图片   |
| leftTop     | String | -      | 左边标题   |
| leftBottom  | String | -      | 左边副标题 |
| rightTop    | String | -      | 右边标题   |
| rightBottom | String | -      | 右边副标题 |
| rightIcon   | String | -      | 右边箭头   |

### 插槽

| 具名        | 说明                           |
| :---------- | :----------------------------- |
| leftIcon    | 一般用于：使用图标代替图片场景 |
| leftTop     |                                |
| leftBottom  |                                |
| rightTop    |                                |
| rightBottom |                                |
| rightIcon   | 一般用于：使用图标代替图片场景 |

### 事件
| 事件名 | 说明 | 返回值        |
| :----- | :--- | :------------ |
| @click | 点击 | Cell 触发事件 |

