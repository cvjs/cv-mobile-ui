
# cv-box 宫格布局

### 简要

>`cv-box` 和 `cv-block 两者配合使用效果更佳
 
### 示例

```vue
<template>
  <view>
    <cv-box>
      <!-- 此处可以是任意内容 -->
    </cv-box>
  </view>
</template>
<script>
export default {
  data() {
    return {

    };
  }
}
</script>
```


### 属性

| 属性名          | 类型   | 默认值  | 说明                     |
| :-------------- | :----- | :------ | :----------------------- |
| margin          | Number | 10      | 外边框大小（px）         |
| padding         | Number | 5       | 内边距大小（px）         |
| backgroundColor | String | #ffffff | 背景色，支持所有背景写法 |
