
# cv-message 文本提示

### 简要

>文本提示，一般和表单配合使用，默认为实心白色背景、红色字体。
 
### 示例

```vue
<template>
  <view>
    <cv-message message="请认真填写" />
  </view>
</template>
<script>
export default {
  data() {
    return {
    };
  },
  methods: {
  }
}
</script>
```


### 属性

| 属性名  | 类型   | 默认值  | 说明     |
| :------ | :----- | :------ | :------- |
| message | String | -       | 文本提示 |
| color   | String | #ff0000 | 文本颜色 |
