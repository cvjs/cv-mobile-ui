# cv-banner 幻灯片

### 简要

>普通幻灯片，可自定义图片大小和内容等，默认为透明背景。
 
### 示例

<CvCodePreview src="/pages/layout/cv-banner">

<<< @/../src/pages/layout/cv-banner.vue

</CvCodePreview>

### 属性

| 属性名        | 类型    | 默认值 | 说明                 |
| :------------ | :------ | :----- | :------------------- |
| dataLists     | Array   | -      | 图片数组             |
| height        | Number  | 160    | 轮播的高度，单位：px |
| paramConfig   | Object  | -      | dataLists字段配置    |
| indicatorDots | Boolean | true   | 是否显示面板指示点   |

- [ `paramConfig` ]参数明细

| 字段名 | 类型   | 默认值 | 说明     |
| :----- | :----- | :----- | :------- |
| image  | String | image  | 图片地址 |
| text   | String | text   | 底部文字 |

### 事件

| 属性名  | 说明                                                                                  |
| :------ | :------------------------------------------------------------------------------------ |
| @change | current 改变时会触发 change 事件，event.detail = '{current: current, source: source}' |
