# cv-tab-lists 自带tab的列表页

### 简要

>自带tab的列表页，一般用户列表业务使用，默认为实心白色背景
 
### 示例

```vue
<template>
  <view>
    <cv-tab-lists :tabsConfig="tabsConfig">
      <template #default="{rows,index,tabIndex}">
        <!-- ... -->
      </template>
    </cv-tab-lists>
  </view>
</template>
<script>
export default {
  data() {
    return {
      tabsConfig: [{
        tabTitle: '切换卡',
        apiFunc: getData,
        apiParam: {}
      }],
    };
  },
  methods: {
    getData: (params) => {
      return new Promise((resolve, reject) => {
        let data = [];
        /* 模拟请求500毫秒拿到数据 */
        setTimeout(() => {
          if (params.page > 3) {
            resolve({ data: data });
          } else {
            for (let i = 0; i < params.pagesize; ++i) {
              data.push({ title: params.tabTitle, info: '此处为演示内容' });
            }
            resolve({ data: data });
          }
        }, 500)
      })
    }
  }
}
</script>
```

::: warning 注意
如果：需要在这个组件的插槽中使用其他组件  
那么：你需要单独 `import` 引用插槽内的组件  
:::

### 属性

| 属性名         | 类型     | 默认值 | 必填 | 说明                                                 |
| :------------- | :------- | :----- | :--- | :--------------------------------------------------- |
| tabsConfig     | Array    | -      | 是   | tab数组                                              |
| disableTouch   | Boolean  | false  | 否   | 是否关闭左右滑动，单列时建议开启                     |
| disableTab     | Boolean  | false  | 否   | 是否关闭tab区域，单列时建议开启                      |
| disableEnabled | Boolean  | false  | 否   | 是否禁止下拉刷新                                     |
| disableStyle   | Boolean  | false  | 否   | 禁止样式，true时改为 refresher-default-style ='none' |
| onUpdate       | Number   | -      | 否   | 监听更新数据通知 ,建议传入当前时间戳                 |
| listsClass     | String   | -      | 否   | 组件额外class                                        |
| publicParam    | Object   | -      | 否   | 每列数据请求接口通用 `param`                         |
| publicFunc     | Function | -      | 否   | 每列数据请求通用方法                                 |
| tabIndex       | Number   | 0      | 否   | 默认显示列的Key                                      |

#### tabsConfig 的结构

| 属性名    | 类型     | 默认值 | 说明                                              |
| :-------- | :------- | :----- | :------------------------------------------------ |
| tabTitle  | String   | -      | tab名称                                           |
| apiFunc   | Function | -      | 自定义方法；**小程序接收不到**                    |
| apiUrl    | String   | -      | api地址(不推荐使用)                               |
| apiParam  | Object   | -      | 接口请求地址的默认参数                            |
| apiFlag   | String   | -      | 接口请求地址的标识（当 `publicFunc`不为空时可选） |
| parseFunc | Function | -      | `props.publicFunc` 的第一个参数(非必填)           |
| ordersort | String   | DESC   | 排序策略['DESC','ASC']                            |
| pagesize  | Nember   | 10     | 页长                                              |
| nodataMsg | String   | -      | 没有数据提示符                                    |

### 事件


| 事件名| 说明| 返回值	|
| ---	| ---	| ---	|
| @change|监听tab变化|当前tab的key|
| @dataLists|监听tab请求数据的变化|当前tab的最新数据|

### default插槽接收参数 `{rows,index,tabIndex}` 说明

| 属性名| 说明| 
| :---:	| :---	| 
| rows|当前列的全部数据，来自你的API|
| index|当前列rows的index|
| tabIndex|当前列的index，|
