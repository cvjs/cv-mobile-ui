
# cv-block 布局

### 简要

>`cv-block` 和 `cv-box` 两者配合使用效果更佳
 
### 示例

```vue
<template>
  <view>
    <cv-box>
      <cv-block col="3">
        <!-- 插槽自定义内容 -->
      </cv-block>
      <cv-block col="3">
        <!-- 插槽自定义内容 -->
      </cv-block>
      <cv-block col="3">
        <!-- 插槽自定义内容 -->
      </cv-block>
    </cv-box>
  </view>
</template>
<script>
export default {
  data() {
    return {

    };
  }
}
</script>
```

### 属性

| 属性名       | 类型   | 默认值 | 说明            |
| :----------- | :----- | :----- | :-------------- |
| col          | Number | 1      | 宽度占比(1/col) |
| margin       | Number | 5      | 外面距离        |
| marginLeft   | Number | 5      | 左外面距离      |
| marginRight  | Number | 5      | 右外面距离      |
| marginTop    | Number | 5      | 上外面距离      |
| marginBottom | Number | 5      | 底外面距离      |

### 事件

| 事件名 | 说明     | 返回值 |
| :----- | :------- | :----- |
| @click | 点击触发 |        |
