
# cv-grid-item 网格菜单

### 简要

>网格菜单，可以配合 `cv-grid-group` 快速实现菜单布局
 
### 示例

<CvCodePreview src="/pages/layout/cv-grid-item">

<<< @/../src/pages/layout/cv-grid-item.vue

</CvCodePreview>

### 属性

| 属性名     | 类型   | 默认值     | 说明                                                         |
| :--------- | :----- | :--------- | :----------------------------------------------------------- |
| label      | String | -          | 字段名                                                       |
| img        | String | -          | 图片地址                                                     |
| url        | String | -          | 跳转路径                                                     |
| urlType    | String | navigateTo | 跳转类型：[navigateTo],[redirectTo]，[reLaunch]，[switchTab] |
| col        | Number | 1          | 横向占用空间 1/n                                             |
| imgMaxSize | Number | 40         | 定义图片最大宽度                                             |
| imgPadding | Number | 0          | 定义图片内边框                                               |

### 插槽

| 名称    | 默认值 | 说明                        |
| :------ | :----- | :-------------------------- |
| default | -      | 插槽内容会替代图片[img]参数 |

### 事件

| 事件名 | 说明 | 返回值        |
| :----- | :--- | :------------ |
| @click | 点击 | Cell 触发事件 |
