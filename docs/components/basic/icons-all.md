# cv-icons 图标

### 简要

>大量图标可供使用
 
[在线预览](https://cvjs.cn/h5-demo#/pages/extend/cv-icons)
 
### 示例

```vue
<template>
  <view>
    <cv-icons type="people-my" size="30" />
  </view>
</template>
<script>
export default {
  data() {
    return {
    };
  },
  methods: {
  }
}
</script>
```


### 属性

| 属性名 | 类型    | 默认值 | 说明               |
| :----- | :------ | :----- | :----------------- |
| size   | Number  | 24     | 图标大小           |
| type   | String  | -      | 图标图案，参考示例 |
| color  | String  | -      | 图标颜色           |
| rotate | Boolean | false  | 是否旋转           |

### 事件

| 事件名 | 说明 | 返回值        |
| :----- | :--- | :------------ |
| @click | 点击 | Icon 触发事件 |