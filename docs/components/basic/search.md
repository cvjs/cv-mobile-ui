# cv-search 搜索

### 简要

>搜索
 
### 示例

```vue
<template>
  <view>
    <cv-search v-model="formData.name" @search="onSearch">
      <!-- 左侧预留内容插槽 -->
    </cv-search>
  </view>
</template>
<script>
export default {
  data() {
    return {
      formData: {
        name: ""
      }
    };
  },
  methods: {
    /* 监听点击搜索按钮并获取搜索框内容 */
    onSearch(e) {
      console.log('搜索框内容：' + e);
    }
  }
}
</script>
```

### 属性

| 属性名          | 类型     | 默认值 | 说明                    |
| :-------------- | :------- | :----- | :---------------------- |
| v-model         | String   | -      | 双向绑定输入框数据      |
| placeholder     | String   | -      | 占位符内容              |
| onSearch        | Function | -      | 点击搜索触发事件        |
| disableRealTime | Boolean  | false  | 是否禁止实时更新v-model |

### 事件

|事件名	| 说明			|返回参数	|
| :--------------	| :--------------				| :--------------		|
| search	|点击`搜索`触发事件| 输入框内容		|
