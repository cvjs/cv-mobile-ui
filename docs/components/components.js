import componentsBasic from './_menu/components_basic.js';
import componentsLayout from './_menu/components_layout.js';
import componentsForm from './_menu/components_form.js';
import componentsData from './_menu/components_data.js';
import componentsDialog from './_menu/components_dialog.js';
import componentsDraw from './_menu/components_draw.js';

export default [componentsBasic, componentsLayout, componentsForm, componentsData, componentsDialog, componentsDraw];
