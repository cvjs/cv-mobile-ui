
# cv-datetime-linkage 日期时间选择 | 五联动

### 简要

>从底部弹起的滚动选择器 | 五联动  
>本组件一般用于 日期时间选择  
>日期时间 `五联动` 选择框，一般和表单配合使用，默认为实心白色背景。  
 
### 示例

<CvCodePreview src="/pages/form/cv-datetime-linkage">

<<< @/../src/pages/form/cv-datetime-linkage.vue

</CvCodePreview>

### 属性

| 属性名      | 类型   | 默认值 | 说明                            |
| :---------- | :----- | :----- | :------------------------------ |
| v-model     | String | -      | 数据双向绑定 (YYYY-MM-dd HH:ii) |
| placeholder | String | -      | 占位符                          |
| start       | String | -      | 开始时间(YYYY-MM-dd HH:ii)      |
| end         | String | -      | 结束时间(YYYY-MM-dd HH:ii)      |
