# cv-radio-opt-base 单选普通样式

### 简要

>本组件用于性别选择  
>性别单选框，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-radio-opt-base">

<<< @/../src/pages/form/cv-radio-opt-base.vue

</CvCodePreview>


### 属性

| 属性名  | 类型    | 默认值 | 说明     |
| :------ | :------ | :----- | :------- |
| value   | String  | -      | 选后的值 |
| label   | String  |        | 文本展示 |
| checked | Boolean | -      | 是否选择 |
