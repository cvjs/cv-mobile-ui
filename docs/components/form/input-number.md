
# cv-input-number 数字输入框

### 简要

>属性为`number`的input  
>数字输入键盘 |	均支持，注意iOS上app-vue弹出的数字键盘并非9宫格方式  
>数字输入框，一般和表单配合使用，默认为实心白色背景。  
 
### 示例

<CvCodePreview src="/pages/form/cv-input-number">

<<< @/../src/pages/form/cv-input-number.vue

</CvCodePreview>


### 属性

| 属性名      | 类型    | 默认值 | 说明                 |
| :---------- | :------ | :----- | :------------------- |
| v-model     | String  | -      | 数据双向绑定         |
| placeholder | String  | -      | 占位符               |
| disabled    | Boolen  | false  | 是否禁止             |
| maxlength   | Number  | -1     | 输入长度限制         |
| clearable   | Boolean | false  | 是否显示一键清空按钮 |
