
# cv-geo-local 带地图选择的地址输入框
  
### 简要

>带地图选择的地址输入框，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-geo-local">

<<< @/../src/pages/form/cv-geo-local.vue

</CvCodePreview>

### 属性

| 属性名      | 类型    | 默认值 | 说明                 |
| :---------- | :------ | :----- | :------------------- |
| v-model     | String  | -      | 数据双向绑定         |
| placeholder | String  | -      | 占位符               |
| disabled    | Boolen  | false  | 是否禁止             |
| maxlength   | Number  | -1     | 输入长度限制         |
| clearable   | Boolean | false  | 是否显示一键清空按钮 |
| isShowMap   | Boolean | false  | 是否显示地图预览     |
| location    | Object  | {}     | 初始化地图中心点位置 |
| scale       | Number  | 14     | 地图缩放等级         |


- [ `location` ]结构

| 属性名    | 类型   | 默认值 | 说明 |
| :-------- | :----- | :----- | :--- |
| latitude  | String | -      | 纬度 |
| longitude | String | -      | 经度 |

### 事件

| 事件名    | 说明 | 返回值   |
| :-------- | :--- | :------- |
| @location | 监听 | 地址信息 |

- [ `@location` ] 返回参数

| 属性名    | 说明                                                                  |
| :-------- | :-------------------------------------------------------------------- |
| name      | 位置名称                                                              |
| address   | 详细地址                                                              |
| latitude  | 纬度，浮点数，范围为-90~90，负数表示南纬，使用 gcj02 国测局坐标系。   |
| longitude | 经度，浮点数，范围为-180~180，负数表示西经，使用 gcj02 国测局坐标系。 |

