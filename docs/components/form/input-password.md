
# cv-input-password 输入框

### 简要

>密码输入  
>属性为`text`且 @password属性为`true`的input  
>本组件一般用于隐藏密码内容的输入框  
>密码输入框，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-input-password">

<<< @/../src/pages/form/cv-input-password.vue

</CvCodePreview> 

### 属性

| 属性名      | 类型    | 默认值 | 说明                 |
| :---------- | :------ | :----- | :------------------- |
| v-model     | String  | -      | 数据双向绑定         |
| placeholder | String  | -      | 占位符               |
| disabled    | Boolen  | false  | 是否禁止             |
| maxlength   | Number  | -1     | 输入长度限制         |
| clearable   | Boolean | false  | 是否显示一键清空按钮 |
