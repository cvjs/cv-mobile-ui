
# cv-input-digit 带小数点输入框

### 简要

>属性为`digit`的input 小数输入  
>带小数点输入框，一般和表单配合使用，默认为实心白色背景。  
>带小数点的数字键盘  |	App的nvue页面、微信、支付宝、百度、头条、QQ小程序
 
### 示例

<CvCodePreview src="/pages/form/cv-input-digit">

<<< @/../src/pages/form/cv-input-digit.vue

</CvCodePreview>


### 属性

| 属性名      | 类型    | 默认值 | 说明                 |
| :---------- | :------ | :----- | :------------------- |
| v-model     | String  | -      | 数据双向绑定         |
| placeholder | String  | -      | 输入框占位文本       |
| disabled    | Boolen  | false  | 是否禁止             |
| maxlength   | Number  | -1     | 输入长度限制         |
| clearable   | Boolean | false  | 是否显示一键清空按钮 |
