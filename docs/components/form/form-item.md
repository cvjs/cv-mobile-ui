# cv-form-item 

### 简要


 
### 示例

<CvCodePreview src="/pages/form/index">

<<< @/../src/pages/form/index.vue

</CvCodePreview>



### 属性

- 撒打算大

| 属性名         | 类型    | 默认值 | v                        |
| :------------- | :------ | :----- | :----------------------- |
| label          | String  | -      | 字段名                   |
| labelWidth     | Number  | 60     | label宽度，上下排列无效  |
| labelPosition  | String  | left   | 布局方式：可选 left、top |
| require        | Boolean | false  | 是否必填                 |
| message        | String  | -      | 错误提示                 |
| messageDisplay | Boolean | false  | 错误提示是否显示         |

### 插槽

| 插槽名 | 说明 | 插槽作用域 |
| :----- | :--- | :--------- |
| tip    | 提示 |            |