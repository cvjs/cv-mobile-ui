
# cv-input-text 文本输入框

### 简要

>type属性为`text`的input  
>文本输入框，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-input-text">

<<< @/../src/pages/form/cv-input-text.vue

</CvCodePreview> 
```

### 属性

| 属性名      | 类型    | 默认值 | 说明                 |
| :---------- | :------ | :----- | :------------------- |
| placeholder | String  | -      | 占位符               |
| disabled    | Boolen  | false  | 是否禁止             |
| v-model     | String  | -      | 数据双向绑定         |
| maxlength   | Number  | -1     | 输入长度限制         |
| rtip        | String  | -      | 输入框右侧占位提示   |
| clearable   | Boolean | false  | 是否显示一键清空按钮 |

### 插槽

| 插槽名 | 说明       | 插槽作用域 |
| :----- | :--------- | :--------- |
| append | 在后面追加 |            |
