
# cv-rate 评分

### 简要

>多用于商品评价打分、服务态度评价、用户满意度等场景，组件名：``cv-rate ``
 
### 示例

<CvCodePreview src="/pages/form/cv-rate">

<<< @/../src/pages/form/cv-rate.vue

</CvCodePreview> 

### 属性

| 属性名      | 类型    | 默认值  | 说明                 |
| :---------- | :------ | :------ | :------------------- |
| v-model     | Number  | 1       | 当前评分             |
| isFill      | Boolean | false   | 星星的类型，是否镂空 |
| color       | String  | #ececec | 星星的颜色           |
| activeColor | String  | #ffca3e | 星星选中状态颜色     |
| size        | Number  | 24      | 星星的大小           |
| max         | Number  | 5       | 最大评分             |
| margin      | Number  | 0       | 星星的间距           |
| disabled    | Boolean | false   | 是否可点击           |

