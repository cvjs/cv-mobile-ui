# cv-checkbox-group 多选框

### 简要

>多选框，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-checkbox-group">

<<< @/../src/pages/form/cv-checkbox-group.vue

</CvCodePreview>

### 属性

| 属性名      | 类型            | 默认值    | 说明                                         |
| :---------- | :-------------- | :-------- | :------------------------------------------- |
| v-model     | Array           | -         | 双向绑定数据,cv-checkbox-group 类型为:String |
| dataType    | String          | 'value'   | 数据类型 :['value','text']                   |
| dataLists   | [Array, String] | -         | 展示数据                                     |
| dataValue   | String          | 'value'   | dataLists中value的标识                       |
| dataLabel   | String          | 'label'   | dataLists中text的标识                        |
| dataChecked | String          | 'checked' | dataLists中checked的标识                     |
| max-number  | Number          | 0         | 最大选中数量                                 |
  