
# cv-geo-region 省市区联动选择框

### 简要

>从底部弹起的滚动选择器 | 省市区三级联动选择器  
>本组件一般用于 地区选择器  
>省市区联动选择框，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-geo-region">

<<< @/../src/pages/form/cv-geo-region.vue

</CvCodePreview>

### 属性

| 属性名      | 类型   | 默认值 | 说明                     |
| :---------- | :----- | :----- | :----------------------- |
| v-model     | String | -      | 数据双向绑定             |
| placeholder | String | -      | 占位符                   |
| dataType    | String | text   | 数据类型['value','text'] |

### 事件

| 事件名   | 参数   | 说明                                                     |
| :------- | :----- | :------------------------------------------------------- |
| onChoose | objArr | 返回由选中对象组成的数组，对象中包含该对象在数组中的索引 |


- [ `onChoose` ] 返回数据格式

```json
{ 
  "province": { "text": "福建省", "value": "350000","index":"" }, 
  "city": { "text": "福州市", "value": "350100","index":"" }, 
  "district": { "text": "鼓楼区", "value": "350102","index":"" } 
}
```