# cv-radio-opt-tag 单选标签样式

### 简要

>本组件一般用单项选择  
>单选标签，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-radio-opt-tag">

<<< @/../src/pages/form/cv-radio-opt-tag.vue

</CvCodePreview>

### 属性

| 属性名  | 类型    | 默认值 | 说明     |
| :------ | :------ | :----- | :------- |
| value   | String  | -      | 选后的值 |
| label   | String  |        | 文本展示 |
| checked | Boolean | -      | 是否选择 |

