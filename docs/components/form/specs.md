# cv-specs 规格选择

### 简要

>规格选择组件，一般用于商品规格选择，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-picker1">

<<< @/../src/pages/form/cv-picker1.vue

</CvCodePreview>

### 属性

| 属性名         | 类型    | 默认值 | 说明                |
| :------------- | :------ | :----- | :------------------ |
| specList       | Object  | -      | 规格                |
| skuList        | Object  | -      | sku信息，库存价格等 |
| disabledNumber | Boolean | false  | 是否禁止数量选择    |


- [ ` specList ` ]说明

| 属性名 |       |      | 类型   | 默认值 | 说明     |
| :----- | :---- | :--- | :----- | :----- | :------- |
| items  |       |      | Array  | -      | 分组内容 |
|        | name  |      | String | -      | 分组名称 |
|        | items |      | Array  | -      | 规格内容 |
|        |       | name | String | -      | 规格名称 |

- [ ` skuList ` ]说明

| 属性名 |       | 类型   | 默认值 | 说明     |
| :----- | :---- | :----- | :----- | :------- |
| items  |       | Array  | -      | 分组内容 |
|        | stock | Number | -      | 库存     |
|        | sku   | Array  | -      | 规格搭配 |

### 事件

| 事件名 | 说明                   | 返回值        |
| :----- | :--------------------- | :------------ |
| change | 选中内容发生变化时触发 | data=change.e |

| 属性名      | 类型   | 默认值 | 说明                                  |
| :---------- | :----- | :----- | :------------------------------------ |
| number      | Number | -      | 数量（ `disabledNumber=true` 不返回） |
| selectSpecs | Array  | -      | 当前选中的规格                        |
| sku         | Object | -      | 当前匹配到的sku，来自：`skuList`      |

