# cv-radio-group 单选框

### 简要

>本组件一般用单项选择  
>普通单选，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-radio-group">

<<< @/../src/pages/form/cv-radio-group.vue

</CvCodePreview>

### 属性

| 属性名    | 类型   | 默认值 | 说明                     |
| :-------- | :----- | :----- | :----------------------- |
| v-model   | String | -      | 数据双向绑定             |
| dataType  | String | value  | 数据类型['value','text'] |
| dataLists | Array  | -      | 选项,展示数据            |
| dataValue | String | value  | dataLists中value的标识   |
| dataText  | String | text   | dataLists中text的标识    |

