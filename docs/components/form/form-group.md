
# cv-form-group 表单分组
  
### 简要

>分组，一般和表单字段分组配合使用，默认为实心白色背景。
 
### 示例

```vue
```

### 属性

| 属性名     | 类型   | 默认值 | 说明                       | sms  |
| :--------- | :----- | :----- | :------------------------- | :--- |
| label      | String | -      | 字段名                     | √    |
| labelAlign | String | left   | 对齐方式                   | √    |
| layout     | String | row    | ['row','col']              | √    |
| labelWidth | Number | 60     | 字段区域宽度，上下排列无效 | √    |
