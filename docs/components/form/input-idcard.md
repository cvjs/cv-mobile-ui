
# cv-input-idcard 身份证输入框

### 简要

>属性为`idcard`的input  
>身份证输入框，一般和表单配合使用，默认为实心白色背景。  
>身份证输入键盘 |	微信、支付宝、百度、QQ小程序  
 
### 示例

<CvCodePreview src="/pages/form/cv-input-idcard">

<<< @/../src/pages/form/cv-input-idcard.vue

</CvCodePreview> 


### 属性

| 属性名      | 类型    | 默认值 | 说明                 |
| :---------- | :------ | :----- | :------------------- |
| v-model     | String  | -      | 数据双向绑定         |
| placeholder | String  | -      | 占位符               |
| disabled    | Boolen  | false  | 是否禁止             |
| maxlength   | Number  | -1     | 输入长度限制         |
| clearable   | Boolean | false  | 是否显示一键清空按钮 |
