
# cv-time-base 时间选择框
 
### 简要

>从底部弹起的滚动选择器 | 日期选择器 mode = time  
>本组件一般用于 时间选择  
>时间选择框，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-time-base">

<<< @/../src/pages/form/cv-time-base.vue

</CvCodePreview>

### 属性

| 属性名      | 类型   | 默认值 | 说明             |
| :---------- | :----- | :----- | :--------------- |
| v-model     | String | -      | 数据双向绑定     |
| placeholder | String | -      | 占位符           |
| startTime   | String | -      | 开始时间 (hh-mm) |
| endTime     | String | -      | 结束时间 (hh-mm) |

