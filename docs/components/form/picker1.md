
# cv-picker1 一级选择框

### 简要

>从底部弹起的滚动选择器 | 普通选择器 mode = selector  
>本组件一般用于 单列选择  
>单列选择框，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-picker1">

<<< @/../src/pages/form/cv-picker1.vue

</CvCodePreview>

### 属性

| 属性名      | 类型   | 默认值 | 说明                           |
| :---------- | :----- | :----- | :----------------------------- |
| v-model     | String | -      | 数据双向绑定                   |
| placeholder | String | -      | 占位符                         |
| dataType    | String | value  | 数据类型['value','text']       |
| dataLists   | Array  | -      | 展示数据                       |
| dataValue   | String | value  | dataLists中value的标识         |
| dataText    | String | text   | dataLists中text的标识          |
| children    | string | tree   | 指定子树为节点对象的某个属性值 |