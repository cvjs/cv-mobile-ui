
# cv-date-base 日期选择框
 
### 简要

>从底部弹起的滚动选择器 | 日期选择器 mode = date  
>本组件一般用于 日期选择  
>日期选择框，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-date-base">

<<< @/../src/pages/form/cv-date-base.vue

</CvCodePreview>

### 属性

| 属性名      | 类型   | 默认值 | 说明                 |
| :---------- | :----- | :----- | :------------------- |
| v-model     | String | -      | 数据双向绑定         |
| placeholder | String | -      | 占位符               |
| startDate   | String | -      | 开始日期(YYYY-MM-dd) |
| endDate     | String | -      | 结束日期(YYYY-MM-dd) |

