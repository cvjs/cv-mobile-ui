
# cv-editor-quill 富文本编辑器

### 简要

>富文本编辑器，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-editor-quill">

<<< @/../src/pages/form/cv-editor-quill.vue

</CvCodePreview>

```vue
<template>
  <view>
    <cv-editor-quill label="详情" v-model="formData.editor" />
  </view>
</template>
<script>
export default {
  data() {
    return {
      formData: {
        editor: '',
      },
    };
  },
  methods: {
  }
}
</script>
```

### 平台差异
| App           | H5     | 微信小程序    | 支付宝小程序 | 百度小程序 | 字节跳动小程序 | QQ小程序 |
| :------------ | :----- | :------------ | :----------- | :--------- | :------------- | :------- |
| 2.0+，app-vue | 2.4.5+ | 基础库 2.7.0+ | x            | x          | x              | x        |

### 属性

| 属性名      | 类型   | 默认值 | 说明         |
| :---------- | :----- | :----- | :----------- |
| v-model     | String | -      | 数据双向绑定 |
| placeholder | String | -      | 占位符       |
| upload      | Object | -      | 图片上传配置 |

### {upload} 说明

| upload.key      | 类型   | 默认值         | 说明                                                 |
| :-------------- | :----- | :------------- | :--------------------------------------------------- |
| url             | String | -              | 图片上传的接口地址                                   |
| params          | Object | -              | 提交接口额外携带参数（formData）                     |
| fileKey         | String | -              | 服务器端获取文件数据的参数名                         |
| connectionCount | Number | 1              | 最大选择图片数量                                     |
| leaveConfirm    | String | '正在上传文件' | 上传图片等待显示内容                                 |
| quality         | Number | 80             | 压缩质量，范围0～100，数值越小，质量越低，压缩率越高 |
| maxSize         | Number | 1080           | 压缩后的最大尺寸                                     |

### {upload.url} 接口返回参数

| 属性名 | 类型   | 说明                 |
| :----- | :----- | :------------------- |
| error  | number | 错误代码，0 表示成功 |

