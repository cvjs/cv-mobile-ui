# cv-switch 滑块开关

### 简要

>type属性为`switch`的switch  
>滑块开关，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-switch">

<<< @/../src/pages/form/cv-switch.vue

</CvCodePreview>

### 属性

| 属性名   | 类型    | 默认值 | 说明         |
| :------- | :------ | :----- | :----------- |
| v-model  | String  | -      | 数据双向绑定 |
| disabled | Boolean | false  | 是否禁止     |
