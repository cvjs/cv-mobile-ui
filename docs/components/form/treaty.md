# cv-treaty 阅读协议

### 简要

>阅读协议，一般和表单配合使用，默认为实心白色背景。  

### 示例

<CvCodePreview src="/pages/form/cv-treaty">

<<< @/../src/pages/form/cv-treaty.vue

</CvCodePreview>


### 属性

| 属性名  | 类型    | 默认值   | 说明         |
| :------ | :------ | :------- | :----------- |
| v-model | Boolean | -        | 双向绑定数据 |
| explain | String  | 我已阅读 | 条约提示信息 |
| treaty  | String  | 服务协议 | 条约名称     |
| url     | String  | -        | 条约地址     |

### 事件

| 事件名 | 说明 | 返回值   |
| :----- | :--- | :------- |
| @click | 点击 | 触发事件 |

