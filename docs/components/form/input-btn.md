
# cv-input-btn 自带按钮的输入框

### 简要

>自带按钮的文本输入框，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-input-btn">

<<< @/../src/pages/form/cv-input-btn.vue

</CvCodePreview>

### 属性

| 属性名      | 类型   | 默认值 | 说明         |
| :---------- | :----- | :----- | :----------- |
| v-model     | String | -      | 数据双向绑定 |
| placeholder | String | -      | 占位符       |
| disabled    | Boolen | false  | 是否禁止     |
| maxlength   | Number | -1     | 输入长度限制 |



### 事件

| 事件名 | 说明 | 返回值            |
| :----- | :--- | :---------------- |
| @click | 点击 | 点击按钮 触发事件 |
