# cv-checkbox-opt-base

### 简要

>多选框，普通样式

### 示例

<CvCodePreview src="/pages/form/cv-checkbox-opt-base">

<<< @/../src/pages/form/cv-checkbox-opt-base.vue

</CvCodePreview>

### 属性

| 属性名  | 类型    | 默认值 | 说明     |
| :------ | :------ | :----- | :------- |
| value   | String  | -      | 选后的值 |
| label   | String  |        | 文本展示 |
| checked | Boolean | -      | 是否选择 |

