# cv-upload-image 图片上传

### 简要

>图片上传，一般和表单配合使用，默认为实心白色背景。
 
### 示例

<CvCodePreview src="/pages/form/cv-upload-image">

<<< @/../src/pages/form/cv-upload-image.vue

</CvCodePreview>

### 属性

| 属性名      | 类型                | 默认值 | 说明                                              |
| :---------- | :------------------ | :----- | :------------------------------------------------ |
| v-model     | Array,Object,String | -      | 具体使用如下                                      |
| src         | Array,Object,String | -      | 具体使用如下                                      |
| uploadType  | String              | one    | 上传类型（ one单图，more多图，big大图 ）          |
| maxlength   | Number              | 1      | 上传图片个数                                      |
| filename    | String              | file   | 上传表单名                                        |
| uploadParam | Object              | -      | 额外传递的数据，会拼接 file_name :`filename` 进去 |
| uploadTip   | Object              | -      | 提示内容                                          |
| uploadTitle | String              | -      | 当uploadType=big时可以添加标题                    |
| apiFunc     | Function            | -      | 上传方法                                          |

### 当 { uploadType:one } 或者 { uploadType:big } 时
  
| 属性名  | 类型   | 默认值 | 说明                                        |
| :------ | :----- | :----- | :------------------------------------------ |
| v-model | String | -      | 传递单图需要保存的字符串                    |
| src     | String | null   | 含 http:// 或 https:// 的图片地址，作为呈现 |

### uploadTip 结构&默认值

| 属性名    | 类型   | 默认值              | 说明 |
| :-------- | :----- | :------------------ | :--- |
| dimension | String | 长*宽：750*375像素; | 尺寸 |
| format    | String | 格式：jpg/png;      | 格式 |
| size      | String | 大小：小于2MB;      | 大小 |


例如：
```vue
<template>
  <view>
    <cv-upload-image v-model="formData.img" :src="formData.img_original" />
  </view>
</template>
<script>
export default {
  data(){
    return {
      formData:{
          img:'/upload/img/xxx.png',
          img_original:'http://xxx.com/upload/img/xxx.png',
      },
    };
  }
}
</script>
```

### 当 { uploadType:more } 时
  
| 属性名  | 类型  | 默认值 | 说明         |
| :------ | :---- | :----- | :----------- |
| v-model | Array | -      | 双向绑定数据 |
| src     | Array | null   | 已有图片数据 |


例如：
```vue
<template>
    <view>
        <cv-upload-image v-model="formData.img" :src="formData.img_original" />
    </view>
</template>
<script>
export default {
  data(){
    return {
      formData:{
        img: [
          '/upload/img/xxx1.png',
          '/upload/img/xxx2.png',
          '/upload/img/xxx3.png'
        ],
        img_original: [
          'https://xxx.com/upload/img/xxx1.png',
          'https://xxx.com/upload/img/xxx2.png',
          'https://xxx.com/upload/img/xxx3.png',
        ],
      },
    };
  }
}
</script>
```