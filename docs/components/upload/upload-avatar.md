# cv-upload-avatar 头像上传


### 示例

<CvCodePreview src="/pages/form/cv-upload-avatar">

<<< @/../src/pages/form/cv-upload-avatar.vue

</CvCodePreview>

```vue
<template>
  <view>
    <cv-upload-avatar v-model="formData.img" :src="formData.img_original" />
  </view>
</template>
<script>
export default {
  data() {
    return {
      formData: {
        img: '',//双向绑定
        img_original: ''//默认值
      },
    };
  }
}
</script>
```

### 属性

| 属性名  | 类型     | 默认值 | 说明     |
| :------ | :------- | :----- | :------- |
| v-model | String   | -      | 图片地址 |
| src     | String   | -      | 展示     |
| apiFunc | Function | -      | 上传方法 |

