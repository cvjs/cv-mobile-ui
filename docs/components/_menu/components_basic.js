export default {
  text: 'Basic 基础组件',
  collapsed: false,
  items: [
    { text: '搜索', link: '/components/basic/search' },
    { text: '多条件过滤', link: '/components/basic/filter' },
    { text: '图标', link: '/components/basic/icons-all' },
    { text: '导航图标下载', link: '/components/basic/icons-topbar' },
    { text: '宫格导航', link: '/components/basic/grid-item' }
  ]
};
