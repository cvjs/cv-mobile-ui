export default {
  text: '弹窗',
  collapsed: false,
  items: [
    { text: '底部弹窗', link: '/components/dialog/dialog-bottom' },
    { text: '全屏弹窗', link: '/components/dialog/dialog-full' },
    { text: '弹框加载', link: '/components/dialog/dialog-loading' },
    { text: '分享弹框', link: '/components/dialog/dialog-share' }
  ]
};
