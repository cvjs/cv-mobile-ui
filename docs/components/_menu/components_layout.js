export default {
  text: 'Layout 布局',
  collapsed: false,
  items: [
    { text: '幻灯片', link: '/components/layout/banner' },
    { text: '卡片式幻灯片', link: '/components/layout/banner-card' },
    { text: 'Box 布局', link: '/components/layout/box' },
    { text: 'Block 布局', link: '/components/layout/block' },
    { text: '自带Tab的列表', link: '/components/layout/tab-lists' },
    { text: '加载更多', link: '/components/layout/load-more' },
    { text: '横向列表', link: '/components/layout/cell-row' },
    { text: '信息提示', link: '/components/layout/message' }
  ]
};
