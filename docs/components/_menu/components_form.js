export default {
  text: 'Form 表单组件',
  collapsed: false,
  items: [
    {
      text: '基础',
      items: [
        { text: 'form-base', link: '/components/form/form-base' },
        { text: 'form-inline', link: '/components/form/form-inline' },
        { text: 'form-item', link: '/components/form/form-item' },
        { text: 'form-group 表单分组', link: '/components/form/form-group' }
      ]
    },
    {
      text: '输入框',
      items: [
        { text: '文本', link: '/components/form/input-text' },
        { text: '密码', link: '/components/form/input-password' },
        { text: '数字', link: '/components/form/input-number' },
        { text: '小数', link: '/components/form/input-digit' },
        { text: '身份证', link: '/components/form/input-idcard' },
        { text: '多行', link: '/components/form/textarea' },
        { text: '自带按钮', link: '/components/form/input-btn' }
      ]
    },
    {
      text: '选择器',
      items: [
        { text: '一级', link: '/components/form/picker1' },
        { text: '二级联动', link: '/components/form/picker2' },
        { text: '三级联动', link: '/components/form/picker3' }
      ]
    },
    {
      text: '时间日期',
      items: [
        { text: '时间', link: '/components/form/time-base' },
        { text: '日期', link: '/components/form/date-base' },
        { text: '日期时间', link: '/components/form/datetime-base' },
        { text: '日期时间|五联动', link: '/components/form/datetime-linkage' }
      ]
    },
    {
      text: '单选',
      items: [
        { text: '单选组', link: '/components/form/radio-group' },
        { text: '普通样式', link: '/components/form/radio-opt-base' },
        { text: '标签样式', link: '/components/form/radio-opt-tag' }
      ]
    },
    {
      text: '多选',
      items: [
        { text: '多选组', link: '/components/form/checkbox-group' },
        { text: '普通样式', link: '/components/form/checkbox-opt-base' },
        { text: '标签样式', link: '/components/form/checkbox-opt-tag' }
      ]
    },
    {
      text: '位置',
      items: [
        { text: '省市区', link: '/components/form/geo-region' },
        { text: '地图选择', link: '/components/form/geo-local' }
      ]
    },
    {
      text: '上传',
      items: [
        { text: '头像上传', link: '/components/upload/upload-avatar' },
        { text: '图片上传', link: '/components/upload/upload-image' }
      ]
    },
    {
      text: '编辑器',
      items: [{ text: '富文本编辑器', link: '/components/form/editor-quill' }]
    },
    {
      text: '其他',
      items: [
        { text: '滑块开关', link: '/components/form/switch' },
        { text: '阅读协议', link: '/components/form/treaty' },
        { text: '评分', link: '/components/form/rate' },
        { text: '规格选择', link: '/components/form/specs' },
        { text: '短信验证', link: '/components/senior/code-sms' }
      ]
    }
  ]
};
