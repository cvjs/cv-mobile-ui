export default {
  text: '绘制组件',
  collapsed: false,
  items: [
    { text: '条形码', link: '/components/draw/draw-barcode' },
    // { text: '海报',link: 'draw-post',  },
    { text: '圆形进度', link: '/components/draw/draw-progress' },
    { text: '二维码', link: '/components/draw/draw-qrcode' },
    { text: '海报', link: '/components/draw/draw-posters' }
  ]
};
