#!/bin/bash  
  
if [ $# -eq 0 ]; then  
    echo "参数不能为空"
fi

uiName=$1

baseDir=$(pwd)
docsDir="$baseDir/_docs/"

uiDir="$docsDir/$uiName/uni_modules/uview-ui/"

#
# prettier 格式化 
#
prettier $uiDir/components/*/props.js --write --config .vscode/.prettierrc.cjs --log-level=error
echo "格式化 components/*/props.js"
prettier $uiDir/components/*/*.vue --write --config .vscode/.prettierrc.cjs --log-level=error
echo "格式化 components/*/*.vue"
prettier $uiDir/libs/mixin/*.js --write --config .vscode/.prettierrc.cjs --log-level=error
echo "格式化 libs/mixin/*.js"


#
# node-sass 修改 SASS
#


# 拆分 scss 为单独文件
node $docsDir/script/change-scss-leave.js $uiDir/components
#  scss 文件 转换成 css 文件
node $docsDir/script/uviewui-scss-to-css.js $uiDir/

# 拆分 css 为单独文件
# node $docsDir/script/change-css-split.js ../ui-uview-2.0.35/components


#
# 修改兼容 vue3
# 修改 props
#
node $docsDir/script/change-v3-props.js $uiDir/components
node $docsDir/script/change-v3-vue.js $uiDir/components

# 格式化下
prettier $uiDir/components/*/style.scss --write --config .vscode/.prettierrc.cjs --log-level=error

