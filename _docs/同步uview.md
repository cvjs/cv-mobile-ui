
# uview-ui



### 同步记录

### 2023-07-27
- uview-ui 版本：2.0.36
- uview-plus 版本：3.1.34

### 2023-01-04
- 版本：2.0.35


### 同步手册


- 下载 uview-ui     
https://gitee.com/umicro/uView2.0

- 下载 uview-plus     
https://gitee.com/uiadmin/uview-plus

- 解析uView2

将 uView2解压到 `_docs` 目录下，然后修改版本，执行脚本
```sh

sh _docs/parse-uview-ui.sh uView2.0-2.0.36

```
然后工具对比 ` /src/uni_modules/uview-ui ` 和 ` /_docs/uView2.0-2.0.36/uni_modules/uview-ui `

- 解析uView-plus

将 uView-plus 解压到 `_docs` 目录下，然后修改版本，执行脚本
```sh

sh _docs/parse-uview-plus.sh uview-plus-master

```
然后工具对比 ` /src/uni_modules/uview-plus ` 和 ` /_docs/uview-plus-master/uni_modules/uview-ui `



 
### 升级流程

- 修改props

### 使用



~~~



直接给完整代码，不要分步骤



~~~