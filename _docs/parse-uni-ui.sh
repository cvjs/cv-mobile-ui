#!/bin/bash  
  
if [ $# -eq 0 ]; then  
    echo "参数不能为空"
fi

uiName=$1

baseDir=$(pwd)
docsDir="$baseDir/_docs/"

uiDir="$docsDir/$uiName/lib/"

#
# prettier 格式化 
#
prettier $uiDir/*/*.vue --write --config .vscode/.prettierrc.cjs --log-level=error
echo "格式化 /*/*.vue"

# 拆分 scss 为单独文件
node $docsDir/script/change-scss-leave.js $uiDir
#  scss 文件 转换成 css 文件
node $docsDir/script/uniui-scss-to-css.js $uiDir/


# node $docsDir/script/change-css-leave.js $uiDir