# uni-ui 同步



### 同步记录

- 2023-01-04
- 2022-10-27
- 2021-08-27


### 同步

同步日期：2023-09-05  
插件地址：https://ext.dcloud.net.cn/plugin?id=55  
git地址：https://github.com/dcloudio/uni-ui/tags  
npm地址：https://www.npmjs.com/package/@dcloudio/uni-ui  
Tag版本：v1.4.23  


### 升级流程


- 下载 

用 pnpm 下载包名，然后把 ` node_modules/@dcloudio/uni-ui ` 复制到 ` _docs/uni-ui_1.4.28 ` 下 
并且复制一份 hello-uniapp/uni.scss 到 ` _docs/uni-ui_1.4.28/lib/uni-scss ` 下 

```sh
pnpm i @dcloudio/uni-ui
```

- 执行脚本

```
sh _docs/parse-uni-ui.sh uni-ui_1.4.28
sh _docs/parse-uni-ui.sh uni-ui-old

sh _docs/parse-uni-ui.sh uni-ui@1.4.28

```
