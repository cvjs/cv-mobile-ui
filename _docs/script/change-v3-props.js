const fs = require('fs');
const path = require('path');
const { findFiles, checkIfTextExistsInFile, replaceTextInFile, indentJavaScriptFile } = require('./common.js');

const processArgv = process.argv;
if (processArgv.length < 3) {
  console.log('参数数量错误');
  return false;
}
if (!processArgv[2]) {
  console.log('参数不存在');
  return false;
}

const changeDir = path.resolve(__dirname, processArgv[2]);
const changeName = 'props.js';

const searchText1 = "import defprops from '../../libs/config/props';";
const replaceText1 = `import defprops from '../../libs/config/props';
export default {`;

const searchText2 = `uni.$u.props`;
const replaceText2 = 'defprops';

const matchingFiles = findFiles(changeDir, changeName);
// console.log(matchingFiles);
for (let filePath of matchingFiles) {
  if (checkIfTextExistsInFile(filePath, searchText2)) {
    replaceTextInFile(filePath, `uni.\\$u.props`, replaceText2);
    if (!checkIfTextExistsInFile(filePath, searchText1)) {
      console.log('--不存在-', searchText1);
      replaceTextInFile(filePath, 'export default {', replaceText1);
    }
  }
  indentJavaScriptFile(filePath);
}
