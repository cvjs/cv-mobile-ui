const fs = require('fs');
const path = require('path');
var sass = require('/usr/local/lib/node_modules/node-sass');
const {
  findFiles,
  checkIfTextExistsInFile,
  replaceTextInFile,
  indentJavaScriptFile,
  removeExtraLines,
  readLinesWithPrefix
} = require('./common.js');

const processArgv = process.argv;
if (processArgv.length < 3) {
  console.log('参数数量错误');
  return false;
}
if (!processArgv[2]) {
  console.log('参数不存在');
  return false;
}

const changeDir = path.resolve(__dirname, processArgv[2]);

/**
 * 获取 $u-开头
 */
function getScssArrToObj(filePath) {
  const tmsScssArr = readLinesWithPrefix(filePath, `$u-`);
  let tmsScssObj = {};
  for (let i in tmsScssArr) {
    let itemData = tmsScssArr[i];
    let itemArr = itemData.split(':');
    let leftText = itemArr[0];
    let rightText = itemArr[1];
    tmsScssObj[`${leftText}`] = {
      css_var: leftText.replace('$u-', 'var(--u-') + ')',
      key: leftText.replace('$u-', '--u-'),
      value: rightText
    };
  }
  return tmsScssObj;
}
function scssToCss(filePath, tmsScssObj) {
  const fileDir = path.dirname(filePath);
  let oldContent = fs.readFileSync(filePath, 'utf8');
  // 遍历对象，替换文件内容中的对应值
  for (const key in tmsScssObj) {
    oldContent = oldContent
      .replace(new RegExp(`\\${key};`, 'g'), tmsScssObj[key].css_var + ';')
      .replace(new RegExp(`\\${key}!`, 'g'), tmsScssObj[key].css_var + '!')
      .replace(new RegExp(`\\${key} `, 'g'), tmsScssObj[key].css_var + ' ');
  }

  if (filePath.includes('/style.scss')) {
    // 这个是 components下的
    oldContent = oldContent.replace('./vue.scss', './vue-parse.scss').replace('./nvue.scss', './nvue-parse.scss');
  } else if (filePath.includes('/index.scss')) {
    // 这个是 index.scss
    oldContent = oldContent.replace(new RegExp(`.scss`, 'g'), '-libcss.scss');
  } else if (filePath.includes('/libs/css/')) {
    // 这个是 libs/css下的 .scss
    const appenFirstLine = `@mixin flex($direction: row) {
  /* #ifndef APP-NVUE */
  display: flex;
  /* #endif */
  flex-direction: $direction;
}
`;
    oldContent = `${appenFirstLine}${oldContent}`;
  }

  let newContent = sass.renderSync(
    {
      // file: outFileName,
      importer: require('/usr/local/lib/node_modules/sass-loader').importer,
      outputStyle: 'expanded',
      data: oldContent,
      /**
       * 需要查找的 css文件
       */
      includePaths: [changeDir + '/libs/css/', fileDir]
    },
    function (err, result) {
      // 这里的 result 对象包含了许多东西，但主要是 "css" 和 "map"
      if (err) {
        console.log('----错误页面---', filePath, err);
      } else {
        const resultCss = result.css.toString();
        // fs.writeFileSync(outFileName, resultCss);
        // console.log(result.css);
      }
    }
  );
  return newContent.css.toString();
}

const tmsScssObj = getScssArrToObj(changeDir + '/theme.scss');

/**
 * 解析 components 的非 style.scss
 *
 */
const matchingStyleScssList = findFiles(changeDir + '/components', '.scss');
for (const filePath of matchingStyleScssList) {
  if (filePath.includes('/vue.scss') || filePath.includes('/nvue.scss')) {
    let scssToCssText = scssToCss(filePath, tmsScssObj);
    // 去除多余空行
    const lastContent = scssToCssText.replace(/^\s*[\r\n]/gm, '');
    const outFileName = filePath.replace('.scss', '-parse.scss');
    console.log('--outFileName---', outFileName);
    fs.writeFileSync(outFileName, lastContent);
  }
}
/**
 * 解析 components 下的 scss
 */
for (const filePath of matchingStyleScssList) {
  if (filePath.includes('/style.scss')) {
    const scssToCssText = scssToCss(filePath, tmsScssObj);
    // 去除多余空行
    const lastContent = scssToCssText.replace(/^\s*[\r\n]/gm, '');
    const outFileName = filePath.replace('.scss', '.css');
    fs.writeFileSync(outFileName, lastContent);
  }
}
/**
 * 解析 libs/css/*.scss
 */
const matchingLibsScssList = findFiles(changeDir + '/libs/css', '.scss');
for (const filePath of matchingLibsScssList) {
  if (!filePath.includes('-libcss.scss')) {
    const scssToCssText = scssToCss(filePath, tmsScssObj);
    // 去除多余空行
    const lastContent = scssToCssText.replace(/^\s*[\r\n]/gm, '');
    const outFileName = filePath.replace('.scss', '-libcss.scss');
    fs.writeFileSync(outFileName, lastContent);
  }
}
/**
 * 解析 index.scss
 */
const indexCssContent = scssToCss(changeDir + '/index.scss', tmsScssObj);
fs.writeFileSync(changeDir + '/index.css', indexCssContent);

/**
 * 解析 theme.scss
 */
let themeCssContent = [];
for (let tmsKey in tmsScssObj) {
  let tmsItem = tmsScssObj[tmsKey];
  themeCssContent.push(`  ${tmsItem.key}:${tmsItem.value}`);
}
fs.writeFileSync(
  changeDir + '/theme.css',
  `page {
${themeCssContent.join('\n')}
}
`
);
