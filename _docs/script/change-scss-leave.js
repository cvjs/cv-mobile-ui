const fs = require('fs');
const path = require('path');
const { findFiles, checkIfTextExistsInFile, replaceTextInFile, indentJavaScriptFile, removeExtraLines } = require('./common.js');
const processArgv = process.argv;
if (processArgv.length < 3) {
  console.log('参数数量错误');
  return false;
}
if (!processArgv[2]) {
  console.log('参数不存在');
  return false;
}

const changeDir = path.resolve(__dirname, processArgv[2]);
const changeName = '.vue';
const matchingFiles = findFiles(changeDir, changeName);

const regexStyles = /<style[^>]*>([\s\S]*?)<\/style>/gi;
// const regexImport = /import\s+([\w\s]+?)\s+from\s+\'([\w]+?)\'/g;
/**
 *
 */
function includesScssParse(filePath) {
  // 获取 文件内容
  const fileContent = fs.readFileSync(filePath, 'utf-8');
  /**
   * 解析 scss
   */
  if (fileContent.includes('lang="scss"')) {
    const fileDir = path.dirname(filePath);
    // console.log(filePath, fileDir);
    // return false;
    // 获取 scss
    const match = regexStyles.exec(fileContent);
    const styleContent = match ? match[1] : null;
    if (styleContent) {
      // 对 styleContent 进行处理,并且写入
      // console.log(styleContent);
      fs.writeFileSync(`${fileDir}/style.scss`, styleContent);
      // 替换成 style 标签
      let styleNewTag = '';
      if (fileContent.includes('scoped')) {
        styleNewTag = "<style scoped>\n@import 'style.css';\n</style>";
      } else {
        styleNewTag = "<style>\n@import 'style.css';\n</style>";
      }
      const modifiedContent = fileContent.replace(regexStyles, styleNewTag);
      fs.writeFileSync(filePath, modifiedContent);
    } else {
      let styleNewTag = '';
      if (fileContent.includes('scoped')) {
        styleNewTag = '<style scoped></style>';
      } else {
        styleNewTag = '<style></style>';
      }
      const modifiedContent = fileContent.replace(regexStyles, styleNewTag);
      fs.writeFileSync(filePath, modifiedContent);
      console.log('未找到 style 标签 的内容', filePath);
    }
  }
}

for (let filePath of matchingFiles) {
  includesScssParse(filePath);
  const fileContent = fs.readFileSync(filePath, 'utf-8');
  const modifiedContent = fileContent.replace(/^\s*[\r\n]/gm, '');
  fs.writeFileSync(filePath, modifiedContent);
}
