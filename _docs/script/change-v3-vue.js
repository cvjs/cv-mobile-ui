const fs = require('fs');
const path = require('path');
const { findFiles, checkIfTextExistsInFile, replaceTextInFile, indentJavaScriptFile, removeExtraLines } = require('./common.js');
const processArgv = process.argv;
if (processArgv.length < 3) {
  console.log('参数数量错误');
  return false;
}
if (!processArgv[2]) {
  console.log('参数不存在');
  return false;
}

const changeDir = path.resolve(__dirname, processArgv[2]);
const changeName = '.vue';
const matchingFiles = findFiles(changeDir, changeName);

// const regexImport = /import\s+([\w\s]+?)\s+from\s+\'([\w]+?)\'/gi;
const regexImport = /import\s+.*\s+from\s+['"].*['"];$/gm;

/**
 * 追加 import
 */
function includeStrAppendImport(filePath, searchText, replaceReg, replaceText, appendContent) {
  // 获取 文件内容
  const fileContent = fs.readFileSync(filePath, 'utf-8');
  if (fileContent.includes(searchText)) {
    let modifiedContent = fileContent.replace(new RegExp(replaceReg, 'g'), replaceText);

    if (!fileContent.includes(appendContent)) {
      // 解析import语句
      // 使用 matchAll 方法匹配所有 import 语句
      const matches = fileContent.match(regexImport);
      if (matches && matches.length > 0) {
        // 获取最后一行import语句
        const lastImport = matches[matches.length - 1];
        // 追加内容到最后一行的下一行
        const newContent = `${lastImport}\n${appendContent}`;
        modifiedContent = modifiedContent.replace(lastImport, newContent);
      } else {
        modifiedContent = modifiedContent.replace(/(<script[^>]*>)/i, `$1\n${appendContent}\n`);
      }
    }
    fs.writeFileSync(filePath, modifiedContent);
    // fs.writeFileSync(filePath, updatedContent);
  }
}

for (let filePath of matchingFiles) {
  /**
   * 这个同步修改成 vue3
   */
  includeStrAppendImport(
    filePath,
    `uni.$u.mpMixin`,
    `uni.\\$u.mpMixin`,
    'mpMixin',
    "import mpMixin from '../../libs/mixin/mpMixin.js';"
  );

  includeStrAppendImport(filePath, `uni.$u.mixin`, `uni.\\$u.mixin`, 'mixin', "import mixin from '../../libs/mixin/mixin.js';");

  replaceTextInFile(filePath, `https://www.uviewui.com/`, `https://ijry.github.io/uview-plus/`);
  // 去除空行
  removeExtraLines(filePath);
}
