const fs = require('fs');
const path = require('path');
const readline = require('readline');
/**
 * 根据文件名，查找文件
 */

function findFiles(dir, fileName) {
  let files = fs.readdirSync(dir);
  let result = [];

  files.forEach((file) => {
    let fullPath = path.join(dir, file);
    let stat = fs.statSync(fullPath);

    if (stat.isDirectory()) {
      result = result.concat(findFiles(fullPath, fileName));

      // const childPaths = findFiles(fullPath, fileName);
      // result.push(...childPaths);
    } else if (path.basename(fullPath).match(fileName)) {
      // } else if (stats.isFile() && file.toLowerCase().endsWith(fileName)) {
      // } else if (path.basename(fullPath).match(new RegExp(`${fileName}`))) {
      result.push(fullPath);
    }
  });

  return result;
}
/**
 * 替换内容
 * @param {*} filePath
 * @param {*} searchText
 * @param {*} replaceText
 */
function replaceTextInFile(filePath, searchText, replaceText) {
  // 只处理文件，不处理目录
  if (fs.statSync(filePath).isFile()) {
    // 读取文件内容
    const fileContent = fs.readFileSync(filePath, 'utf-8');
    // 使用正则表达式替换所有匹配的文本
    const replacedContent = fileContent.replace(new RegExp(searchText, 'g'), replaceText);
    // const replacedContent = fileContent.replace(searchText, replaceText);
    // const replacedContent = fileContent.toString().replace(`/${searchText}/g`, replaceText);

    // 将修改后的内容写回文件
    fs.writeFileSync(filePath, replacedContent);
  }
}

/**
 * 查找内容
 */
function checkIfTextExistsInFile(filePath, searchText) {
  // 读取文件内容
  let fileContent = fs.readFileSync(filePath, 'utf8');
  if (fileContent.includes(searchText)) {
    return true;
  } else {
    return false;
  }
}
/**
 * 文件缩近
 * @param {*} filePath
 * @returns
 */
function indentJavaScriptFile(filePath) {
  const fileContent = fs.readFileSync(filePath, 'utf-8');
  const indentedContent = fileContent.replace(/\n/g, '\n  ');
  return indentedContent;
}
/**
 * 去掉多余的空行
 * @param {*} filePath
 */
function removeExtraLines(filePath) {
  const fileContent = fs.readFileSync(filePath, 'utf8');
  // 使用正则表达式匹配和去除多余的空行
  const modifiedData = fileContent.replace(/^\s*[\r\n]/gm, '');
  fs.writeFileSync(filePath, modifiedData);
}
/**
 * 读取每行内容存到数组里
 */
function readLinesWithPrefix(filePath, prefix) {
  try {
    const fileContent = fs.readFileSync(filePath, 'utf-8');
    const linesArray = fileContent.split(`\n`);
    const results = [];
    linesArray.forEach((line) => {
      if (line.startsWith(prefix)) {
        results.push(line);
      }
    });
    return results;
  } catch (error) {
    console.error('读取文件出错：', error);
  }
}

module.exports = {
  findFiles,
  replaceTextInFile,
  checkIfTextExistsInFile,
  indentJavaScriptFile,
  removeExtraLines,
  readLinesWithPrefix
};
