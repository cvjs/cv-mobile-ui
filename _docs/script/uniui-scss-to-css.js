const fs = require('fs');
const path = require('path');
var sass = require('/usr/local/lib/node_modules/node-sass');
const {
  findFiles,
  checkIfTextExistsInFile,
  replaceTextInFile,
  indentJavaScriptFile,
  removeExtraLines,
  readLinesWithPrefix
} = require('./common.js');

const processArgv = process.argv;
if (processArgv.length < 3) {
  console.log('参数数量错误');
  return false;
}
if (!processArgv[2]) {
  console.log('参数不存在');
  return false;
}

const changeDir = path.resolve(__dirname, processArgv[2]);
/**
 * 获取 $u-开头
 */
function getScssArrToObj(filePath) {
  const tmsScssArr = readLinesWithPrefix(filePath, `$uni-`);
  let tmsScssObj = {};
  for (let i in tmsScssArr) {
    let itemData = tmsScssArr[i];
    let itemArr = itemData.split(':');
    let leftText = itemArr[0];
    let rightText = itemArr[1].split(';')[0] + ';';
    tmsScssObj[`${leftText}`] = {
      css_var: leftText.replace('$uni-', 'var(--uni-') + ')',
      key: leftText.replace('$uni-', '--uni-'),
      value: rightText
    };
  }
  return tmsScssObj;
}

function scssToCss(filePath, tmsScssObj) {
  const fileDir = path.dirname(filePath);
  let oldContent = fs.readFileSync(filePath, 'utf8');
  // 遍历对象，替换文件内容中的对应值
  for (const key in tmsScssObj) {
    if (!oldContent.includes(`${key}:`)) {
      oldContent = oldContent
        .replace(new RegExp(`\\${key};`, 'g'), tmsScssObj[key].css_var + ';')
        .replace(new RegExp(`\\${key}!`, 'g'), tmsScssObj[key].css_var + '!')
        .replace(new RegExp(`\\${key} `, 'g'), tmsScssObj[key].css_var + ' ');
    }
  }
  if (filePath.includes('/style.scss')) {
    // 这个是 components下的
    // oldContent = oldContent.replace('./vue.scss', './vue-parse.scss').replace('./nvue.scss', './nvue-parse.scss');
  } else if (filePath.includes('/index.scss')) {
    // 这个是 index.scss
    oldContent = oldContent.replace(new RegExp(`.scss`, 'g'), '-libcss.scss');
  } else if (filePath.includes('/libs/css/')) {
    // 这个是 libs/css下的 .scss
    const appenFirstLine = `@mixin flex($direction: row) {
  /* #ifndef APP-NVUE */
  display: flex;
  /* #endif */
  flex-direction: $direction;
}
`;
    oldContent = `${appenFirstLine}${oldContent}`;
  }

  let newContent = sass.renderSync(
    {
      // file: outFileName,
      importer: require('/usr/local/lib/node_modules/sass-loader').importer,
      outputStyle: 'expanded',
      data: oldContent,
      /**
       * 需要查找的 css文件
       */
      includePaths: [changeDir + '/uni-scss', fileDir]
    },
    function (err, result) {
      // 这里的 result 对象包含了许多东西，但主要是 "css" 和 "map"
      if (err) {
        console.log('----错误页面---', filePath, err);
      } else {
        const resultCss = result.css.toString();
        // fs.writeFileSync(outFileName, resultCss);
        // console.log(result.css);
      }
    }
  );
  try {
    return newContent.css.toString();
  } catch (error) {
    return '';
  }
}

const tmsScssObj1 = getScssArrToObj(changeDir + '/uni-scss/uni.scss');
const tmsScssObj2 = getScssArrToObj(changeDir + '/uni-scss/theme.scss');
const tmsScssObj = Object.assign(tmsScssObj1, tmsScssObj2);
/**
 * 解析 theme.scss
 */
let themeCssContent = [];
for (let tmsKey in tmsScssObj) {
  let tmsItem = tmsScssObj[tmsKey];
  themeCssContent.push(`${tmsKey}:${tmsItem.css_var};`);
}
const appendTmsCssContent = `${themeCssContent.join('\n')}`;
/**
 * 解析 components 下的 scss
 */
const matchingStyleScssList = findFiles(changeDir, 'style.scss');

// console.log(appendTmsCssContent);
// return false;
/**
 * 解析 components 的 style.scss
 */
for (const filePath of matchingStyleScssList) {
  if (filePath.includes('/style.scss')) {
    const scssToCssText = scssToCss(filePath, tmsScssObj);
    // 去除多余空行
    const lastContent = scssToCssText.replace(/^\s*[\r\n]/gm, '');
    const outFileName = filePath.replace('.scss', '.css');
    fs.writeFileSync(outFileName, lastContent);
  }
}
