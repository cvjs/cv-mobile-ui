import { defineConfig } from 'vite';
import uni from '@dcloudio/vite-plugin-uni';

// https://vitejs.dev/config/
// https://blog.csdn.net/admans/article/details/126985018

export default defineConfig({
  plugins: [uni()],
  build: {
    minify: 'terser',
    terserOptions: {
      warnings: false,
      compress: {
        // 生产环境时移除console
        drop_console: true,
        drop_debugger: true
      }
    }
  }
  // publicPath: '/xxxx',
  // outputDir: 'dist',
  // assetsDir: 'static',
  // configureWebpack: {
  //   devServer: {
  //     // 调试时允许内网穿透，让外网的人访问到本地调试的H5页面
  //     disableHostCheck: true
  //   }
  // }
});
