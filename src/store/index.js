import { createStore } from 'vuex';

// import getters from './getters';

// https://webpack.js.org/guides/dependency-management/#requirecontext
const modulesFiles = import.meta.glob('./modules/*.js', { eager: true });

const modules = {};
for (const key in modulesFiles) {
  modules[key.replace(/(\.\/modules\/|\.js)/g, '')] = modulesFiles[key].default;
}
Object.keys(modules).forEach((item) => {
  modules[item]['namespaced'] = true;
});

const store = createStore({
  modules
  // getters
});
export default store;
