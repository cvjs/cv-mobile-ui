export function getDemoData(params) {
  return new Promise((resolve, reject) => {
    let data = [];
    /* 模拟请求500毫秒拿到数据 */
    setTimeout(() => {
      if (params.page > 3) {
        resolve({ data: data });
      } else {
        for (let i = 0; i < params.pagesize; ++i) {
          data.push({ title: params.tabTitle, info: '此处为演示内容' });
        }
        resolve({ data: data });
      }
    }, 500);
  });
}
export function testReqMock(testData) {
  return new Promise((resolve, reject) => {
    resolve({});
  });
}

/* #ifdef MP-WEIXIN */
// import umeng from './utils/umeng-weixin.js'
/* #endif */
