import ITF from './ITF';
import ITF14 from './ITF14';

export default {
  ITF: ITF,
  ITF14: ITF14
};
