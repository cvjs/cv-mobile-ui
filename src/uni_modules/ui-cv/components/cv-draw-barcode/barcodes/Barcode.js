function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError('Cannot call a class as a function');
  }
}

export function BarcodeFunc(data, options) {
  _classCallCheck(this, BarcodeFunc);
  this.data = data;
  this.text = options.text || data;
  this.options = options;
}
// export function BarcodeFunc;
// export function BarcodeFunc;
