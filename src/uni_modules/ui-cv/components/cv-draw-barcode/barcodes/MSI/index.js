import MSI from './MSI.js';
import MSI10 from './MSI10.js';
import MSI11 from './MSI11.js';
import MSI1010 from './MSI1010.js';
import MSI1110 from './MSI1110.js';

export default {
  MSI: MSI,
  MSI10: MSI10,
  MSI11: MSI11,
  MSI1010: MSI1010,
  MSI1110: MSI1110
};
