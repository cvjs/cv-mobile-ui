/* 基础图标库 */
export default {
	"wallet-union-pay": "\ue313",
	"wallet-kaquan": "\ue312",
	"wallet-kaquan-filled": "\ue311",
	"wallet-pay-zhanghuyue": "\ue310",
	"wallet-qianbao": "\ue309",
	"wallet-qianbao-filled": "\ue308",
	"wallet-fukuan": "\ue307",
	"wallet-yue": "\ue306",
	"wallet-pay-zaixianzhifu": "\ue305",
	"wallet-pay-jinbi": "\ue304",
	"wallet-pay-weixin": "\ue303",
	"wallet-pay-zhifubao": "\ue302",
	"wallet-pay-yunshanfu": "\ue301",

}
