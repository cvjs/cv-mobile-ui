/* 基础图标库 */
const iconsFiles = import.meta.glob('./iconfont/*-icons.js', { eager: true });
console.log('---iconsFiles--', iconsFiles);
const modules = {};
for (const key in iconsFiles) {
  modules[key.replace(/(\.\/iconfont\/|\.js)/g, '')] = iconsFiles[key].default;
}
Object.keys(modules).forEach((item) => {
  modules[item]['namespaced'] = true;
});
// 初始化
let iconsAll = {};
for (let i in modules) {
  if (modules[i]) {
    iconsAll = Object.assign({}, iconsAll, modules[i]);
  }
}
export default iconsAll;
