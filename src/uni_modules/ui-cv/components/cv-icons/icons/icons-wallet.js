export default [
  { name: 'wallet-union-pay', unicode: 'e313' },
  { name: 'wallet-kaquan', unicode: 'e312' },
  { name: 'wallet-kaquan-filled', unicode: 'e311' },
  { name: 'wallet-pay-zhanghuyue', unicode: 'e310' },
  { name: 'wallet-qianbao', unicode: 'e309' },
  { name: 'wallet-qianbao-filled', unicode: 'e308' },
  { name: 'wallet-fukuan', unicode: 'e307' },
  { name: 'wallet-yue', unicode: 'e306' },
  { name: 'wallet-pay-zaixianzhifu', unicode: 'e305' },
  { name: 'wallet-pay-jinbi', unicode: 'e304' },
  { name: 'wallet-pay-weixin', unicode: 'e303' },
  { name: 'wallet-pay-zhifubao', unicode: 'e302' },
  { name: 'wallet-pay-yunshanfu', unicode: 'e301' }
];
