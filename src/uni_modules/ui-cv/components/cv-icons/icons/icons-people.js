export default [
  { name: 'people-member-add', unicode: 'e749' },
  { name: 'people-member-delete', unicode: 'e748' },
  { name: 'people-member', unicode: 'e747' },
  { name: 'people-member-my', unicode: 'e746' },
  { name: 'people-member-filled', unicode: 'e745' },
  { name: 'people-user', unicode: 'e744' },
  { name: 'people-user-filled', unicode: 'e743' },
  { name: 'people-user-supplier-center', unicode: 'e742' },
  { name: 'people-user-supplier-center-filled', unicode: 'e741' },
  { name: 'people-user-supplier-follow', unicode: 'e740' },
  { name: 'people-user-supplier-follow-filled', unicode: 'e739' },
  { name: 'people-user-add', unicode: 'e738' },
  { name: 'people-user-add-filled', unicode: 'e737' },
  { name: 'people-user-setting', unicode: 'e736' },
  { name: 'people-user-follow', unicode: 'e735' },
  { name: 'people-user-done', unicode: 'e734' },
  { name: 'people-user-wealth', unicode: 'e733' },
  { name: 'people-user-wealth-filled', unicode: 'e732' },
  { name: 'people-user-supplier-group', unicode: 'e731' },
  { name: 'people-user-supplier-group-filled', unicode: 'e730' },
  { name: 'people-user-group', unicode: 'e729' },
  { name: 'people-user-group-filled', unicode: 'e728' },
  { name: 'people-user-group-delete', unicode: 'e727' },
  { name: 'people-user-group-add', unicode: 'e726' },
  { name: 'people-user-group-follow', unicode: 'e725' },
  { name: 'people-pintuan', unicode: 'e701' },
  { name: 'people-pintuan-filled', unicode: 'e700' },
  { name: 'people-baobao', unicode: 'e724' },
  { name: 'people-shenshen', unicode: 'e723' },
  { name: 'people-gugu', unicode: 'e722' },
  { name: 'people-ayi', unicode: 'e721' },
  { name: 'people-shushu', unicode: 'e720' },
  { name: 'people-meimei', unicode: 'e719' },
  { name: 'people-didi', unicode: 'e718' },
  { name: 'people-gege', unicode: 'e717' },
  { name: 'people-jiejie', unicode: 'e716' },
  { name: 'people-waipo', unicode: 'e715' },
  { name: 'people-waigong', unicode: 'e714' },
  { name: 'people-nainai', unicode: 'e713' },
  { name: 'people-yeye', unicode: 'e712' },
  { name: 'people-mama', unicode: 'e711' },
  { name: 'people-baba', unicode: 'e710' },
  { name: 'people-my', unicode: 'e705' },
  { name: 'people-my-filled', unicode: 'e704' }
];
