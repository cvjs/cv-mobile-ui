export default [
  { name: 'base-idcard', unicode: 'e699' },
  { name: 'base-idcard-filled', unicode: 'e698' },
  { name: 'base-quan-question', unicode: 'e697' },
  { name: 'base-quan-question-filled', unicode: 'e696' },
  { name: 'base-quan-remind', unicode: 'e695' },
  { name: 'base-quan-remind-filled', unicode: 'e694' },
  { name: 'base-quan-warn', unicode: 'e693' },
  { name: 'base-quan-warn-filled', unicode: 'e692' },
  { name: 'base-zhoubian', unicode: 'e691' },
  { name: 'base-map-pin', unicode: 'e690' },
  { name: 'base-map-pin-ellipse', unicode: 'e689' },
  { name: 'base-geo-map', unicode: 'e688' },
  { name: 'base-geo-map-filled', unicode: 'e687' },
  { name: 'base-map-address', unicode: 'e686' },
  { name: 'base-map-address-filled', unicode: 'e685' },
  { name: 'base-map-location', unicode: 'e684' },
  { name: 'base-map-location-filled', unicode: 'e683' },
  { name: 'base-map', unicode: 'e682' },
  { name: 'base-map-filled', unicode: 'e681' },
  { name: 'base-nav', unicode: 'e680' },
  { name: 'base-nav-filled', unicode: 'e679' },
  { name: 'base-paper-plane', unicode: 'e678' },
  { name: 'base-paper-plane-filled', unicode: 'e677' },
  { name: 'base-robot', unicode: 'e676' },
  { name: 'base-lianxishangjia-gengduo', unicode: 'e675' },
  { name: 'base-tishi', unicode: 'e674' },
  { name: 'base-guize', unicode: 'e673' },
  { name: 'base-baohu', unicode: 'e672' },
  { name: 'base-youzhi2', unicode: 'e671' },
  { name: 'base-gongyong', unicode: 'e670' },
  { name: 'base-haomabaohu', unicode: 'e669' },
  { name: 'base-QQ', unicode: 'e668' },
  { name: 'base-QQ-filled', unicode: 'e667' },
  { name: 'base-QQ-solid', unicode: 'e666' },
  { name: 'base-sina-weibo-filled', unicode: 'e665' },
  { name: 'base-sina-weibo', unicode: 'e664' },
  { name: 'base-sina-weibo-solid', unicode: 'e663' },
  { name: 'base-weixin', unicode: 'e662' },
  { name: 'base-weixin-filled', unicode: 'e661' },
  { name: 'base-weixin-solid', unicode: 'e660' },
  { name: 'base-pengyouquan', unicode: 'e659' },
  { name: 'base-bidu', unicode: 'e658' },
  { name: 'base-model', unicode: 'e657' },
  { name: 'base-saoyisao', unicode: 'e656' },
  { name: 'base-qrcode', unicode: 'e655' },
  { name: 'base-yaoqingma', unicode: 'e654' },
  { name: 'base-FaceID', unicode: 'e653' },
  { name: 'base-TouchID', unicode: 'e652' },
  { name: 'base-hezuo', unicode: 'e651' },
  { name: 'base-wifi', unicode: 'e650' },
  { name: 'base-shouji', unicode: 'e649' },
  { name: 'base-shijian', unicode: 'e648' },
  { name: 'base-caidan', unicode: 'e647' },
  { name: 'base-liebiao', unicode: 'e646' },
  { name: 'base-setting', unicode: 'e645' },
  { name: 'base-quan-more', unicode: 'e644' },
  { name: 'base-more', unicode: 'e643' },
  { name: 'base-more-filled', unicode: 'e642' },
  { name: 'base-small-circle-filled', unicode: 'e641' },
  { name: 'base-spinner', unicode: 'e640' },
  { name: 'base-boy', unicode: 'e639' },
  { name: 'base-girl', unicode: 'e638' },
  { name: 'base-calender', unicode: 'e637' },
  { name: 'base-yuding', unicode: 'e636' },
  { name: 'base-lianxi', unicode: 'e635' },
  { name: 'base-phone', unicode: 'e634' },
  { name: 'base-phone-filled', unicode: 'e633' },
  { name: 'base-email-open', unicode: 'e632' },
  { name: 'base-email-open-filled', unicode: 'e631' },
  { name: 'base-email', unicode: 'e630' },
  { name: 'base-email-filled', unicode: 'e629' },
  { name: 'base-question-filled', unicode: 'e628' },
  { name: 'base-question', unicode: 'e627' },
  { name: 'base-message', unicode: 'e626' },
  { name: 'base-message-filled', unicode: 'e625' },
  { name: 'base-chat', unicode: 'e624' },
  { name: 'base-chat-filled', unicode: 'e623' },
  { name: 'base-home', unicode: 'e610' },
  { name: 'base-home-filled', unicode: 'e609' },
  { name: 'base-eye', unicode: 'e608' },
  { name: 'base-eye-fill', unicode: 'e607' },
  { name: 'base-eye-slash', unicode: 'e606' },
  { name: 'base-eyeslash-fill', unicode: 'e605' },
  { name: 'base-gongju', unicode: 'e604' },
  { name: 'base-gongju-filled', unicode: 'e603' },
  { name: 'base-flag', unicode: 'e602' },
  { name: 'base-falg-filled', unicode: 'e601' }
];
