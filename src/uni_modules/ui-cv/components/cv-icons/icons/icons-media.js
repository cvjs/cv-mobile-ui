export default [
  { name: 'media-ring', unicode: 'e841' },
  { name: 'media-ring-filled', unicode: 'e840' },
  { name: 'media-ring-mute', unicode: 'e839' },
  { name: 'media-ring-mute-filled', unicode: 'e838' },
  { name: 'media-sound', unicode: 'e837' },
  { name: 'media-sound-filled', unicode: 'e836' },
  { name: 'media-sound-mute', unicode: 'e835' },
  { name: 'media-sound-mute-filled', unicode: 'e834' },
  { name: 'media-player-pause-circle', unicode: 'e833' },
  { name: 'media-player-play-circle', unicode: 'e832' },
  { name: 'media-player-pause-circle-filled', unicode: 'e831' },
  { name: 'media-player-play-circle-filled', unicode: 'e830' },
  { name: 'media-player-backward-fast', unicode: 'e829' },
  { name: 'media-player-backward', unicode: 'e828' },
  { name: 'media-player-backward-step', unicode: 'e827' },
  { name: 'media-player-play', unicode: 'e826' },
  { name: 'media-player-pause', unicode: 'e825' },
  { name: 'media-player-forward-step', unicode: 'e824' },
  { name: 'media-player-forward', unicode: 'e842' },
  { name: 'media-player-forward-fast', unicode: 'e823' },
  { name: 'media-film', unicode: 'e822' },
  { name: 'media-film-filled', unicode: 'e821' },
  { name: 'media-movie', unicode: 'e820' },
  { name: 'media-movie-filled', unicode: 'e819' },
  { name: 'media-music', unicode: 'e818' },
  { name: 'media-music-filled', unicode: 'e817' },
  { name: 'media-printer', unicode: 'e816' },
  { name: 'media-printer-fill', unicode: 'e815' },
  { name: 'media-weather-sunny', unicode: 'e814' },
  { name: 'media-weather-day', unicode: 'e813' },
  { name: 'media-images', unicode: 'e812' },
  { name: 'media-images-filled', unicode: 'e811' },
  { name: 'media-image', unicode: 'e810' },
  { name: 'media-image-filled', unicode: 'e809' },
  { name: 'media-camera', unicode: 'e808' },
  { name: 'media-camera-filled', unicode: 'e807' },
  { name: 'media-video', unicode: 'e806' },
  { name: 'media-video-filled', unicode: 'e805' },
  { name: 'media-microphone', unicode: 'e804' },
  { name: 'media-microphone-filled', unicode: 'e803' },
  { name: 'media-microphone-mute', unicode: 'e802' },
  { name: 'media-microphone-mute-filled', unicode: 'e801' }
];
