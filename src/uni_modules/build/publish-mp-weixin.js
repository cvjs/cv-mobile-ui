const fs = require('fs-extra');
const path = require('path');
const glob = require('glob');
const exec = require('child_process').exec;
const argv = process.argv.splice(2)[0];

/**
 * ---------- ----------
 * 处理项目
 * ---------- ----------
 */
const projectPath = process.cwd();
const projectDist = path.join(projectPath, '/dist/build/mp-weixin');

/* 版本号+1 */
const ci = require('miniprogram-ci');
(async () => {
  const project = new ci.Project({
    appid: 'wxsomeappid', //合法的小程序/小游戏 appid
    type: 'miniProgram', //显示指明当前的项目类型, 默认为 miniProgram，有效值 miniProgram/miniProgramPlugin/miniGame/miniGamePlugin
    projectPath: projectDist, //项目路径
    privateKeyPath: 'the/privatekey/path', //私钥的路径
    ignores: ['node_modules/**/*'] //指定需要排除的规则
  });
  const uploadResult = await ci.upload({
    project, //#项目对象
    version: '1.1.1', //自定义版本号
    desc: '来自jenkins自动化构建', //自定义备注
    setting: {
      //#编译设置
      es6: true
    },
    onProgressUpdate: console.log //进度更新监听函数
  });
  console.log(uploadResult);
})();
