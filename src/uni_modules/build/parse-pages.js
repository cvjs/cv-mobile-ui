// const fs = require('/usr/local/lib/node_modules/fs-extra');
// const path = require('path');
// const glob = require('/usr/local/lib/node_modules/glob');
// const exec = require('child_process').exec;
/**
 * https://www.cnblogs.com/Megasu/p/16635566.html
 */
// import fs from "fs-extra";
import fs from 'node:fs';
import { fileURLToPath } from 'node:url';
import { dirname } from 'node:path';
import path from 'node:path';
import glob from 'glob';
// 获取 __filename 的 ESM 写法
const __filename = fileURLToPath(import.meta.url);
// 获取 __dirname 的 ESM 写法
const __dirname = dirname(fileURLToPath(import.meta.url));

import { exec } from 'child_process';
const argv = process.argv.splice(2)[0];
console.log('---', argv);
/* pages.json
 * 用户 pages 分布式配置，分别为三类配置 mian、sub、common
 * *-main 为主包所有路径配置
 * *-sub 为当前分包所有路径配置
 * *-common 为globalStyle、tabBar和easycom的配置
 * 已实现空路径自动创建vue文件，模板文件：./themp.vue
 * 内置命令 npm run pages
 */

/* 初始化vue模板文件 */
const libTemplPath = path.join(__dirname, './themp.vue');
let libTemplData = '';
fs.access(libTemplPath, function (flag) {
  if (!flag) {
    libTemplData = fs.readFileSync(path.join(libTemplPath), 'utf-8');
  }
});
/**
 * ---------- ----------
 * 处理项目
 * ---------- ----------
 */
const projectPath = process.cwd();
const projectSrc = path.join(projectPath, '/src');

// 用于存放pages所有配置
var pages = {};
// 获取所有
const pages_main = glob.sync(projectSrc + '/pages/**/*-main.json');
const pages_sub = glob.sync(projectSrc + '/pages/**/*-sub.json');
const pages_common = glob.sync(projectSrc + '/pages/**/*-common.json');

var pages_common_data = {};
var pages_main_data = [];
var pages_sub_data = [];
getCommonData();

function getCommonData() {
  pages_common
    .reduce((promise, fileName) => {
      return promise.then(() => {
        console.log('【主要配置】' + fileName);
        var temp = JSON.parse(filter_comments(fs.readFileSync(path.join(fileName), 'utf-8')));

        Object.assign(pages_common_data, temp);
      });
    }, Promise.resolve([]))
    .then(() => {
      if (!pages_common_data.tabBar) {
        console.log('【error】主要配置文件不正确（tabBar配置不存在），请检查修改配置后重试');
        return;
      }
      console.log('【主要配置】 - 读取完毕');
      ergodic(pages_common_data.tabBar.list);
      getMainData();
    });
}
function getMainData() {
  pages_main
    .reduce((promise, fileName) => {
      return promise.then(() => {
        console.log('【主包配置】 ' + fileName);
        var temp = JSON.parse(filter_comments(fs.readFileSync(path.join(fileName), 'utf-8')));
        pages_main_data = pages_main_data.concat(temp);
      });
    }, Promise.resolve([]))
    .then(() => {
      console.log('【主包配置】 - 读取完毕');
      ergodic(pages_main_data, 'path');
      getSubData();
    });
}
function getSubData() {
  pages_sub
    .reduce((promise, fileName) => {
      return promise.then(() => {
        console.log('【分包配置】 ' + fileName);
        let root = fileName.match(/pages\/(\S*)\//)[0];
        var temp = JSON.parse(filter_comments(fs.readFileSync(path.join(fileName), 'utf-8')));
        ergodic(temp, 'path', root);
        temp = {
          root: root,
          pages: temp
        };
        pages_sub_data = pages_sub_data.concat(temp);
      });
    }, Promise.resolve([]))
    .then(() => {
      console.log('【分包配置】  读取完毕');
      sortoutPages();
    });
}
function sortoutPages() {
  //以tabBar第一条为首页
  for (let key in pages_main_data) {
    if (pages_main_data[key].path == pages_common_data.tabBar.list[0].pagePath) {
      let temp = { ...pages_main_data[key] };
      pages_main_data.splice(key, 1);
      pages_main_data.unshift(temp);
    }
  }
  pages['pages'] = pages_main_data;
  pages['subPackages'] = pages_sub_data;
  pages['globalStyle'] = pages_common_data.globalStyle; //用于设置应用的状态栏、导航条、标题、窗口背景色等。
  pages['tabBar'] = pages_common_data.tabBar;
  pages['easycom'] = pages_common_data.easycom;
  pages['preloadRule'] = pages_common_data.preloadRule; //分包预载配置。
  pages['condition'] = pages_common_data.condition; //启动模式配置，仅开发期间生效，用于模拟直达页面的场景，如：小程序转发后，用户点击所打开的页面。
  pages = pages_diy(pages);
  fs.writeFileSync(projectSrc + '/pages.json', JSON.stringify(pages, '', 2));
  console.log('【更新完毕】');
}
/**
 * 遍历拼接文件地址
 * root 分包根目录
 */

function ergodic(data, page = 'pagePath', root = '') {
  for (const key in data) {
    new_file(root + data[key][page], data[key][page]);
  }
}

/* 新建文件 */
function new_file(dir) {
  fs.access(projectSrc + '/' + dir + '.vue', function (flag) {
    if (flag) {
      console.log('【新建文件】 ' + projectSrc + '/' + dir + '.vue');
      fs.writeFileSync(projectSrc + '/' + dir + '.vue', libTemplData.replace(/\<!-- path --\>/g, '<text>' + dir + '</text>'));
    }
  });
}

/* 过滤注释 */
function filter_comments(string) {
  string = string.replace(/\/\/[\s\S]*?(\r\n|\n)|\/\*[\s\S]*?\*\//g, ''); //过滤多行注释
  return string;
}

/* 抽取自定义配置diy-plus到 pages-diy-plus.js */
function pages_diy(pages) {
  if (argv !== 'plus') {
    return pages;
  }
  console.log('plus');
  let diyPlus = {};
  // 遍历主包配置

  for (const key in pages.pages) {
    if (pages.pages[key]['diy-plus']) {
      let k = pages.pages[key].path.replace(/\//g, '_');
      console.log('【抽取diy-plus】 ' + pages.pages[key].path, ' -> ', k);
      diyPlus[k] = pages.pages[key]['diy-plus'];
      delete pages.pages[key]['diy-plus'];
    }
  }

  //遍历分包
  for (const key in pages.subPackages) {
    for (const index in pages.subPackages[key].pages) {
      let root = pages.subPackages[key].root;
      if (pages.subPackages[key].pages[index]['diy-plus']) {
        let full_path = root + pages.subPackages[key].pages[index].path;
        let k = full_path.replace(/\//g, '_');
        console.log('【抽取diy-plus】 ' + full_path, ' -> ', k);
        diyPlus[k] = pages.subPackages[key].pages[index]['diy-plus'];
        delete pages.subPackages[key].pages[index]['diy-plus'];
      }
    }
  }
  if (diyPlus) {
    diyPlus = 'export default ' + JSON.stringify(diyPlus);
    console.log('【更新文件】' + projectSrc + '/pages-diy-plus.js');
    fs.writeFileSync(projectSrc + '/pages-diy-plus.js', diyPlus);
  }
  return pages;
}
