function scanCodeFunc() {
  // #ifdef H5
  uni.showToast({
    title: 'h5不支持扫码'
  });
  // #endif
  // #ifndef H5

  // #ifdef APP-PLUS
  var icon = plus.nativeObj.View.getViewById('icon0');
  if (icon) {
    setTimeout(function () {
      icon.hide();
    }, 5);
    uni.setTabBarItem({
      index: 0,
      selectedIconPath: '/static/a2.png'
    });
  }
  // #endif

  return new Promise((resolve, reject) => {
    uni.scanCode({
      scanType: ['qrCode'],
      success: function (res) {
        // uni.showModal({
        // 	title: '提示',
        // 	showCancel: false,
        // 	content:'条码内容：' + res.result
        // });
        console.log('条码类型：' + res.scanType);
        console.log('条码内容：' + res.result);

        let codeRes = res.result;

        // 模拟扫码成功
        // let codeRes="type=1&&qr_code=b9ed77aa9b17df6cc0683ea4088970ec&&time=1556091878";

        codeRes = codeRes.toString();
        let jianche = codeRes.slice(0, 5);

        if (jianche == 'type=') {
          let resArr = codeRes.split('&&');

          let ustypeArr = resArr[0].split('=');
          let ustype = ustypeArr[1]; //用户类型
          let usQrArr = resArr[1].split('=');
          let usQr = usQrArr[1]; //扫码结果
          return resolve({
            resArr: resArr,
            ustype: ustype,
            usQrArr: usQrArr,
            usQr: usQr
          });
        } else {
          uni.showToast({
            title: '不是该应用的二维码',
            icon: 'none'
          });
        }
      },
      complete: function (res) {
        console.log('scancomplete');
      },
      fail: (res) => {
        reject(res.result);
        console.log('failfailfailfailfailfailfailfailfailfailfail');
        uni.showToast({
          title: res.result
        });
      }
    });
  });

  // #endif
}
