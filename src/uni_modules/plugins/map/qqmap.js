import QQMapWX from '@/utils/qqmap/qqmap-wx-jssdk.js';
const Config = {
  // J4BBZ-4ZBKO-BDUWG-SAEMG-PFIPV-BLF2B
  qqMapKey: ''
};
const qqWxMap = new QQMapWX({
  key: Config.qqMapKey
});

/**
 * 腾讯地图
 * 小程序管理中心需要配置合法request路径: https://apis.map.qq.com
 */
export default class qqMap {
  static getLocateInfo() {
    let that = this;
    return new Promise(function (resolve, reject) {
      that.location().then(
        function (val) {
          //如果通过授权，那么直接使用腾讯的微信小程序sdk获取当前定位城市
          qqWxMap.reverseGeocoder({
            location: {
              latitude: val.latitude,
              longitude: val.longitude
            },
            success: function (res) {
              resolve(res.result); //返回城市
            },
            fail: function (res) {
              reject(res);
            },
            complete: function (res) {
              // console.log(res);
            }
          });
        },
        function (error) {
          //如果用户拒绝了授权，那么这里会提醒他，去授权后再定位
          console.log('您拒绝了授权,请授权后使用');
          uni.showModal({
            title: '提示',
            content: '自动定位需要授权地理定位选项',
            confirmText: '去授权',
            success: function (res) {}
          });
        }
      );
    });
  }

  /**
   * 定位，获取当前经纬度
   */
  static location() {
    return new Promise(function (resolve, reject) {
      uni.getLocation({
        type: 'gcj02',
        altitude: true,
        success: function (res) {
          resolve(res);
        },
        fail(res) {
          reject(res);
        }
      });
    });
  }

  static getCityList() {
    return new Promise(function (resolve, reject) {
      qqWxMap.getCityList({
        success: function (res) {
          resolve(res);
        },
        fail: function (res) {
          reject(res);
        }
      });
    });
  }
  static geocoder(addres) {
    return new Promise(function (resolve, reject) {
      qqWxMap.geocoder({
        address: addres,
        success: function (res) {
          resolve(res);
        },
        fail: function (res) {
          reject(res);
        }
      });
    });
  }
}
