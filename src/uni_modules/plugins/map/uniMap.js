/**
 * uni-app 自带地图
 */
export default class uniMap {
  /**
   * 获取定位信息
   *
   * @return {Object}
   *        res.longitude //经度
   *        res.latitude  //纬度
   */
  static getLocation() {
    return new Promise((resolve, reject) => {
      uni.getLocation({
        type: 'gcj02',
        success: function (res) {
          resolve(res);
        },
        fail(err) {
          reject(err);
        }
      });
    });
  }
  /**
   * 选择地图
   * @param {Object} keyword 搜索关键字
   * @return {Object}
   *
   *   位置名称：res.name
   *   详细地址： res.address
   *   纬度： res.latitude
   *   经度：res.longitude
   */
  static chooseLocation(keyword) {
    return new Promise((resolve, reject) => {
      uni.chooseLocation({
        keyword: keyword,
        success(res) {
          resolve(res);
        },
        fail(err) {
          reject(err);
        }
      });
    });
  }

  /**
   * 打开地图
   * @param {Object} lat  经度
   * @param {Object} lng  纬度
   */
  static openLocation(lat, lng) {
    return new Promise((resolve, reject) => {
      uni.openLocation({
        latitude: latitude,
        longitude: longitude,
        success: function () {
          resolve();
        },
        fail(err) {
          reject(err);
        }
      });
    });
  }
}
