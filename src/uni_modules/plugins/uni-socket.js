import cConfig from '@/utils/ctoui-config.js';

const SocketConnectInit = 'JKLINK_IM|connec_init|cucenterTC|' + cConfig.syOpenAppsId + '|';
const SocketConnectRefresh = 'JKLINK_IM|connec_refresh|cucenterTC|' + cConfig.syOpenAppsId + '|';
const s_data = {
  beat_duration: 30000, //心跳间隔
  limit_times: 60, //重连限制
  reconnet_times: 0 //当前重连次数
};
var socket_status = false; //回话状态（主动）
var connect_status = false; //连接状态（被动）
//心跳时间
// 账户信息存在时执行
//页面栈，
var pagelist = [];
//this = '';
//pagetype = ''; //msglist ,  chat
//chat_id = ''; //when chat

const SocketClass = {
  /**
   * 主动关闭,监听端口
   */
  close_socket: function () {
    socket_status = false;
    uni.closeSocket({
      url: cConfig._URL_WSS_IM1_
    });
  },
  xintiaoFunc: function () {
    let timer = setInterval(function () {
      if (!socket_status) {
        console.log('已关闭心跳');
        clearInterval(timer);
        return;
      }
      //发送心跳
      if (connect_status)
        uni.sendSocketMessage({
          data: SocketConnectRefresh + this.syAppAccess('ucenter_id'),
          success: function (successRes) {
            s_data.reconnet_times = 0;
          },
          fail: function (failRes) {
            console.log('发送失败', failRes);
          }
        });
      else {
        console.log('尝试断线重连');
        clearInterval(timer);
        if (s_data.reconnet_times <= s_data.limit_times) {
          SocketClass.close_socket();
          SocketClass.init();
          s_data.reconnet_times++;
        }
        return;
      }
    }, s_data.beat_duration);
  },
  init: function () {
    // // uni.showLoading({s
    // // title: '正在初始化客户端...',
    // // mask: true
    // // })

    if (!this.syAppAccess('ucenter_id')) {
      console.log('未登录');
      return;
    }
    socket_status = true;
    // 连接端口
    uni.connectSocket({
      url: cConfig._URL_WSS_IM1_
    });
    // 端口++开启
    uni.onSocketOpen(function (res) {
      uni.sendSocketMessage({
        data: SocketConnectInit + this.syAppAccess('ucenter_id'),
        success: function (successRes) {
          connect_status = true;
          s_data.reconnet_times = 0;
          console.log('WebSocket已打开！');
          console.log('connect success', successRes);
          SocketClass.xintiaoFunc();
        }
      });
    });
    // 接收端口消息
    uni.onSocketMessage(function (res) {
      let data = JSON.parse(res.data);
      if (data.wsdoflag == 'connec_refresh') return;
      console.log('收到服务器内容：', data);
      receive_data(data);
    });
    // 线上端口关闭
    uni.onSocketClose(function (res) {
      connect_status = false;
      console.log('WS连接已断开,推送关闭！');
    });
    //监听socket失败
    uni.onSocketError(function (res) {
      console.log('WebSocket连接打开失败，请检查！');
      setTimeout(function () {
        console.log('监听socket失败  尝试重新连接');
        if (s_data.reconnet_times <= s_data.limit_times) {
          SocketClass.init();
          s_data.reconnet_times++;
        }
        return;
      }, s_data.beat_duration);
    });
  },

  set_page: function (type, _this, chat_id) {
    //type为空默认关闭
    if (!type) {
      if (pagelist.length > 0) pagelist.pop();
      return;
    }
    if (!connect_status) return;
    //type      msglist ,  chat
    if (type != 'msglist' && type != 'chat') {
      console.log('**set_page****type传参错误*********');
      return;
    }
    if (type == 'chat' && (typeof chat_id != 'number' || !chat_id)) {
      console.log('***set_page***chat_id传参错误*********');
      return;
    }
    if (!_this) {
      console.log('***set_page***this传参错误*********');
      return;
    }
    console.log('page：' + type + '载入完成');
    let item = {
      _this: _this,
      type: type,
      chat_id: chat_id || 0
    };
    if (type == 'msglist') pagelist = [item];
    else pagelist[1] = item;
  },
  get_connect_status: function () {
    return connect_status;
  },
  get_socket_status: function () {
    return socket_status;
  }

  //
  /**
   * 连接,监听端口
   */
};

/**
 * 接收数据分发
 */
function receive_data(data) {
  if (data.wsdoflag != 'user_creat_connect_customer_service' && data.wsdoflag != 'letter_new_record_insert') return;
  for (let i in pagelist) {
    let _this = pagelist[i]._this;
    if (pagelist[i].type == 'msglist') {
      console.log('set_socket_data msglist');
      typeof _this.set_socket_data == 'function' && _this.set_socket_data(data);
    }
    if (pagelist[i].type == 'chat' && pagelist[i].chat_id == data.data[0].chat_id) {
      console.log('set_socket_data    chat');
      typeof _this.set_socket_data == 'function' && _this.set_socket_data(data);
    }
  }
}

export default SocketClass;
