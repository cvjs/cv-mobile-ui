function uniEleOffset(selector) {
  // 获取高度
  return new Promise((resolve) =>
    uni
      .createSelectorQuery()
      .in(this)
      .select(selector)
      .boundingClientRect((data) => resolve(data))
      .exec()
  );
}

export { uniEleOffset };
