import HttpObj from '@/plugins/http.js';
function checkVersion(plus,widgetInfo){
  uni.request({
    url: 'https://uniapp.dcloud.io/update', // 检查更新的服务器地址
    data: {
      appid: (plus.runtime.appid || ''),
      version: (plus.runtime.version || ''),
      imei: (plus.device.imei || ''),
      appid: widgetInfo.appid, //当前应用的APPID
      version: widgetInfo.version, // 当前应用的版本  
      name: widgetInfo.name, // 当前应用的名称 
      imei: plus.device.imei,
      os: plus.os.name // OS环境 ios / android 
    },
    success: res => {
      if (res.code == 200) {
        if(res.data.version_update ='no_need'){
          uni.showModal({
            title: '提示',
            content: '已是最新版本',
            showCancel: false
          });
        }else{
          if(res.data.version_update ='yes_need'){
            showVersion(res.data);
          }
        }
      } 
    }
  });
}
function showVersion(apiData){
  
  
  // 提醒用户更新
  uni.showModal({
    title: '更新提示',
    title: '版本更新' + apiData.versionCode,
    content: apiData.note ? apiData.note : '是否选择更新',
    content: apiData.description,
    confirmText: '更新',
    showCancel: !res.forceUpdate,
    success: showResult => {
      if (showResult.confirm) {
        downVersion(apiData);
      } else {
        //取消
      }
    }
  });
}
function downVersion(){


  if (plus.os.name.toLowerCase() == 'ios') {
    // 跳转到下载页面
    let upgradeUrl = apiData.url_ios;
    plus.runtime.openURL(res.data.upgradeUrl);
  } else {
    // let upgradeUrl =  apiData.url_android;
    // plus.runtime.openURL(upgradeUrl);
    var dtask = plus.downloader.createDownload(res.data.upgradeUrl, {}, function (d, status) {
      uni.showToast({
        title: '下载完成',
        mask: false,
        duration: 1000
      });
      // 下载完成
      if (status == 200) {
        plus.runtime.install(
          plus.io.convertLocalFileSystemURL(d.filename),
          {},
          (e) => e,
          function (error) {
            uni.showToast({
              title: '安装失败-01',
              mask: false,
              duration: 1500
            });
          }
        );
      } else {
        uni.showToast({
          title: '更新失败-02',
          mask: false,
          duration: 1500
        });
      }
    });
    try {
      dtask.start(); // 开启下载的任务
      var prg = 0;
      var showLoading = plus.nativeUI.showWaiting('正在下载'); //创建一个showWaiting对象
      dtask.addEventListener('statechanged', function (task, status) {
        // 给下载任务设置一个监听 并根据状态  做操作
        switch (task.state) {
          case 1:
            showLoading.setTitle('正在下载');
            break;
          case 2:
            showLoading.setTitle('已连接到服务器');
            break;
          case 3:
            prg = parseInt((parseFloat(task.downloadedSize) / parseFloat(task.totalSize)) * 100);
            showLoading.setTitle('  正在下载' + prg + '%  ');
            break;
          case 4:
            plus.nativeUI.closeWaiting();
            //下载完成
            break;
        }
      });
    } catch (err) {
      plus.nativeUI.closeWaiting();
      uni.showToast({
        title: '更新失败-03',
        mask: false,
        duration: 1500
      });
    }
  }
} 
function downloadVersion(){
  uni.downloadFile({
    url: res.data.wgtUrl,
    success: (downloadResult) => {
      if (downloadResult.statusCode === 200) {
        plus.runtime.install(downloadResult.tempFilePath, {
          force: false
        }, (d) => {
          console.log('install success...');
          plus.runtime.restart();
        }, (e) => {
          console.error('install fail...');
        })
      }
    }
  });
}

// APP检测更新 具体可以参考：https://ask.dcloud.net.cn/article/35667
plus.runtime.getProperty(plus.runtime.appid, widgetInfo => {
  checkVersion(plus,widgetInfo); 
});

var UniUpgradeClass = function{
  // console.log('App Launch')
  // 调用API从本地缓存中获取数据
  var logs = uni.getStorageSync('logs') || [];
  logs.unshift(Date.now());
  uni.setStorageSync('logs', logs);
  // 展示本地存储能力
  if (uni.canIUse('getUpdateManager')) {
    const updateManager = uni.getUpdateManager();

    updateManager.onCheckForUpdate((ForUpdateRes) => {
      // 请求完新版本信息的回调
      if (ForUpdateRes.hasUpdate === true) { //有新的版本
        uni.showLoading({
          title: '应用升级中', //正在加载中
          mask: true
        }); 
      }
    });

    updateManager.onUpdateReady((ReadyRes) => {
      uni.hideLoading();
      uni.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        success: function (ModalRes) {
          if (ModalRes.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
            updateManager.applyUpdate()
          }
        }
      })
    })
    updateManager.onUpdateFailed((FailedRes) => {
      uni.hideLoading();
      // 新的版本下载失败
      uni.showModal({
        title: '更新提示',
        content: '新版本下载失败',
        showCancel: false
      })
      uni.showModal({
        title: '已经是最新版本了呦',
        content: '或请您删除当前小程序，重新搜索打开哟',
        showCancel: false
      })
    });
  }
}
function showPlusVersion() {
  // 跟后台传过来的版本号比对，如果版本号不一致
  plus.nativeUI.confirm(
    '检测到有新版本，是否更新',
    function (e) {
      /**
       * 如果是苹果手机
       */
      if (navigator.userAgent.match(/(iPhone|iPod|iPad);?/i)) {
        /**
         * 如果选择更新
         */
        if (e.index == 0) {
          window.location.href = 'itms-services://?action=download-manifest&url=https://ios.17rua.top/static/ios/x5.plist';
          //不经过苹果商店下载（不懂得看我的另一篇文章）
          plus.nativeUI.showWaiting('正在下载...');
        }
      } else {
        if (e.index == 0) {
          that.downWgt(res.data.url); //下载文件
        } else {
          plus.runtime.quit(); //安卓控制不更新退出应用
        }
      }
    },
    '',
    ['立即更新', '以后再说']
  );
}

function versionCheckUpdate() {
  var that = this;
  HttpObj.post('/version/checkUpdate', {
    version: that.wgtVer
  })
    .then((res) => {
      if (res.data.code == 0) {
        showPlusVersion();
        // 检验过一次版本就加入缓存，不在检测
        sessionStorage.setItem('kbj_banben', true);
      } else {
        alert('获取数据失败');
      }
    })
    .catch(function (error) {
      alert('请检查网络连接');
    });
}
let xx = {
  //检测当前版本号
  dqbanben: function () {
    var that = this;
    //在页面中初始化plus插件
    mui.init();
    mui.plusReady(function () {
      // 获取本地应用资源版本号
      that.wgtVer = plus.runtime.version;
      that.checkUpdate();
    });
  },
  //检查更新
  checkUpdate: function () {},
  //下载资源包
  downWgt: function (wgtUrl) {
    var that = this;
    var task = plus.downloader.createDownload(wgtUrl, {}, function (download, status) {
      //安装到手机的目录
      if (status == 200) {
        plus.runtime.install(download.filename); // 安装下载的apk文件
      } else {
        mui.toast('下载更新失败！');
        plus.nativeUI.closeWaiting();
      }
    });
    //监听下载
    task.addEventListener('statechanged', function (download, status) {
      switch (download.state) {
        case 2:
          plus.nativeUI.showWaiting('正在下载...');
          break;
        case 3:
          //进度条百分比 totalSize为总量，baifen为当前下载的百分比
          if (that.totalSize == 0) {
            that.totalSize = parseInt(download.totalSize);
          }
          if (parseInt((download.downloadedSize / that.totalSize) * 100) != that.baifen) {
            that.baifen = parseInt((download.downloadedSize / that.totalSize) * 100);
          }
          break;
        case 4:
          mui.toast('下载完成');
          plus.nativeUI.closeWaiting();
          break;
      }
    });
    task.start();
  }
};