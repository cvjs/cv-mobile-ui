const MessageBox = function (options) {};

/**
 *  alert 消息提示
 *  @author cvjs
 * 	@param {string} content 内容，默认为空
 *  @param {string} title		标题，默认为空
 *  @param {string} confirmText 	确认按钮，默认确定
 * */
MessageBox['alert'] = (content, title, confirmText) => {
  return new Promise((res, rej) => {
    uni.showModal({
      title: title || '',
      content: String(content || '') || '',
      confirmText: confirmText || '确定',
      showCancel: false,
      success: function (modalRes) {
        if (modalRes.confirm) {
          res(modalRes);
        } else if (modalRes.cancel) {
          console.log('用户点击了取消');
        }
      },
      fail: function (err) {
        rej(err);
      }
    });
  });
};
/**
 *  confirm 确认消息
 *  @author cvjs
 *  @param {string} title		 确认标题，	默认提示
 * 	@param {string} content  确认内容，	默认？？
 * */
MessageBox['confirm'] = (content, title) => {
  return new Promise((res, rej) => {
    uni.showModal({
      title: title || '提示',
      content: String(content || '') || '？？',
      confirmText: '确定',
      showCancel: true,
      cancelText: '取消',
      success: function (modalRes) {
        if (modalRes.confirm) {
          res(modalRes);
        } else if (modalRes.cancel) {
          console.log('用户点击了取消');
        }
      },
      fail: function (err) {
        rej(err);
      }
    });
  });
};

/**
 *  prompt 提交内容
 *  @author cvjs
 *  @param {string} title		标题，默认请输入
 * 	@param {string} content 输入内容，默认为空
 * */
MessageBox['prompt'] = (content, title) => {
  return new Promise((res, rej) => {
    uni.showModal({
      title: title || '请输入',
      content: String(content || '') || '',
      confirmText: '确定',
      showCancel: true,
      cancelText: '取消',
      editable: true,
      success: function (modalRes) {
        if (modalRes.confirm) {
          res(modalRes);
        } else if (modalRes.cancel) {
          console.log('用户点击了取消');
        }
      },
      fail: function (err) {
        rej(err);
      }
    });
  });
};

export default MessageBox;
