// console.log(plus.device.getInfo);
function requestDownloadFile(apiUrl, paramData) {
  return requestApi('download', apiUrl, paramData);
}
function _baseResponse() {}
function confirmEnding(str, target) {
  // 请把你的代码写在这里
  var start = str.length - target.length;
  var arr = str.substr(start, target.length);
  if (arr == target) {
    return true;
  }
  return false;
}
function _hideUniapp() {
  try {
    uni.hideToast();
    uni.hideLoading();
    uni.hideNavigationBarLoading();
    uni.stopPullDownRefresh();
  } catch (e) {}
}
function showErrorMsg(msg) {
  uni.hideToast();
  uni.hideLoading();
  uni.showToast({
    icon: 'none',
    duration: 2000,
    title: msg || ''
  });
}
/*
 * 收集错误信息
 */
const EnumStatusToTitle = {
  400: '请求错误',
  400: '无效请求',
  401: '未经授权',
  403: '拒绝访问',
  404: '请求出错',
  408: '请求超时',
  422: '请求参数错误',
  500: '服务器错误',
  501: '服务未实现',
  502: '网络错误',
  503: '服务不可用',
  504: '网络超时',
  505: 'HTTP版本不受支持',
  10001: '未登录',
  10002: 'token过期',
  10009: '退出登录'
};
const EnumStatusToMsg = {
  401: '没有权限，请重新登录'
};

class RequestClass {
  constructor(options = {}) {
    this.flagMap = options.flagMap || {};
    this.flagFunc = options.flagFunc;
    this.baseURL = options.baseURL || '';
    this.requests = Object.assign({}, options.requests || {});
    /**
     * 内置的，支持的 methods
     * 处理，需要转换的 methods
     */
    let inlayMehtod = ['PUT', 'PATCH', 'DELETE'];
    let supportMethod = options.needMethods || [];
    let needMethods = [];
    for (var i = 0; i < supportMethod.length; i++) {
      let daxieItem = supportMethod[i].toUpperCase();
      if (inlayMehtod.indexOf(daxieItem) !== -1) {
        needMethods.push(daxieItem);
      }
    }
    this.needMethods = needMethods;

    // 如果你想重新规定返回的数据格式，那么可以借助 response 参数，如：
    let response = {
      statusName: 'status', // 规定数据状态的字段名称，默认：code
      statusCode: 200, // 规定成功的状态码，默认：0
      msgName: 'msg', // 规定状态信息的字段名称，默认：msg
      countName: 'total', // 规定数据总数的字段名称，默认：count
      dataName: 'data' // 规定数据列表的字段名称，默认：data
    };

    this.headers = Object.assign({}, {}, options.headers || {});
    // 需要加密的url
    this.encryptURL = options.encryptURL || [];
    this.encryptFlag = false;
    this.canshuOpt = {};
    this.retries = 3;
    this.debugState = options.debug || false;
    this.requestHandle = null;
    /* 新创建 axios 实例配置 */
    this.creSett = Object.assign(
      {},
      {
        timeout: options.timeout || 5000, // request timeout
        // 表示跨域请求时是否需要使用凭证;
        // 开启withCredentials后，服务器才能拿到你的cookie
        // 当然后端服务器也要设置允许你获取你开启了才有用
        // withCredentials: true, // 开启跨域身份凭证
        retry: 2
      },
      {}
    );
    this._initReqHandle();
  }
  debugLog() {
    if (this.debugState) {
      console.log('[调试]', ...arguments);
    }
  }
  /* 初始化 request 句柄 */
  _initReqHandle() {
    let _this = this;
  }

  /**
   * 设置flag参数
   */
  setFlag(newFlagMap) {
    this.flagMap = Object.assign({}, this.flagMap, newFlagMap || {});
    return this;
  }
  parseFlag(oldFlag) {
    // 类型对
    // 不存在
    // 重新获取flag方法存在
    if (typeof oldFlag == 'string' && !this.flagMap[oldFlag] && typeof this.flagFunc == 'function') {
      // 重新加载flag
      let newFlagArr = this.flagFunc();
      // console.log(newFlagArr);
      this.setFlag(newFlagArr);
    }
    return oldFlag;
  }
  /**
   * 设置header参数
   * @param {Object} options
   * @returns {Class} this
   */
  setDefHeaders(options) {
    this.headers = Object.assign({}, this.headers, options || {});
    // this.reqObj.defaults.headers = this.headers;
    return this;
  }
  setHeaders(options) {
    this.headers = Object.assign({}, this.headers, options || {});
    return this;
  }
  /**
   * 设置requests参数
   * @param {Object} options
   * @returns {Class} this
   */
  setDefRequests(options) {
    this.requests = Object.assign({}, this.requests, options || {});
    // this.reqObj.defaults.requests = this.requests;
    return this;
  }
  setRequests(options) {
    this.requests = Object.assign({}, this.requests, options || {});
    return this;
  }
  /**
   * 设置body 参数
   * 设置 额外参数
   * @param {Object} options
   * @returns {Class} this
   */
  setOptions(options = {}) {
    this.canshuOpt = Object.assign({}, this.canshuOpt, options || {});
    return this;
  }
  /**
   * 设置 signal
   */
  setSignal(signal) {}
  /**
   * 请求前置
   * 请求后置
   */
  requestBefore(callFunc) {
    this.requestBeforeCall = callFunc;
  }
  requestAfter(callFunc) {
    this.requestAfterCall = callFunc;
  }
  /**
   * 加密
   */
  encrypt() {
    this.encryptFlag = true;
    return this;
  }
  /***
   * ================== ================== ================== ==================
   * 允许重写
   * ================== ================== ================== ==================
   */
  /**
   * 设置错误时响应
   */
  resetNetError(error) {
    showErrorMsg(error.message || '');
    console.log(error.message || '');
  }
  /**
   * 设置网络请求响应
   * 设置自定义业务响应
   */
  resetNetResponse(apiResData) {
    // 修改请求状态
    _hideUniapp();

    let apiResStatus = apiResData.status || apiResData.code || -1;
    let apiResMessage = apiResData.msg || apiResData.message || '操作失败';
    apiResStatus = parseInt(apiResStatus);
    // 这里根据后端提供的数据进行对应的处理
    switch (apiResStatus) {
      // 没有凭证，请登录
      case 100101:
        // 【退出登录操作】
        this.resetNetError({ type: 'error', message: apiResMessage });
        break;
      // 无效凭证，请重新登录
      case 100102:
        // 【退出登录操作】
        let errorData = Object.assign(apiResData, { type: 'error', message: apiResMessage });
        this.resetNetError(errorData);
        break;
      // 凭证过期，请重新登录
      case 100103:
        // 【退出登录操作】
        return;
        break;
      // syOpenAppToken 错误
      case 100109:
        break;
      case 422:
      case 100422:
        let msg422 = apiResMessage || '操作失败';
        this.resetNetError({ type: 'error', message: `${msg422}` });
        return Promise.reject(apiResData);
        break;
      case 404:
      case 100404:
        let msg404 = apiResMessage || '操作失败';
        this.resetNetError({ type: 'error', message: `${msg404}` });
        return Promise.reject(apiResData);
      // return new Promise((resolve, reject) => {});
      case '8002':
      case 8002: // uni.login 微信注册
        //
        break;
      case '13001':
      case 13001:
        // eid 实名认证
        uni.showModal({
          title: '提示',
          content: apiResData.msg,
          showCancel: true,
          success: function (res) {
            if (res.confirm) {
            } else {
              console.log('用户点击取消');
            }
          }
        });
        break;
      // 处理成功
      case 200:
        // this.requestAfterCall?.();
        return apiResData;
        return Promise.resolve(apiResData);
        break;
      // 调试
      case -1:
        return Promise.reject({ message: 'error' });
        break;
      default:
        return apiResData;
        break;
    }
    console.log('---');
  }
  /**
   * 调用 API 统一 请求 方法
   *
   * @author ctocode-zhw
   * @version 2018-12-20
   * @param apiUrl 不需要域名，域名统一拼接，只填写相关请求接口 【必须】
   * @param reqData 提交参数
   * @return
   *
   */
  apiAll(type, apiUrl, reqData) {
    let request_url = this.baseURL + apiUrl;
    // if (apiUrl.indexOf('http://') != -1 || apiUrl.indexOf('https://') != -1) {
    if (apiUrl.includes('http://') || apiUrl.includes('https://')) {
      request_url = apiUrl;
    }
    /**
     * TODO 过滤除了 https:// 和 http:// 以外的 // 和 ///，但保留 ? 后面的内容
     * request_url = request_url.replace(/^(https?:\/\/[^/]+)\/+/g, '$1/');
     */
    /**
     * 使用正则表达式进行过滤
     * 过滤除了 https:// 和 http:// 以外的 // 和 ///
     */
    request_url = request_url.replace(/([^:]\/)\/+/g, '$1');

    var _this = this;

    reqData = reqData || {};
    reqData = Object.assign({}, this.requests, reqData);
    type = type || 'request';
    type = type.toLowerCase();
    /*
     * 全局 uni.request 配置过滤不需要登录的页面
     */

    // uni.showNavigationBarLoading(); // 顶部加载动画

    // 初始化句柄
    let $requestHandle = null;
    let request_setttings = {
      url: request_url,
      header: this.headers
    };
    if (type == 'upload') {
      /* 文件上传 */
      // 一定要删除这个，让uni.uploadFile自己携带
      delete request_setttings.header['Content-Type'];
      request_setttings = Object.assign({}, request_setttings, reqData);
      $requestHandle = uni.uploadFile(request_setttings);
    } else if (type == 'download') {
      /* 文件下载 */
    } else {
      /* request */

      request_setttings.header['Content-Type'] = 'application/json';
      // request_setttings.header['Content-Type'] = 'application/x-www-form-urlencoded';
      // request_setttings.header['connection'] = 'close';

      let MetSupport = this.needMethods || [];

      if (MetSupport.length > 0) {
        // 转大写
        let typeDaxie = type.toUpperCase();
        // console.log(MetSupport, type, MetSupport.indexOf(typeDaxie));
        if (MetSupport.indexOf(typeDaxie) !== -1) {
          type = 'POST';
          request_setttings.header['X-HTTP-Method-Override'] = typeDaxie;
        } else {
          delete request_setttings.header['X-HTTP-Method-Override'];
        }
      }

      // 全部默认加上 login_type
      Object.assign(request_setttings, {
        data: reqData,
        method: type
      });
      // xxx = {
      //   header: {
      //     ...this.headers
      //   },
      //   data: {
      //     ...this.requests,
      //     reqData,
      //   },
      // }
      $requestHandle = uni.request(request_setttings);
    }
    return $requestHandle.then((uniRes) => {
      _hideUniapp();
      // console.log('----开始请求啊  ------', uniRes);
      let apiResData = uniRes.data || {};
      // uniRes 为微信返回 + 接口返回 拼接的数据
      if (uniRes.statusCode == 200) {
        // uni.hideNavigationBarLoading();

        // apiResData 为 接口返回 拼接的数据，一般 success 成功，开发者只需要 apiResData
        // let apiResData = JSON.parse(uniRes.data, true);

        // upload需要json转数组
        if (type == 'upload') {
          apiResData = JSON.parse(apiResData, true);
        }
        return _this.resetNetResponse(apiResData);
      } else if (uniRes.statusCode == 502) {
      } else {
        return _this.resetNetResponse(apiResData);
      }
    });
    // .catch((uniRes) => {
    //   console.log('--request-catch--', uniRes);
    //   return Promise.reject(uniRes);
    // });
  }
  /**
   * GET 请求
   */
  flagGet() {
    const argumentsArr = arguments;
    return this.unifyApi('flag', 'GET', argumentsArr);
  }
  urlGet() {
    const argumentsArr = arguments;
    return this.unifyApi('url', 'GET', argumentsArr);
  }
  /**
   * POST 请求
   */
  flagPost() {
    const argumentsArr = arguments;
    return this.unifyApi('flag', 'POST', argumentsArr);
  }
  urlPost() {
    const argumentsArr = arguments;
    return this.unifyApi('url', 'POST', argumentsArr);
  }
  /**
   * PUT 请求
   */
  flagPut() {
    const argumentsArr = arguments;
    return this.unifyApi('flag', 'PUT', argumentsArr);
  }
  urlPut() {
    const argumentsArr = arguments;
    return this.unifyApi('url', 'PUT', argumentsArr);
  }
  /**
   * PATCH 请求
   */
  flagPatch() {
    const argumentsArr = arguments;
    return this.unifyApi('flag', 'PATCH', argumentsArr);
  }
  urlPatch() {
    const argumentsArr = arguments;
    return this.unifyApi('url', 'PATCH', argumentsArr);
  }
  /**
   * DELETE 请求
   */
  flagDel() {
    const argumentsArr = arguments;
    return this.unifyApi('flag', 'DELETE', argumentsArr);
  }
  urlDel() {
    const argumentsArr = arguments;
    return this.unifyApi('url', 'DELETE', argumentsArr);
  }
  /* 文件上传通用 */
  flagUpload() {
    const argumentsArr = arguments;
    return this.unifyApi('flag', 'UPLOAD', argumentsArr);
  }
  urlUpload() {
    const argumentsArr = arguments;
    return this.unifyApi('url', 'UPLOAD', argumentsArr);
  }
  /* 图片上传通用 */
  flagUpImg() {
    const argumentsArr = arguments;
    return this.unifyApi('flag', 'UPLOAD', argumentsArr);
  }
  urlUpImg() {
    const argumentsArr = arguments;
    return this.unifyApi('url', 'UPLOAD', argumentsArr);
  }
  /**
   * 统一请求
   */
  unifyApi(flagOrUrl, methodType, argumentsArr) {
    let _this = this;
    let argNum = argumentsArr.length || 0;
    if (argNum <= 0 || argNum > 3) {
      let errorMsg = `-- ${methodType} ： 参数为(1-3)个---`;
      return new Promise((resolve, reject) => {
        this.resetNetError({ message: errorMsg });
        return reject({ message: errorMsg });
      });
    }
    let apiUrl = '';
    let apiUrlNoMsg = '';
    if (flagOrUrl == 'flag') {
      let apiFlag = this.parseFlag(argumentsArr[0]);
      if (typeof apiFlag === 'string') {
        apiFlag = apiFlag || '';
        apiUrl = apiFlag == '' ? '' : this.flagMap[apiFlag] || '';
      }
      apiUrlNoMsg = ' flag' + methodType + '：传入的标示 { ' + `${apiFlag}` + ' }，未匹配到 apiUrl ';
    }
    if (flagOrUrl == 'url') {
      apiUrl = argumentsArr[0];
      if (typeof apiUrl === 'string') {
        apiUrl = apiUrl || '';
      }
      apiUrlNoMsg = ' 未匹配到 apiUrl ';
    }
    if (apiUrl == '') {
      return new Promise((resolve, reject) => {
        this.resetNetError({ message: apiUrlNoMsg });
        return reject({ message: apiUrlNoMsg });
      });
    }

    // 如果传回一个
    if (argNum == 1) {
      return function (reqData) {
        return _this.apiAll(methodType, apiUrl, reqData);
      };
    } else if (argNum == 2 || argNum == 3) {
      let reqData = {};
      let arg2Val = argumentsArr[1];
      let arg2Type = Object.prototype.toString.call(arg2Val);
      // let argType2_2 = typeof argumentsArr[1] === 'object';
      if (arg2Type === '[object String]' || arg2Type === '[object Number]') {
        // console.log('----转换-----', BigInt(arg2Val), Number(arg2Val)); 超过17位
        if (!isNaN(arg2Val)) {
          // console.log('---数字');
          let bigIntNum = BigInt(`${arg2Val}`);
          let isEndId = bigIntNum.toString();
          if (!confirmEnding(apiUrl, '/')) {
            apiUrl = apiUrl + '/';
          }
          apiUrl = apiUrl + isEndId;
        } else {
          // console.log('---字符');
        }
        if (argNum == 3) {
          let arg3Val = argumentsArr[2];
          let arg3Type = Object.prototype.toString.call(arg3Val);
          if (arg3Type === '[object Object]') {
            reqData = arg3Val || {};
          }
        }
      } else if (arg2Type === '[object Object]') {
        reqData = arg2Val || {};
      }
      return _this.apiAll(methodType, apiUrl, reqData);
    } else {
      return new Promise((resolve, reject) => {
        return reject({ message: ' 请求错误 ' });
      });
    }
  }
}

export default RequestClass;
