/**
 * 缓存数据优化
 * 使用方法
 */

function get_data1(key, def) {
  uni.getStorage({
    key: key,
    success: function (res) {
      param.success(res.data || def);
    }
  });
}
function get_data2(key, def) {
  const value = uni.getStorageSync(key);
  return value || def;
}
class StorageClass {
  //实例化类时默认会执行构造函数
  constructor(options = {}) {
    this.config = Object.assign(
      {
        cache_keys: 'syCacheKeys',
        prefix: '',
        suffix: ''
      },
      options
    );
    this.KEYS_CACHE_FLAG = this.config.cache_keys;
  }
  _getAllCache() {
    let fullKey = this._getFullKey('___') || {};
    let fullCache = uni.getStorageSync(fullKey);
    return fullCache;
  }
  _setAllCache(appendKey) {
    let fullKey = this._getFullKey('___');
    let fullCache = uni.getStorageSync(fullKey) || {};
    fullCache[appendKey] = 1;
    uni.setStorageSync(fullKey, fullCache);
  }
  _removeAllCache(removeKey) {
    let fullKey = this._getFullKey('___');
    let fullCache = uni.getStorageSync(fullKey) || {};
    delete fullCache[removeKey];
    uni.setStorageSync(fullKey, fullCache);
  }
  _getFullKey(key) {
    key = key || '';
    const config = this.config;
    if (config.prefix) {
      key = config.prefix + key;
    }
    if (config.suffix) {
      key += config.suffix;
    }
    return key;
  }
  localGet(key) {
    return this.getSync(key) || '';
  }
  localSet(key, value) {
    this.updateKey(key);
    this.setAsync(key, value);
    this.setSync(key, value);
  }
  localDel(key) {
    this.removeAsync(key);
    this.removeSync(key);
  }
  updateKey(key) {
    let allCacheKeys = this.getSync(this.KEYS_CACHE_FLAG) || {};
    // 当前时间
    let currTime = parseInt(new Date().getTime() / 1000);
    allCacheKeys[key] = currTime + 60 * 60 * 24 * 7; // 7天
    this.setSync(this.KEYS_CACHE_FLAG, allCacheKeys);
  }
  checkKey(key) {
    // 所有缓存keys
    let allCacheKeys = this.getSync(this.KEYS_CACHE_FLAG) || {};
    // 缓存时间
    let currCacheExpires = allCacheKeys[key] || 0;
    if (currCacheExpires > 0) {
      return true;
    }
    // 当前时间
    let currTime = parseInt(new Date().getTime() / 1000);
    // 当前缓存
    let currCache = this.getSync(key) || null;
    if (!currCache) {
      return false;
    }
    console.log(currCache);
    if (currCache.length == 0 || currTime > currCacheExpires) {
      return false;
    }
    return true;
  }
  /**
   * 设置缓存 - 异步
   * @param  {[type]} key [键名]
   * @param  {[type]} data [键值]
   * @param  {[type]} t [时间、单位秒]
   * @使用方式 ==== 【设置缓存】
   *     string    cache.put('k', 'string你好啊');
   *     json      cache.put('k', { "b": "3" }, 2);
   *     array     cache.put('k', [1, 2, 3]);
   *     boolean   cache.put('k', true);
   */

  setAsync(key = '', data = '', t = 0) {
    let fullKey = this._getFullKey(key);
    uni.setStorage({
      key: fullKey,
      data: data,
      success: () => {}
    });
    /* 设置过期时间 */
    if (t) {
      let timestamp = parseInt(new Date().getTime());
      uni.setStorage({
        key: fullKey + '_time',
        data: timestamp + t * 1000,
        success: () => {}
      });
    }
    this._setAllCache(fullKey);
  }
  setSync(key = '', data = null, t = 0) {
    let fullKey = this._getFullKey(key);
    try {
      uni.setStorageSync(fullKey, data);
      /* 设置过期时间 */
      if (t) {
        let timestamp = parseInt(new Date().getTime());
        uni.setStorageSync(fullKey + '_time', timestamp + t * 1000);
      }
      this._setAllCache(fullKey);
    } catch (e) {
      console.error(e);
    }
  }
  /**
   * 获取缓存
   * @param  {[type]} key   [键名]
   * @param  {[type]} def [获取为空时默认]
   *
   * @使用方式 ==== 【读取缓存】
   *    默认值     cache.get('key')
   *    string    cache.get('key', '你好')
   *    json      cache.get('key', { "a": "1" })
   */
  /**
   * 根据key获取数据缓存 -  异步
   * @param {String} key
   */
  getAsync(param) {
    param = Object.assign({ def: '' }, param);

    let fullKey = this._getFullKey(param.key);

    //先获取时间
    var deadtime = 0;
    uni.getStorage({
      key: fullKey + '_time',
      success: function (res) {
        if (!res.data) {
          get_data1(fullKey, param.success);
        } else {
          deadtime = parseInt(res.data);
          let timestamp = parseInt(new Date().getTime());
          if (deadtime > 0 && deadtime < timestamp) {
            //过期了
            param.success(def);
          }
          get_data1(fullKey, def);
        }
      }
    });
  }
  /**
   * 根据key获取数据缓存 -  同步
   * @param {String} key
   */
  getSync(key, def = '') {
    let fullKey = this._getFullKey(key);
    try {
      let deadtime = uni.getStorageSync(fullKey + '_time');
      if (!deadtime) {
        return get_data2(fullKey, def);
      } else {
        deadtime = parseInt(deadtime);
        let timestamp = parseInt(new Date().getTime());
        if (deadtime > 0 && deadtime < timestamp) {
          //过期了
          return def;
        }
        return get_data2(fullKey, def);
      }
    } catch (e) {
      console.error(e);
    }
  }
  /**
   * 根据key删除一条缓存 - 异步
   * @param {String|Object} pararm 支持传入字符串、对象
   */
  removeAsync(param) {
    let fullKey = '';
    if (Object.prototype.toString.call(param) === '[object Object]') {
      fullKey = this._getFullKey(param.key);
      param.key = fullKey;
      uni.removeStorage(param);
    } else if (Object.prototype.toString.call(param) === '[object String]') {
      fullKey = this._getFullKey(param);
      uni.removeStorage({
        key: fullKey
      });
    }
    this._removeAllCache(fullKey);

    uni.removeStorage({
      key: fullKey + '_time',
      success: (res) => {}
    });
  }
  /**
   * 根据key删除一条缓存 - 同步
   * @param {String} key
   */
  removeSync(key) {
    let fullKey = this._getFullKey(key);
    try {
      uni.removeStorageSync(fullKey);
      uni.removeStorageSync(fullKey + '_time');
      this._removeAllCache(fullKey);
    } catch (e) {
      console.error(e);
    }
  }
  /**
   * 清除用户端所用缓存
   */
  clearAsync() {
    uni.clearStorage();
  }
  /**
   * 清除用户端所用缓存 - 同步
   */
  clearSync() {
    try {
      uni.clearStorageSync();
    } catch (e) {
      console.error(e);
    }
  }
  /**
   * 同步获取当前 storage 的相关信息。 - 异步
   */
  info(success) {
    uni.getStorageInfo({
      success: function (res) {
        success(res);
      }
    });
  }
  /**
   * 同步获取当前 storage 的相关信息。 - 同步
   */
  infoSync() {
    try {
      return uni.getStorageInfoSync();
    } catch (e) {
      console.log(e);
    }
  }
}
export default StorageClass;
// export default
