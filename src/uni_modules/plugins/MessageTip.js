const MessageTip = function (options) {
  if (Object.prototype.toString.call(options) === '[object Object]') {
    let typeIcon =
      options.type == 'success'
        ? 'none'
        : options.type == 'warning'
        ? 'none'
        : options.type == 'error'
        ? 'none'
        : options.type == 'info'
        ? 'none'
        : 'none';

    let setting = {
      title: options.message || '提示',
      duration: options.duration || 800,
      icon: typeIcon || 'none',
      mask: true
    };
    uni.showToast(setting);
  } else if (Object.prototype.toString.call(options) === '[object String]') {
    MessageTip['info'](options);
  }
};
/**
 *  显示提示框,2s消失的提示框
 *	@author  xiesuhang
 *  @param {string} title		 标题
 * 	@param {string} duration 显示时间
 *  @param {string} icon 		 弹框类型 icon 值说明  success loading   none
 *
 **/
MessageTip['info'] = (title, duration, icon) => {
  uni.showToast({
    title: title || '提示',
    duration: duration || 800,
    icon: icon || 'none'
    // mask: true
  });
};
// 成功提示
MessageTip['success'] = (title, duration) => {
  // MessageTip['info'](title, duration, 'success');
  MessageTip['info'](title, duration, 'none');
};
// 警告提示
MessageTip['warning'] = (title, duration) => {
  MessageTip['info'](title, duration, 'none');
};
// 错误提示
MessageTip['error'] = (title, duration) => {
  // MessageTip['info'](title, duration, 'error');
  MessageTip['info'](title, duration, 'none');
};

export default MessageTip;
