import iconsDefault from '@/uni_modules/ui-cv/components/cv-icons/icons/default-icons.js';
import iconsAction from '@/uni_modules/ui-cv/components/cv-icons/icons/icons-action.js';
import iconsArrow from '@/uni_modules/ui-cv/components/cv-icons/icons/icons-arrow.js';
import iconsBase from '@/uni_modules/ui-cv/components/cv-icons/icons/icons-base.js';
import iconsMall from '@/uni_modules/ui-cv/components/cv-icons/icons/icons-mall.js';
import iconsMedia from '@/uni_modules/ui-cv/components/cv-icons/icons/icons-media.js';
import iconsOrder from '@/uni_modules/ui-cv/components/cv-icons/icons/icons-order.js';
import iconsPeople from '@/uni_modules/ui-cv/components/cv-icons/icons/icons-people.js';
import iconsStatus from '@/uni_modules/ui-cv/components/cv-icons/icons/icons-status.js';
import iconsWallet from '@/uni_modules/ui-cv/components/cv-icons/icons/icons-wallet.js';
// 初始化
let iconsAll = {};

// iconsAll['iconsDefault'] = iconsDefault;
iconsAll['iconsAction'] = iconsAction;
iconsAll['iconsArrow'] = iconsArrow;
iconsAll['iconsBase'] = iconsBase;
iconsAll['iconsMall'] = iconsMall;
iconsAll['iconsMedia'] = iconsMedia;
iconsAll['iconsOrder'] = iconsOrder;
iconsAll['iconsPeople'] = iconsPeople;
iconsAll['iconsStatus'] = iconsStatus;
iconsAll['iconsWallet'] = iconsWallet;

export default iconsAll;
