import { createSSRApp } from 'vue';
import App from './App.vue';
import store from './store';

import { getDemoData, testReqMock } from './plugins/mock.js';
export function createApp() {
  const app = createSSRApp(App);
  app.use(store);

  app.config.globalProperties.$getDemoData = getDemoData;
  app.config.globalProperties.$testReqMock = testReqMock;
  return {
    app
  };
}
